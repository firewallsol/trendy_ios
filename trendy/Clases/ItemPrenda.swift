//
//  ItemPrenda.swift
//  trendy
//
//  Created by Leonel Sanchez on 06/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class itemPrenda {
    var id : Int!
    var nombre : String!
    var precio : String!
    var descuento : String!
    var marca : String!
    var categoria : String!
    var nombreMarca : String!
    var idMarca : String!
    var foto : String!
    var urlTienda : String!
    var detalleTexto: String!
    var fotos : [String]!
    var json : String!
    var foto_ancho : CGFloat = 0
    var foto_alto : CGFloat = 0
    var conDesc : Bool = false
}
