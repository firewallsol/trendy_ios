//
//  Color_support.swift
//  trendy
//
//  Created by Leonel Sanchez on 07/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Color_support {
    var nombre : String
    var color : UIColor
    
    init(xnombre: String, xcolor: UIColor) {
        self.nombre = xnombre
        self.color = xcolor
    }
}
