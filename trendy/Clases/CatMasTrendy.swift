//
//  CatMasTrendy.swift
//  trendy
//
//  Created by Leonel Sanchez on 06/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

class CatMasTrendy {
    
    var id: Int
    var titulo: String
    var descripcion: String
    var fechaInicio: String
    var fechaFin: String
    var tipo: String //vertical-horizontal v - h
    var foto_url: String
    
    init(xid: Int, xtitulo: String, xdescripcion: String, xfechaInicio: String, xfechaFin: String, xtipo: String, xfoto_url: String) {
        self.id = xid
        self.titulo = xtitulo
        self.descripcion = xdescripcion
        self.fechaInicio = xfechaInicio
        self.fechaFin = xfechaFin
        self.tipo = xtipo
        self.foto_url = xfoto_url
    }
}
