//
//  Categoria.swift
//  trendy
//
//  Created by Leonel Sanchez on 05/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Categoria {
    var id : Int!
    var nombre : String!
    var descripcion : String!
    var subcategorias : [SubCategoria]!
    var imagen : String!
    var tipo : String!
    
}
