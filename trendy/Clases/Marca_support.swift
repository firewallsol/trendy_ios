//
//  Marca_support.swift
//  trendy
//
//  Created by Leonel Sanchez on 19/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Foundation

class Marca_support {
    var id : String!
    var nombre : String!
    var tipo: String!
    var url : String!
    var principal : Bool = false
    var ordenPrincipal : Int = 0
}
