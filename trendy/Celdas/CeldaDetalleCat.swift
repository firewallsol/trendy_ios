//
//  CeldaDetalleCat.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaDetalleCat: UICollectionViewCell {
    
    var container : UIView = UIView()
    var imagen : UIImageView = UIImageView()
    var imgLike : UIImageView = UIImageView()
    var lblTitulo : UILabel = UILabel()
    var lblPrecio : UILabel = UILabel()
    var imgDescuento : UIImageView = UIImageView()
    var lblDesc : UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.container.addSubview(self.imagen)
        self.container.addSubview(self.lblTitulo)
        self.container.addSubview(self.lblPrecio)
        self.container.addSubview(self.imgLike)
        self.container.addSubview(self.imgDescuento)
        self.imgDescuento.addSubview(self.lblDesc)
        contentView.addSubview(self.container)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
