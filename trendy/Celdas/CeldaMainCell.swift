//
//  CeldaMainCell.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaMainCell: UICollectionViewCell {
    
    var imagenFondo : UIImageView!
    var lblCategoria : UILabel!
    //var shadowView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.buildInterface()
    }
    
    func buildInterface(){
        self.lblCategoria = UILabel(frame: CGRect(x: 10, y: 10, width: self.frame.size.width, height: 10))
        self.imagenFondo = UIImageView(frame: self.frame)
        //self.shadowView = UIView(frame: self.imagenFondo.frame)
        //self.shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        self.contentView.addSubview(imagenFondo)
        //self.contentView.addSubview(self.shadowView)
        self.contentView.addSubview(self.lblCategoria)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
