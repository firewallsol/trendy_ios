//
//  UIViewControllerExtension.swift
//  trendy
//
//  Created by Leonel Sanchez on 12/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import FacebookCore
import SwiftyJSON
import NVActivityIndicatorView

var activityI : NVActivityIndicatorView!

extension UIViewController {
    func mensajeSimple(mensaje: String, dismiss: Bool = true){
        let alert = UIAlertController(title: "TrendyAdvisor", message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        if !dismiss {
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
        
        if dismiss {
            let when = DispatchTime.now() + 2.5
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    struct MyProfileRequest: GraphRequestProtocol {
        struct Response: GraphResponseProtocol {
            var id : String!
            var nombre : String!
            var apellidos : String!
            var email : String!
            var genero : String!
            var urlImg : String!
            var likes : JSON!
            var isValid : Bool = true
            init(rawResponse: Any?) {
                //print("raw response desde el init -> \(rawResponse!)")
                let jsonData = JSON(rawValue: rawResponse!)
                self.id = ""
                if let value = jsonData?["id"].string {
                    self.id = value
                } else {
                    self.isValid = false
                }
                
                self.nombre = ""
                if let value = jsonData?["first_name"].string {
                    self.nombre = value
                } else {
                    self.isValid = (self.isValid ? false : self.isValid)
                }
                
                self.apellidos = ""
                if let value = jsonData?["last_name"].string {
                    self.apellidos = value
                } else {
                    self.isValid = (self.isValid ? false : self.isValid)
                }
                
                
                self.email = ""
                if let value = jsonData?["email"].string {
                    self.email = value
                } else {
                    self.isValid = (self.isValid ? false : self.isValid)
                }
                
                self.genero = ""
                if let value = jsonData?["gender"].string {
                    self.genero = value
                }
                
                
                self.urlImg = ""
                if (jsonData?["picture"]["data"]["url"].exists())! {
                    self.urlImg = jsonData!["picture"]["data"]["url"].string
                }
                
                self.likes = ""
                if (jsonData?["likes"].exists())!{
                    self.likes = jsonData?["likes"]
                } 
            }
        }
        
        var graphPath = "/me"
        var parameters: [String : Any]? = ["fields": "id, name, email, first_name, last_name, gender, picture, likes.limit(100){category,id,name}"]
        var accessToken = AccessToken.current
        var httpMethod: GraphRequestHTTPMethod = .GET
        var apiVersion: GraphAPIVersion = .defaultVersion
    }
    
    func setLoaderAnimation(){
        if !self.isLoaderAnimating() {
            let xframe = CGRect(x: 100, y: 100, width: Constantes.tamaños.anchoPantalla * 0.1, height: Constantes.tamaños.anchoPantalla * 0.1)
            activityI = NVActivityIndicatorView(frame: xframe, type: NVActivityIndicatorType.ballPulseSync, color: Constantes.paletaColores.rojoTrendy)
            self.view.addSubview(activityI)
            activityI.center.x = self.view.frame.size.width / 2
            activityI.center.y = self.view.frame.size.height / 2
            activityI.startAnimating()
        }
    }
    
    func isLoaderAnimating()-> Bool {
        if let _ = activityI {
            return activityI.isAnimating
        }
        return false
    }
    
    func stopLoaderAnimation(){
        if self.isLoaderAnimating() {
            activityI.stopAnimating()
        }
    }
}


extension String {
    func trimmed()->String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func URLAllowed()->String{
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
    
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
}
