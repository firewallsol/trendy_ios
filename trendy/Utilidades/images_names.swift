// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
    import UIKit.UIImage
    typealias Image = UIImage
#elseif os(OSX)
    import AppKit.NSImage
    typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum Asset: String {
    case _1 = "1"
    case _2 = "2"
    case _3 = "3"
    case _4 = "4"
    case _5 = "5"
    case _6 = "6"
    case buttonShareWhite = "button_share_white"
    case checkBlack48 = "check_black_48"
    case checkWhite48 = "check_white_48"
    case filterWhite = "filter_white"
    case icCloset = "ic_closet"
    case icHome = "ic_home"
    case icProfile = "ic_profile"
    case icSearch = "ic_search"
    case icBackCard = "ic_back_card"
    case icDelete = "ic_delete"
    case icDiscount = "ic_discount"
    case icFacebook = "ic_facebook"
    case icGarbage = "ic_garbage"
    case icHeartPressed = "ic_heart_pressed"
    case icHeart = "ic_heart"
    case icInstagram = "ic_instagram"
    case icPlaceholderProfile = "ic_placeholder_profile"
    case icShareGreen = "ic_share_green"
    case icTendyt = "ic_tendyt"
    case icTrashWhite = "ic_trash_white"
    case icTwitter = "ic_twitter"
    case icArrowDownB = "ic-arrow-down-b"
    case icArrowDownW = "ic-arrow-down-w"
    case icFilterWhite = "ic-filter-white"
    case trendyAdvisorLogo = "Trendy_Advisor_logo"
    case icSwipeLeft = "ic_swipe_left"
    case icSwipeRight = "ic_swipe_right"
    case tutorialBackground = "tutorial_background"
    
    var image: Image {
        let bundle = Bundle(for: BundleToken.self)
        #if os(iOS) || os(tvOS)
            let image = Image(named: rawValue, in: bundle, compatibleWith: nil)
        #elseif os(OSX)
            let image = bundle.image(forResource: rawValue)
        #elseif os(watchOS)
            let image = Image(named: rawValue)
        #endif
        guard let result = image else { fatalError("Unable to load image \(rawValue).") }
        return result
    }
}
// swiftlint:enable type_body_length

extension Image {
    convenience init!(asset: Asset) {
        #if os(iOS) || os(tvOS)
            let bundle = Bundle(for: BundleToken.self)
            self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
        #elseif os(OSX) || os(watchOS)
            self.init(named: asset.rawValue)
        #endif
    }
}

private final class BundleToken {}

