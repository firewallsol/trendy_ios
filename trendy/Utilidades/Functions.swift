//
//  Functions.swift
//  trendy
//
//  Created by Leonel Sanchez on 06/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

enum Sexo: Int {
    case Mujer = 1
    case Hombre = 4
    case Niño = 3
}

enum tipoRegistroLogin : Int {
    case email = 0
    case facebook = 1
}

enum senderAdvancedFilter: String {
    case search = "Search"
    case categoria = "Category"
    case subcategoria = "SubCategory"
    case advancedfilter = "AdvancedFilter"
}

enum topicNotificacion: String {
    case general = "generaldev"
    case ofertas = "generalOfertasdev"
    case hombre = "Hombre"
    case mujer = "Mujer"
}

class Functions  {
    
    let manager = {() -> Alamofire.SessionManager in
        struct Static {
            static var dispatchOnceToken: dispatch_time_t = 0
            static var instance: Alamofire.SessionManager!
        }
        
        //dispatch_once(&Int(Static.dispatchOnceToken)) {
            let config = URLSessionConfiguration.default
            config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
            config.urlCache = nil
            
            let cookies = HTTPCookieStorage.shared
            config.httpCookieStorage = cookies
            Static.instance = Alamofire.SessionManager(configuration: config)
        //}
        
        return Static.instance
    }()
    
    let userDefaults = UserDefaults.standard
    
    func getLoMasTrendy(completionHandler: @escaping ([CatMasTrendy], Error?) ->()) {
        let cualSexo = self.getSexUser()
        let queryString = Constantes.URLS.URL_lomastrendy + "\(cualSexo.rawValue)"
        print(queryString)
        self.manager.request(queryString).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("json lo mas trendy-> \(json)")
                let elArray = self.JSONtoCatMasTrendyArray(elJson: json)
                completionHandler(elArray, nil)
            case .failure(let error):
                print(error)
                completionHandler([CatMasTrendy](), error)
            }
        }
        
    }
    
    func JSONtoCatMasTrendyArray(elJson: JSON)->[CatMasTrendy]{
        var arrayCatMasTrendy = [CatMasTrendy]()
        for item in elJson["mastrendy"].array! {
            var id = 0
            if let xid = item["idmastrendy"].string {
                id = Int(xid)!
            }
            
            var titulo = ""
            if let xtitulo = item["titulo"].string {
                titulo = xtitulo
            }
            
            var descrip = ""
            if let xdescrip = item["descripcion"].string{
                descrip = xdescrip
            }
            var fechai = ""
            if let xfecha = item["fechainicio"].string {
                fechai = xfecha
            }
            var fechaf = ""
            if let xfecha = item["fechafinal"].string {
                fechaf = xfecha
            }
            var tipo = ""
            if let xtipo = item["tipo"].string {
                tipo = xtipo
            }
            var foto = ""
            if let xfoto = item["foto"].string{
                foto = xfoto
            }
            
            let unObjCatMasTrendy = CatMasTrendy(
                xid: id,
                xtitulo: titulo,
                xdescripcion: descrip,
                xfechaInicio: fechai,
                xfechaFin: fechaf,
                xtipo: tipo,
                xfoto_url: foto
            )
            arrayCatMasTrendy.append(unObjCatMasTrendy)
        }
        return arrayCatMasTrendy
    }
    
    func getLoMastrendyDetailItems(id_cat: Int, completionHandler: @escaping ([itemPrenda], Error?) ->()) {
        let urlDetalle = Constantes.URLS.URL_lomastrendyDetalle + "\(id_cat)"
        self.manager.request(urlDetalle).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("getLoMastrendyDetailItems->\(json)")
                let elArray = self.JSONtoArrayItemsPrenda(elJson: json)
                completionHandler(elArray, nil)
            case .failure(let error):
                print(error)
                completionHandler([itemPrenda](), error)
            }
        }
    }
    
    func JSONtoArrayItemsPrenda(elJson: JSON)->[itemPrenda]{
        var arrayItems = [itemPrenda]()
        if elJson["prenda"].exists() {
            let item0 = elJson["prenda"].array?[0]
            if (item0?["resultado"].exists())! {
                return arrayItems
            } else {
                for item in elJson["prenda"].array! {
                    let unItem = itemPrenda()
                    unItem.id = 0
                    if let xidprenda = item["idprenda"].string {
                        unItem.id = Int(xidprenda)
                    }
                    unItem.nombre = ""
                    if let xnombre = item["nombre"].string {
                        unItem.nombre = xnombre
                    }
                    
                    unItem.precio = "0"
                    if let strPrecio = item["precio"].string {
                        let newPrecio = strPrecio.replacingOccurrences(of: ",", with: ".")
                        unItem.precio = newPrecio
                    }
                    unItem.descuento = "0"
                    if let strDesc = item["descuento"].string {
                        let newDesc = strDesc.replacingOccurrences(of: ",", with: ".")
                        unItem.descuento = newDesc
                    }
                    
                    unItem.marca = ""
                    if let xmarca = item["marca"].string {
                        unItem.marca = xmarca
                    }
                    
                    unItem.idMarca = ""
                    if let value = item["idmarca"].string {
                        unItem.idMarca = value
                    }
                    
                    unItem.categoria = ""
                    if let nameCat = item["categoria"].string {
                        unItem.categoria = nameCat
                    }
                    
                    unItem.nombreMarca = ""
                    if let xnombremarca = item["nombremarca"].string {
                        unItem.nombreMarca = xnombremarca
                    }
                    
                    unItem.foto = ""
                    if let xfoto = item["foto"].string {
                        unItem.foto = xfoto
                    }
                    
                    unItem.urlTienda = ""
                    if let xurltienda = item["urltienda"].string {
                        unItem.urlTienda = xurltienda
                    }
                    
                    unItem.detalleTexto = ""
                    if let xdetalleTexto = item["detalletexto"].string {
                        unItem.detalleTexto = xdetalleTexto
                    }
                    //unItem.foto_ancho = 180
                    if item["anchoFoto"].exists() {
                        if let anchof = item["anchoFoto"].int {
                            unItem.foto_ancho = CGFloat(anchof)
                        }
                    }
                    //unItem.foto_alto = 210
                    if item["altoFoto"].exists() {
                        if let altof = item["altoFoto"].int {
                            unItem.foto_alto = CGFloat(altof)
                        }
                    }
                    
                    unItem.json = item.rawString()
                    
                    var lasFotos = [String]()
                    for foto in item["fotos"].arrayValue {
                        if foto.string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
                            lasFotos.append(foto.string!)
                        }
                    }
                    
                    if lasFotos.count == 0 {
                        lasFotos.append(unItem.foto)
                    }
                    
                    unItem.fotos = lasFotos
                    
                    unItem.conDesc = self.getTieneDescuento(precio: unItem.precio, descuento: unItem.descuento)
                    
                    arrayItems.append(unItem)
                }
            }
        }
        
        return arrayItems
    }
    
    func JSONtoItemPrenda(elJson: JSON)-> itemPrenda {
        let unItem = itemPrenda()
        unItem.id = 0
        if let xidprenda = elJson["idprenda"].string {
            unItem.id = Int(xidprenda)
        }
        unItem.nombre = ""
        if let xnombre = elJson["nombre"].string {
            unItem.nombre = xnombre
        }
        
        unItem.precio = "0"
        if let strPrecio = elJson["precio"].string {
            let newPrecio = strPrecio.replacingOccurrences(of: ",", with: ".")
            unItem.precio = newPrecio
        }
        unItem.descuento = "0"
        if let strDesc = elJson["descuento"].string {
            let newDesc = strDesc.replacingOccurrences(of: ",", with: ".")
            unItem.descuento = newDesc
        }
        
        unItem.marca = ""
        if let xmarca = elJson["marca"].string {
            unItem.marca = xmarca
        }
        
        unItem.idMarca = ""
        if let value = elJson["idmarca"].string {
            unItem.idMarca = value
        }
        
        unItem.categoria = ""
        if let nameCat = elJson["categoria"].string {
            unItem.categoria = nameCat
        }
        
        unItem.nombreMarca = ""
        if let xnombremarca = elJson["nombremarca"].string {
            unItem.nombreMarca = xnombremarca
        }
        
        unItem.foto = ""
        if let xfoto = elJson["foto"].string {
            unItem.foto = xfoto
        }
        
        unItem.urlTienda = ""
        if let xurltienda = elJson["urltienda"].string {
            unItem.urlTienda = xurltienda
        }
        
        unItem.detalleTexto = ""
        if let xdetalleTexto = elJson["detalletexto"].string {
            unItem.detalleTexto = xdetalleTexto
        }
        //unItem.foto_ancho = 180
        if elJson["anchoFoto"].exists() {
            if let anchof = elJson["anchoFoto"].int {
                unItem.foto_ancho = CGFloat(anchof)
            }
        }
        //unItem.foto_alto = 210
        if elJson["altoFoto"].exists() {
            if let altof = elJson["altoFoto"].int {
                unItem.foto_alto = CGFloat(altof)
            }
        }
        
        unItem.json = elJson.rawString()
        
        var lasFotos = [String]()
        for foto in elJson["fotos"].arrayValue {
            if foto.string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
                lasFotos.append(foto.string!)
            }
        }
        
        if lasFotos.count == 0 {
            lasFotos.append(unItem.foto)
        }
        
        unItem.fotos = lasFotos
        
        unItem.conDesc = self.getTieneDescuento(precio: unItem.precio, descuento: unItem.descuento)
        
        return unItem
    }
    
    func JSONtoArrayItemsPrendaFA(elJson: JSON)->[itemPrenda]{
        var arrayItems = [itemPrenda]()
        if elJson["prenda"].exists() {
            
            if (elJson["prenda"].array?.count == 0) {
                return arrayItems
            } else {
                for item in elJson["prenda"].array! {
                    let unItem = itemPrenda()
                    unItem.id = 0
                    if let xidprenda = item["idprenda"].string {
                        unItem.id = Int(xidprenda)
                    }
                    unItem.nombre = ""
                    if let xnombre = item["nombre"].string {
                        unItem.nombre = xnombre
                    }
                    
                    unItem.precio = "0"
                    if let strPrecio = item["precio"].string {
                        let newPrecio = strPrecio.replacingOccurrences(of: ",", with: ".")
                        unItem.precio = newPrecio
                    }
                    unItem.descuento = "0"
                    if let strDesc = item["descuento"].string {
                        let newDesc = strDesc.replacingOccurrences(of: ",", with: ".")
                        unItem.descuento = newDesc
                    }
                    
                    unItem.marca = ""
                    if let xmarca = item["marca"].string {
                        unItem.marca = xmarca
                    }
                    
                    unItem.idMarca = ""
                    if let value = item["idmarca"].string {
                        unItem.idMarca = value
                    }
                    
                    unItem.categoria = ""
                    if let nameCat = item["categoria"].string {
                        unItem.categoria = nameCat
                    }
                    
                    unItem.nombreMarca = ""
                    if let xnombremarca = item["nombremarca"].string {
                        unItem.nombreMarca = xnombremarca
                    }
                    
                    unItem.foto = ""
                    if let xfoto = item["foto"].string {
                        unItem.foto = xfoto
                    }
                    
                    unItem.urlTienda = ""
                    if let xurltienda = item["urltienda"].string {
                        unItem.urlTienda = xurltienda
                    }
                    
                    unItem.detalleTexto = ""
                    if let xdetalleTexto = item["detalletexto"].string {
                        unItem.detalleTexto = xdetalleTexto
                    }
                    //unItem.foto_ancho = 180
                    if item["anchoFoto"].exists() {
                        if let anchof = item["anchoFoto"].int {
                            unItem.foto_ancho = CGFloat(anchof)
                        }
                    }
                    //unItem.foto_alto = 210
                    if item["altoFoto"].exists() {
                        if let altof = item["altoFoto"].int {
                            unItem.foto_alto = CGFloat(altof)
                        }
                    }
                    
                    unItem.json = item.rawString()
                    
                    var lasFotos = [String]()
                    for foto in item["fotos"].arrayValue {
                        if foto.string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
                            lasFotos.append(foto.string!)
                        }
                    }
                    
                    if lasFotos.count == 0 {
                        lasFotos.append(unItem.foto)
                    }
                    
                    unItem.fotos = lasFotos
                    
                    unItem.conDesc = self.getTieneDescuento(precio: unItem.precio, descuento: unItem.descuento)
                    
                    arrayItems.append(unItem)
                }
            }
        }
        
        return arrayItems
    }
    
    func getStringPrecio(precio: String, descuento: String)-> NSMutableAttributedString{
        let labelPrecios = NSMutableAttributedString()
        let trimmedPrecio = precio.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let trimmedDescuento = descuento.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let descFloat = (trimmedDescuento == "" ? 0  : Float(trimmedDescuento))
        let precioFloat = (trimmedPrecio == "" ? 0  : Float(trimmedPrecio))
        let attrMiddleLine = [ NSStrikethroughStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), NSBaselineOffsetAttributeName: 0 ]
        
        /*
         
         attributeString.addAttribute(NSBaselineOffsetAttributeName, value: 0, range: NSMakeRange(0, attributeString.length))
         attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
         
         */
        if descFloat! < precioFloat! && descFloat! > Float(0) {
            
            let precioOriginal = NSAttributedString(string: "\(trimmedPrecio) \(Constantes.euro)", attributes: attrMiddleLine)
            let precioDesc = NSAttributedString(string: "\(trimmedDescuento) \(Constantes.euro)")
            labelPrecios.append(precioOriginal)
            labelPrecios.append(NSAttributedString(string: " "))
            labelPrecios.append(precioDesc)
        }
        return labelPrecios
    }
    
    func getTieneDescuento(precio: String, descuento: String)->Bool{
        var sitiene = false
        let trimmedPrecio = precio.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let trimmedDescuento = descuento.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let descFloat = Float(descuento.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        let floatZero = Float(0)
        if trimmedDescuento != "" && trimmedDescuento != trimmedPrecio && descFloat! > floatZero {
            sitiene = true
        } 
        return sitiene
    }
    
    func getStringDescuento(precio: String, descuento: String)->String{
        var resultFloat : Float = 100
        let descFloat = Float(descuento.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        let precioFloat = Float(precio.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        resultFloat = 100 - roundf((descFloat! * 100) / precioFloat!)
        let resultInt = Int(resultFloat)
        return "\(resultInt) % dto"
    }
    
    func agregarQuitarDeArmario(id_prenda: Int, xjson: String?, registrarenit: Bool = true, idMarca: String)->String{
        
        var mensaje = "Ocurrio un problema al intentar agregar/quitar la prenda del armario"
        var dicArmario = self.getDicArmario()
        //print("tamaño de dic armario antes : \(dicArmario.count)")
        if let _ = dicArmario[id_prenda] { //si existe el json se elimina
            //print("Se elimina prenda \(id_prenda) del armario ")
            dicArmario.removeValue(forKey: id_prenda)
            mensaje = ""
            if registrarenit {
                self.itelligentInteraccionRegistrar(ttipoInteraccion: "DescartarArmario", ridMarca: idMarca){ exito, error in
                    //
                    
                }
            }
        } else { //si no existe se agrega
            //print("Se agrega prenda \(id_prenda) del armario ")
            dicArmario[id_prenda] = xjson
            mensaje = ""
            if registrarenit {
                self.itelligentInteraccionRegistrar(ttipoInteraccion: "AñadirArmario", ridMarca: idMarca){ exito, error in
                    //
                    
                }
            }
        }
        let data = NSKeyedArchiver.archivedData(withRootObject: dicArmario)
        self.userDefaults.set(data, forKey: "dicArmario")
        self.updateCategoriasArmario()
        //print("tamaño de dic armario despues : \(dicArmario.count)")
        return mensaje
    }
    
    func updateItemArmario(id_prenda: Int, elItem: itemPrenda){
        var dicArmario = self.getDicArmario()
        if let _ = dicArmario[id_prenda] {
            dicArmario[id_prenda] = elItem.json
            let data = NSKeyedArchiver.archivedData(withRootObject: dicArmario)
            self.userDefaults.set(data, forKey: "dicArmario")
            self.updateCategoriasArmario()
        }
    }
    
    func existeIdEnArmario(id_prenda: Int)->Bool{
        let dicArmario = self.getDicArmario()
        if let _ = dicArmario[id_prenda] {
            return true
        } else {
            return false
        }
    }
    
    func getDicArmario()->[Int:String]{
        
        var dicArmario = [Int: String]()
        if let dicA = self.userDefaults.value(forKey: "dicArmario") as? NSData {
            let newDic = NSKeyedUnarchiver.unarchiveObject(with: dicA as Data) as! [Int:String]
            dicArmario = newDic
        }
        
        return dicArmario
    }
    
    func getCategoriasArmario()->[String]{
        var setCatsArmario = [String]()
        if let dicCats = self.userDefaults.value(forKey: "catsArmario") as? NSData {
            let newSet = NSKeyedUnarchiver.unarchiveObject(with: dicCats as Data) as! [String]
            if !newSet.isEmpty {
                setCatsArmario.append("Todos")
                setCatsArmario.append(contentsOf: newSet)
            }
        }
        return setCatsArmario
    }
    
    func updateCategoriasArmario(){
        let dicArmario = self.getDicArmario()
        var tempSetCatsArmario = Set<String>()
        var newSetCatsArmario = [String]()
        if !dicArmario.isEmpty {
            for jsonValue in [String](dicArmario.values) {
                let jsonPrenda = JSON(parseJSON: jsonValue)
                if let value = jsonPrenda["categoria"].string {
                    tempSetCatsArmario.insert(value)
                }
                
            }
            
            newSetCatsArmario = tempSetCatsArmario.sorted()
            
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: newSetCatsArmario)
        self.userDefaults.set(data, forKey: "catsArmario")
        
    }
    
    func borraTodoArmario(){
        self.userDefaults.removeObject(forKey: "dicArmario")
        self.userDefaults.removeObject(forKey: "catsArmario")
    }
    
    func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func scaleImageWidthHeight (sourceImage:UIImage, scaledToSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(scaledToSize)
        sourceImage.draw(in: CGRect(x:0, y:0, width:scaledToSize.width, height:scaledToSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func scaleImageSize(originalSize: CGSize, scaledToWidth: CGFloat)-> CGSize {
        let scaleFactor = scaledToWidth / originalSize.width
        //print("scaleFactor: \(scaleFactor)")
        
        let newHeight = originalSize.height * scaleFactor
        let newWidth = originalSize.width * scaleFactor
        return CGSize(width: newWidth, height: newHeight)
    }
    
    func getCollectionSubCategoriaItems(id_subcat: Int, tipoSexo: Sexo, index: Int, completionHandler: @escaping ([itemPrenda], Error?) ->()) {
        let urlDetalle = Constantes.URLS.URL_ProdSubcat + "\(id_subcat)&sexo=\(tipoSexo.rawValue)&index=\(index)"
        //print("------- urlDetalle: \(urlDetalle)")
        self.manager.request(urlDetalle).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print(json)
                let elArray = self.JSONtoArrayItemsPrenda(elJson: json)
                completionHandler(elArray, nil)
            case .failure(let error):
                print(error)
                completionHandler([itemPrenda](), error)
            }
        }
    }
    
    func getResultBusqueda(tipoSexo: Sexo, strBusqueda: String, indice : Int = 0, completionHandler: @escaping ([itemPrenda], Error?) ->()) {
        
        var strBusquedaClean = strBusqueda.trimmingCharacters(in: .whitespacesAndNewlines)
        strBusquedaClean = strBusquedaClean.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let urlSearch = Constantes.URLS.URL_ProdSearch + "\(tipoSexo.rawValue)&busqueda='\(strBusquedaClean)'&index=\(indice)"

        self.manager.request(urlSearch).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("getResultBusqueda->\(json)")
                let elArray = self.JSONtoArrayItemsPrenda(elJson: json)
                completionHandler(elArray, nil)
            case .failure(let error):
                print(error)
                completionHandler([itemPrenda](), error)
            }
        }
    }
        
    
    func getItemsArmario()-> [itemPrenda] {
        var arrItems = [itemPrenda]()
        let armarioActual = self.getDicArmario()
        
        if armarioActual.count > 0 {
            for (_, strJson) in armarioActual {
                let jsonPrenda = JSON(parseJSON: strJson)
                arrItems.append(self.JSONtoItemPrenda(elJson: jsonPrenda))
            }
        }
        
        return arrItems
    }
    
    func getSexUser()->Sexo {
        var xsexo = Sexo(rawValue: 1)!
        if let sexusr = self.userDefaults.value(forKey: "sexoUsr") as? Int {
            xsexo = Sexo(rawValue: sexusr)!
        }
        return xsexo
    }
    
    /*
    func getFirAuthErrorMessage(elError : Error)->String{
        var mensaje = "Ocurrio un error al intentar crear el usuario"
        if let errCode = AuthErrorCode(rawValue: elError._code) {
            
            switch errCode {
            case .emailAlreadyInUse:
                mensaje = "Este correo ya se encuentra en uso"
                break
            case .userDisabled:
                mensaje = "Esta cuenta se encuentra deshabilitada"
                break
            case .invalidEmail:
                mensaje = "Dirección de email no válida"
                break
            case .wrongPassword:
                mensaje = "La contraseña es incorrecta"
                break
            case .userNotFound:
                mensaje = "No existe este usuario registrado"
                break
            case .networkError:
                mensaje = "Ha ocurrido un problema con la red, intente de nuevo por favor"
                break
            case .weakPassword:
                mensaje = "Su contraseña es muy débil, por favor ingrese una nueva"
                break
            default: mensaje = "Ocurrio un error al intentar crear el usuario"
                
            }
        }
        return mensaje
    }
    */
    
    func borraTodoCierraSesion(){
        self.borraTodoArmario()
        self.userDefaults.removeObject(forKey: "userLogged")
        self.userDefaults.removeObject(forKey: "idUsr")
        self.userDefaults.removeObject(forKey: "emailUsr")
        self.userDefaults.removeObject(forKey: "nombreUsr")
        self.userDefaults.removeObject(forKey: "apellidosUsr")
        self.userDefaults.removeObject(forKey: "sexoUsr")
        self.userDefaults.removeObject(forKey: "urlImg")
        self.userDefaults.removeObject(forKey: "tipoLogin")
        self.userDefaults.removeObject(forKey: "guestUser")
        self.userDefaults.removeObject(forKey: "notifGeneral")
        self.userDefaults.removeObject(forKey: "notifOfertas")
        self.unSubscribeTo(xtopic: topicNotificacion.general)
        self.unSubscribeTo(xtopic: topicNotificacion.ofertas)
    }
    
    func isUserLogged()->Bool {
        if let _ = self.userDefaults.value(forKey: "userLogged") as? Bool {
            return true
        }
        return false
    }
    
    func getCurrentTipoLogin()->tipoRegistroLogin {
        var elTipo = tipoRegistroLogin.email
        if let t = self.userDefaults.value(forKey: "tipoLogin") as? tipoRegistroLogin {
            elTipo = t
        }
        return elTipo
    }
    
    func getVistoTutorial()->Bool{
        var visto = false
        
        if let _ = self.userDefaults.value(forKey: "vistoTutorial") as? Bool {
            visto = true
        }
        self.userDefaults.set(true, forKey: "vistoTutorial")
        
        return visto
    }
    
    func getCategoriaTienda(completionHandler: @escaping ([Tienda_support], [Subcategoria_support], Error?) ->()) {
        
        self.manager.request(Constantes.URLS.URL_CategoriaTienda).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("json lo mas trendy-> \(json)")
                let dicTiendas = self.JSONtoTiendas(elJson: json)
                let dicCategoria = self.JSONtoCategoria(elJson: json)
                completionHandler(dicTiendas, dicCategoria, nil)
            case .failure(let error):
                print(error)
                completionHandler([Tienda_support](), [Subcategoria_support](), error)
            }
        }
    }
    
    func getCategoriaMarcaTienda(completionHandler: @escaping ([Marca_support], [Subcategoria_support], [Tienda_support], Error?) ->()) {
        
        self.manager.request(Constantes.URLS.URL_CategoriaMarca).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("json lo mas trendy-> \(json)")
                let dicMarcas = self.JSONtoMarcas(elJson: json)
                let dicCategoria = self.JSONtoCategoria(elJson: json)
                let dicTiendas = self.JSONtoTiendas(elJson: json)
                completionHandler(dicMarcas, dicCategoria, dicTiendas, nil)
            case .failure(let error):
                print(error)
                completionHandler([Marca_support](), [Subcategoria_support](), [Tienda_support](), error)
            }
        }
    }
    
    func JSONtoCategoria(elJson: JSON)->[Subcategoria_support] {
        var arraySubCategoria = [Subcategoria_support]()
        let unItem = Subcategoria_support()
        unItem.id = "0"
        unItem.nombre = "Todas"
        unItem.tipo = "0"
        arraySubCategoria.append(unItem)
        if !elJson["subcategoria"].exists() {
            return arraySubCategoria
        } else {
            for item in elJson["subcategoria"].array! {
                let unaSubcat = Subcategoria_support()
                unaSubcat.id = item["idsubcategoria"].string!
                unaSubcat.nombre = item["nombre"].string!
                unaSubcat.tipo = item["tipo"].string!
                arraySubCategoria.append(unaSubcat)
                
            }
        }
        
        return arraySubCategoria
    }
    
    func JSONtoTiendas(elJson: JSON)->[Tienda_support] {
        var arrayTiendas = [Tienda_support]()
        let unItem = Tienda_support()
        unItem.id = "0"
        unItem.nombre = "Todas"
        arrayTiendas.append(unItem)
        if !elJson["tiendas"].exists() {
            return arrayTiendas
        } else {
            for item in elJson["tiendas"].array! {
                let unaTienda = Tienda_support()
                unaTienda.id = item["idtienda"].string!
                unaTienda.nombre = item["nombre"].string!
                arrayTiendas.append(unaTienda)
            }
        }
        
        return arrayTiendas
    }
    
    func JSONtoMarcas(elJson: JSON)->[Marca_support] {
        var arrayMarcas = [Marca_support]()
        
        if ((elJson["marcasMujer"].exists()) && (elJson["marcasHombre"].exists()) && (elJson["marcasNino"].exists())) {
            for item in elJson["marcasMujer"].array! {
                let unaMarca = Marca_support()
                unaMarca.id = item["idmarca"].string!
                unaMarca.nombre = item["nombre"].string!
                unaMarca.tipo = item["tipo"].string!
                unaMarca.url = item["url"].string!
                if item["principal"].string! != "0" {
                    unaMarca.principal = true
                    unaMarca.ordenPrincipal = Int(item["principal"].string!)!
                }
                
                arrayMarcas.append(unaMarca)
            }
            for item in elJson["marcasHombre"].array! {
                let unaMarca = Marca_support()
                unaMarca.id = item["idmarca"].string!
                unaMarca.nombre = item["nombre"].string!
                unaMarca.tipo = item["tipo"].string!
                unaMarca.url = item["url"].string!
                if item["principal"].string! != "0" {
                    unaMarca.principal = true
                    unaMarca.ordenPrincipal = Int(item["principal"].string!)!
                }
                
                arrayMarcas.append(unaMarca)
            }
            for item in elJson["marcasNino"].array! {
                let unaMarca = Marca_support()
                unaMarca.id = item["idmarca"].string!
                unaMarca.nombre = item["nombre"].string!
                unaMarca.tipo = item["tipo"].string!
                unaMarca.url = item["url"].string!
                if item["principal"].string! != "0" {
                    unaMarca.principal = true
                    unaMarca.ordenPrincipal = Int(item["principal"].string!)!
                }
                
                arrayMarcas.append(unaMarca)
            }
            
        } else {
            return arrayMarcas
        }
        
        return arrayMarcas
    }
    
    func getItemsFiltrovanzado(sexo: String, marcas: String?, categorias: String?, color: String?, precios: String, tiendas: String?, index: Int, completionHandler: @escaping ([itemPrenda], Error?) ->()) {
        let urlFAvanzado = Constantes.URLS.URL_FiltroAvanzado
        
        var params = [
            "sexo" : sexo,
            "precios" : precios,
            "index" : String(index)
        ]
        
        if let xmarcas = marcas {
            params["marcas"] = xmarcas
        }
        if let xcat = categorias {
            params["categorias"] = xcat
        }
        if let xcolor = color {
            params["color"] = xcolor
        }
        
        if let xtiendas = tiendas {
            params["tiendas"] = xtiendas
        }
        
        self.manager.request(urlFAvanzado, method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print(json)
                let elArray = self.JSONtoArrayItemsPrendaFA(elJson: json)
                completionHandler(elArray, nil)
            case .failure(let error):
                print(error)
                completionHandler([itemPrenda](), error)
            }
            
        }
        
        
    }
    
    func 🤘makethecall🤘(){
        let queryString = Constantes.URLS.URL_disparaProcesoLikes
        
        let parametros = [
            "id": "\(self.getIdUsr())"
        ]
        
        let xrequest = Alamofire.request(queryString, method: .post, parameters: parametros).responseJSON { response in
            print("respuesta de procesa likes->\(response)")
        }
        
        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){
            xrequest.cancel()
        }
    }
    
    func registerUser(rnombre: String, rapellidos: String, rcorreo: String, rpasswd: String, rsexo: Sexo, rtipo: tipoRegistroLogin, urlImg: String, flikes: JSON?, completionHandler: @escaping (Bool, String, Error?) -> ()){
        var queryString = Constantes.URLS.URL_registroUsuario
        if rtipo == .facebook {
            queryString = Constantes.URLS.URL_registroLoginFB
        }
        
        //sexo 2 - hombre 1 - mujer
        var vsexo = 0
        switch rsexo {
        case .Mujer: vsexo = 1; break;
        case .Hombre: vsexo = 2; break;
        default: break
        }
        
        
        var params = [
            "nombre" : rnombre,
            "apellidos" : rapellidos,
            "email" : rcorreo,
            "password" : rpasswd,
            "sexo" : "\(vsexo)",
            "tipo" : "\(rtipo.rawValue)"
        ]
        
        if rtipo == .facebook {
            params["avatar"] = urlImg
            params["likes"] = "\(flikes!)"
        }
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON { response in
            //print(response)
            var mensaje = "Ocurrio un error al enviar los datos, revise su conexion"
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("respuesta de registro-> \(json)")
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    var sid = 0
                    var semail = ""
                    var snombre = ""
                    var sapellidos = ""
                    var ssexo = 1
                    var surlImg = ""
                    
                    if let tid = json["data"]["id"].int {
                        sid = tid
                    }
                    
                    if sid == 0 {
                        if let tid = json["data"]["id"].string {
                            sid = Int(tid)!
                        }
                    }
                    
                    if sid == 0 {
                        mensaje = "Ocurrio un error al enviar los datos, revise su conexion"
                        completionHandler(false, mensaje,  nil)
                        return
                    }
                    
                    if let temail = json["data"]["email"].string {
                        semail = temail
                    }
                    if let tnombre = json["data"]["nombre"].string {
                        snombre = tnombre
                    }
                    if let tapellidos = json["data"]["apellidos"].string {
                        sapellidos = tapellidos
                    }
                    if let tsexo = json["data"]["sexo"].string {
                        if tsexo == "2" {                            
                            ssexo = 4
                        }
                    }
                    if let turlImg = json["data"]["avatar"].string {
                        surlImg = turlImg
                    }
                    
                    self.saveUserData(id: sid, email: semail, nombre: snombre, apellidos: sapellidos, sexo: ssexo, urlImg: surlImg, tipo: rtipo.rawValue)
                    
                    if rtipo == .facebook {
                        self.🤘makethecall🤘()
                    }
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, error)
            }
        }
        
        
    }
    
    func imgToBase64(laimagen:UIImage)->String{
        let imageData = UIImagePNGRepresentation(laimagen)
        let base64String = imageData?.base64EncodedString()
        return base64String!
    }
    
    func updateUser(rid: Int, rnombre: String, rapellidos: String, rpasswd: String?, rsexo: Sexo, rtipo: tipoRegistroLogin, objImg: UIImage?, completionHandler: @escaping (Bool, Error?) -> ()){
        
        //sexo 2 - hombre 1 - mujer
        var vsexo = 0
        switch rsexo {
        case .Mujer: vsexo = 1; break;
        case .Hombre: vsexo = 2; break;
        default: break
        }
        var xparameters = [String:AnyObject]()
        xparameters = ["nombre":rnombre as AnyObject,
                      "apellidos":rapellidos as AnyObject,
                      "id":rid as AnyObject,
                      "sexo":vsexo as AnyObject,
                      "os":"ANDROID" as AnyObject]
        
       
        
        if let passwd = rpasswd {
            xparameters["password"] = passwd as AnyObject?
        }
        
        if let nuevaImg = objImg {
            xparameters["avatar"] = self.imgToBase64(laimagen: nuevaImg) as AnyObject?
        }
        
        self.manager.request(Constantes.URLS.URL_updateUsuario, method: .post, parameters: xparameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("respuesta de update-> \(json)")
                if json["result"].string == "SUCCESS" {
                    var sid = 0
                    var semail = ""
                    var snombre = ""
                    var sapellidos = ""
                    var ssexo = 1
                    var surlImg = ""
                    
                    if let tid = json["data"]["id"].string {
                        sid = Int(tid)!
                    }
                    if let temail = json["data"]["email"].string {
                        semail = temail
                    }
                    if let tnombre = json["data"]["nombre"].string {
                        snombre = tnombre
                    }
                    if let tapellidos = json["data"]["apellidos"].string {
                        sapellidos = tapellidos
                    }
                    if let tsexo = json["data"]["sexo"].string {
                        if tsexo == "2" {
                            ssexo = 4
                        }
                    }
                    if let turlImg = json["data"]["avatar"].string {
                        surlImg = turlImg
                    }
                    
                    self.saveUserData(id: sid, email: semail, nombre: snombre, apellidos: sapellidos, sexo: ssexo, urlImg: surlImg, tipo: rtipo.rawValue)
                    
                    if ssexo == 1 {
                        self.unSubscribeTo(xtopic: .hombre)
                        self.subscribeTo(xtopic: .mujer)
                    } else {
                        self.unSubscribeTo(xtopic: .mujer)
                        self.subscribeTo(xtopic: .hombre)
                    }
                    
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(false, error)
            }
        }
        
    }
    
    func loginUser(rcorreo: String, rpasswd: String, tipo: tipoRegistroLogin, completionHandler: @escaping (Bool, Error?) -> ()){
        var queryString = Constantes.URLS.URL_loginUsuario
        queryString += "email=\(rcorreo)"
        queryString += "&password=\(rpasswd)"
        queryString += "&tipo=\(tipo.rawValue)"
        
        self.manager.request(queryString).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("respuesta de registro-> \(json)")
                if json["result"].string == "SUCCESS" {
                    var sid = 0
                    var semail = ""
                    var snombre = ""
                    var sapellidos = ""
                    var ssexo = 1
                    var surlImg = ""
                    
                    if let tid = json["data"]["id"].string {
                        sid = Int(tid)!
                    }
                    if let temail = json["data"]["email"].string {
                        semail = temail
                    }
                    if let tnombre = json["data"]["nombre"].string {
                        snombre = tnombre
                    }
                    if let tapellidos = json["data"]["apellidos"].string {
                        sapellidos = tapellidos
                    }
                    if let tsexo = json["data"]["sexo"].string {
                        if tsexo == "2" {
                            ssexo = 4
                        }
                    }
                    if let turlImg = json["data"]["avatar"].string {
                        surlImg = turlImg
                    }
                    self.saveUserData(id: sid, email: semail, nombre: snombre, apellidos: sapellidos, sexo: ssexo, urlImg: surlImg, tipo: tipo.rawValue)
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(false, error)
            }
        }
        
    }
    
    func loginInvitado(completionHandler: @escaping (Bool) -> ()){
        
        self.saveUserData(id: Constantes.guestData.id, email: Constantes.guestData.email, nombre: Constantes.guestData.nombre, apellidos: Constantes.guestData.apellidos, sexo: Constantes.guestData.sexo.rawValue, urlImg: "", tipo: tipoRegistroLogin.email.rawValue, esInvitado: true)
        completionHandler(true)
        
    }
    
    func itelligentUsuarioRegistrar(rid: String, completionHandler: @escaping (Bool, Error?) -> ()){
        var queryString = Constantes.URLS.itelligent.base + Constantes.URLS.itelligent.registro_usuario
        queryString += "cs=\(Constantes.strings.intelligent_service_code)&cc=\(Constantes.strings.intelligent_client_code)"
        
        let parametros = ["id":rid]
        
        self.manager.request(queryString, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("respuesta de intelligent registro usuario-> \(json)")
                completionHandler(true, nil)
            case .failure(let error):
                print(error)
                completionHandler(false, error)
            }
        }
            
        
    }
    
    func itelligentInteraccionRegistrar(ttipoInteraccion: String, ridMarca: String, completionHandler: @escaping (Bool, Error?) -> ()){
        var queryString = Constantes.URLS.itelligent.base + Constantes.URLS.itelligent.registro_interaccion
        queryString += "cs=\(Constantes.strings.intelligent_service_code)&cc=\(Constantes.strings.intelligent_client_code)"
        
        let parametros = [
            "idUsuario": "\(self.getIdUsr())",
            "idContenido" : ridMarca,
            "nombreInteraccion" : ttipoInteraccion
        ]
        
        Alamofire.request(queryString, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("respuesta de intelligent registro interaccion \(ttipoInteraccion) -> \(json)")
                completionHandler(true, nil)
            case .failure(let error):
                print(error)
                completionHandler(false, error)
            }
        }
        
        
    }
    
    func saveUserData(id: Int, email: String, nombre: String, apellidos: String, sexo: Int, urlImg: String, tipo: Int, esInvitado: Bool = false){
        self.borraTodoCierraSesion()
        
        self.userDefaults.set(true, forKey: "userLogged")
        self.userDefaults.set(id, forKey: "idUsr")
        self.userDefaults.set(email, forKey: "emailUsr")
        self.userDefaults.set(nombre, forKey: "nombreUsr")
        self.userDefaults.set(apellidos, forKey: "apellidosUsr")
        self.userDefaults.set(sexo, forKey: "sexoUsr")
        self.userDefaults.set(urlImg, forKey: "urlImg")
        self.userDefaults.set(tipo, forKey: "tipoLogin")
        self.userDefaults.set(true, forKey: "notifGeneral")
        self.userDefaults.set(true, forKey: "notifOfertas")
        if esInvitado {
            self.userDefaults.set(true, forKey: "guestUser")
            self.userDefaults.set(false, forKey: "notifGeneral")
            self.userDefaults.set(false, forKey: "notifOfertas")
        }
    }
    
    func isGuestUser()->Bool{
        return self.userDefaults.bool(forKey: "guestUser")
    }
    
    func getCurrentSexType(strSexo: String)->Sexo{
        var xsexo = Sexo(rawValue: 1)!
        switch strSexo {
        case "Mujer": xsexo = Sexo.Mujer
        case "Hombre": xsexo = Sexo.Hombre
        case "Niños": xsexo = Sexo.Niño
        default: break
        }
        return xsexo
    }
    
    //PAra resize de la imagen
    func scaleAndCropImage(image:UIImage, toSize size: CGSize) -> UIImage {
        // Sanity check; make sure the image isn't already sized.
        if image.size.equalTo(size) {
            return image
        }
        
        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0
        
        scaleFactor = heightFactor
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        
        var thumbnailOrigin = CGPoint.zero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }
            
        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }
        
        var thumbnailRect = CGRect.zero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func getDataUsuario()->[String:Any] {
        var datosUsr = [String:Any]()
        datosUsr["idUsr"] = ""
        datosUsr["emailUsr"] = ""
        datosUsr["nombreUsr"] = ""
        datosUsr["apellidosUsr"] = ""
        datosUsr["sexoUsr"] = Sexo.Mujer
        datosUsr["urlImg"] = ""
        datosUsr["tipoLogin"] = tipoRegistroLogin.email
        
        if let did = self.userDefaults.value(forKey: "idUsr") as? Int {
            datosUsr["idUsr"] = did
        }
        
        
        if self.isGuestUser() {
            let randInt = self.randRange(lower: 10000, upper: 99999)
            datosUsr["emailUsr"] = "invitado\(randInt)@trendyadvisor.com"
        } else {
            if let demail = self.userDefaults.value(forKey: "emailUsr") as? String {
                datosUsr["emailUsr"] = demail
            }
        }
        
        
        if let dnombre = self.userDefaults.value(forKey: "nombreUsr") as? String {
            datosUsr["nombreUsr"] = dnombre
        }
        if let dapellidos = self.userDefaults.value(forKey: "apellidosUsr") as? String {
            datosUsr["apellidosUsr"] = dapellidos
        }
        if let dsexo = self.userDefaults.value(forKey: "sexoUsr") as? Int {
            if dsexo == 4 {
                datosUsr["sexoUsr"] = Sexo.Hombre
            }
        }
        if let durl = self.userDefaults.value(forKey: "urlImg") as? String {
            datosUsr["urlImg"] = durl
        }
        if let durl = self.userDefaults.value(forKey: "tipoLogin") as? Int {
            if durl == 1 {
                datosUsr["tipoLogin"] = tipoRegistroLogin.facebook
            }
            
        }
        
        return datosUsr
    }
    
    func getIdUsr()->Int{
        var xid = 0
        if let did = self.userDefaults.value(forKey: "idUsr") as? Int {
            xid = did
        }
        return xid
    }
    
    func isValidEmailAddress(text: String) -> Bool {
        guard !text.hasPrefix("mailto:") else { return false }
        guard let emailDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return false }
        let matches = emailDetector.matches(in: text, options: NSRegularExpression.MatchingOptions.anchored, range: NSRange(location: 0, length: text.characters.count))
        guard matches.count == 1 else { return false }
        return matches[0].url?.scheme == "mailto"
    }
    
    func forgotPasswordSVC(rcorreo: String, completionHandler: @escaping (Bool, String, Error?) -> ()){
        var queryString = Constantes.URLS.URL_forgotPassword
        queryString += "\(rcorreo)"
        
        self.manager.request(queryString).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let mensaje  = json["desc"].string!
                print("respuesta de registro-> \(json)")
                if json["result"].string == "SUCCESS" {
                    
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(false, "", error)
            }
        }
        
    }
    
    func verificaProducto(id_producto: Int, completionHandler: @escaping (Bool, itemPrenda?, Error?) ->()){
        var queryString = Constantes.URLS.URL_verificaProducto
        queryString += "\(id_producto)"
        
        self.manager.request(queryString).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("respuesta de registro-> \(json)")
                if json["result"].string == "SUCCESS" {
                    let elItem = self.JSONtoItemPrenda(elJson: json["data"])
                    self.updateItemArmario(id_prenda: elItem.id, elItem: elItem)
                    completionHandler(true, elItem, nil)
                } else {
                    completionHandler(false, nil, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(false, nil, error)
            }
            
        }
    }
    
    func validateNotifications(){
        if self.isGuestUser() {
            self.unSubscribeTo(xtopic: topicNotificacion.general)
            self.unSubscribeTo(xtopic: topicNotificacion.ofertas)
            self.unSubscribeTo(xtopic: topicNotificacion.mujer)
            self.unSubscribeTo(xtopic: topicNotificacion.hombre)
        } else {
            let xappDelegate = UIApplication.shared.delegate as! AppDelegate
            var paraGeneral = false
            var paraOfertas = false
            (paraGeneral, paraOfertas) = self.getStatusNotifications()
            
            if paraGeneral {
                xappDelegate.subscribeTo(topic: topicNotificacion.general.rawValue)
            }
            
            if paraOfertas {
                xappDelegate.subscribeTo(topic: topicNotificacion.ofertas.rawValue)
            }
            
            let elSexo = self.getSexUser()
            
            if elSexo == .Hombre{
                self.unSubscribeTo(xtopic: .mujer)
                self.subscribeTo(xtopic: .hombre)
            } else {
                self.unSubscribeTo(xtopic: .hombre)
                self.subscribeTo(xtopic: .mujer)
            }
        }
    }
    
    func getStatusNotifications()->(Bool, Bool) {
        if self.isGuestUser() {
            return (false, false)
        } else {
            return(self.userDefaults.bool(forKey: "notifGeneral"), self.userDefaults.bool(forKey: "notifOfertas"))
        }
    }
    
    func subscribeTo(xtopic : topicNotificacion){
        let xappDelegate = UIApplication.shared.delegate as! AppDelegate
        xappDelegate.subscribeTo(topic: xtopic.rawValue)
        self.changeNotificationStatus(xtopic: xtopic, status: true)
    }
    
    func unSubscribeTo(xtopic : topicNotificacion){
        let xappDelegate = UIApplication.shared.delegate as! AppDelegate
        xappDelegate.unSubscribeTo(topic: xtopic.rawValue)
        self.changeNotificationStatus(xtopic: xtopic, status: false)
    }
    
    func changeNotificationStatus(xtopic: topicNotificacion, status: Bool){
        switch xtopic {
        case .general: self.userDefaults.set(status, forKey: "notifGeneral")
        case .ofertas: self.userDefaults.set(status, forKey: "notifOfertas")
        default: break
        }
    }
    
    func getCategoriasSubcategoriasServicio(elSexo : Sexo, completionHandler: @escaping ([Categoria]?, Error?) ->()){
        
        var queryString = Constantes.URLS.URL_categoriasSubcategorias
        queryString += "\(elSexo.rawValue)"
        
        self.manager.request(queryString).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("respuesta de registro-> \(json)")
                if json["result"].string == "SUCCESS" {
                    let elArray = self.JSONtoArrayCategoria(elJson: json["categorias"])
                    completionHandler(elArray, nil)
                } else {
                    completionHandler(nil, nil)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil, error)
            }
            
        }
    }
    
    func JSONtoArrayCategoria(elJson : JSON)->[Categoria]{
        var tempCategoria = [Categoria]()
        
        for item in elJson.array! {
            let unaCat = Categoria()
            unaCat.id = 0
            unaCat.nombre = ""
            unaCat.descripcion = ""
            unaCat.imagen = ""
            unaCat.tipo = ""
            unaCat.subcategorias = [SubCategoria]()
            
            if let value = item["id"].string {
                unaCat.id = Int(value)
            }
            
            if let value = item["nombre"].string {
                unaCat.nombre = value
            }
            
            if let value = item["descripcion"].string {
                unaCat.descripcion = value
            }
            
            if let value = item["imagen"].string {
                unaCat.imagen = value
            }
            
            if let value = item["tipo"].string {
                unaCat.tipo = value
            }
            
            
            if item["childs"].exists() {
                unaCat.subcategorias = self.JSONtoArraySubCategoria(elJson: item["childs"])
            }
            
            tempCategoria.append(unaCat)
            
        }
        return tempCategoria
    }
    
    func JSONtoArraySubCategoria(elJson : JSON)->[SubCategoria]{
        var tempSubCat = [SubCategoria]()
        
        for item in elJson.array! {
            let unaSubCat = SubCategoria()
            unaSubCat.id = 0
            unaSubCat.nombre = ""
            unaSubCat.descripcion = ""
            unaSubCat.imagen = ""
            unaSubCat.tipo = ""
            unaSubCat.id_categoria = 0
            unaSubCat.subcategorias = [SubCategoria]()
            
            if let value = item["id"].string {
                unaSubCat.id = Int(value)
            }
            
            if let value = item["idPadre"].string {
                unaSubCat.id_categoria = Int(value)
            }
            
            if let value = item["nombre"].string {
                unaSubCat.nombre = value
            }
            
            if let value = item["descripcion"].string {
                unaSubCat.descripcion = value
            }
            
            if let value = item["imagen"].string {
                unaSubCat.imagen = value
            }
            
            if let value = item["tipo"].string {
                unaSubCat.tipo = value
            }
            
            tempSubCat.append(unaSubCat)
            
        }
        
        return tempSubCat
    }
    
    
    func randRange (lower: UInt32 , upper: UInt32) -> Int {
        return Int(lower + arc4random_uniform(upper - lower + 1))
    }
    
    
}
