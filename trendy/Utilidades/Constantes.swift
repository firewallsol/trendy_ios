//
//  Constantes.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

struct Constantes {
    static let euro = "€"
    
    struct strings {
        static let textLabelRegistro = "Al darse de alta en TrendyAdvisor, estás suscribiendo y aceptando expresamente los Términos y Condiciones y la Política de privacidad"
        static let textLabelRecuperarContra = "Introduce tu email y te enviaremos un mensaje de correo con las instrucciones para reestablecer tu contraseña"
        
        static let arrayMH = ["Mujer","Hombre"]
        static let arrayMHN = ["Mujer","Hombre","Niños"]
        static let arrayMHNA = ["Mujer","Hombre","N/A"]
        
        static let strAñadirArmario = "AÑADIR A MI ARMARIO"
        static let strElimiarArmario = "ELIMINAR DE MI ARMARIO"
        static let mensajeSinresultados = "Lo sentimos, no existen resultados para esta búsqueda"
        static let mensajeArmarioVacio = "Tu Armario esta vacío"
        static let mensajeVaciarArmario = "¿Estás seguro de eliminar todas las prendas de tú armario?"
        static let mensajeEliminarPrenda = "¿Estás seguro de eliminar esta prenda de tú armario?"
        
        static let intelligent_service_code = "27ab21ee-4c3d-11e7-a77b-0ad892f83325"
        static let intelligent_client_code = "b3e0b3be-519f-11e7-a77b-0ad892f83325"
        
        static let arrayColores = [
            Color_support(xnombre: "Blanco", xcolor: UIColor.white),
            Color_support(xnombre: "Negro", xcolor: UIColor.black),
            Color_support(xnombre: "Verde", xcolor: UIColor(red: 13/255, green: 1, blue: 0, alpha: 1)), //13	255	0
            Color_support(xnombre: "Azul", xcolor: UIColor(red: 38/255, green: 0, blue: 1, alpha: 1)), //38	0	255
            Color_support(xnombre: "Gris", xcolor: UIColor(red: 166/255, green: 166/255, blue: 166/255, alpha: 1)), //166	166	166
            Color_support(xnombre: "Violeta", xcolor: UIColor(red: 201/255, green: 162/255, blue: 200/255, alpha: 1)), //201	162	200
            Color_support(xnombre: "Rojo", xcolor: UIColor.red),
            Color_support(xnombre: "Rosa", xcolor: UIColor(red: 232/255, green: 55/255, blue: 232/255, alpha: 1)), //232	55	232
            Color_support(xnombre: "Amarillo", xcolor: UIColor.yellow),
            Color_support(xnombre: "Morado", xcolor: UIColor(red: 128/255, green: 0, blue: 128/255, alpha: 1)), //128	0	128
            Color_support(xnombre: "Marron", xcolor: UIColor(red: 89/255, green: 22/255, blue: 13/255, alpha: 1)), //89	33	13
            Color_support(xnombre: "Naranja", xcolor: UIColor(red: 1, green: 166/255, blue: 0, alpha: 1)), //255	166	0
            Color_support(xnombre: "Dorado", xcolor: UIColor(red: 231/255, green: 172/255, blue: 24/255, alpha: 1)) //231	172	24
        ]
    }
    
    struct URLS {
        static let URL_lomastrendy = "https://trendyadvisor.com/servicios/mastrendy.php?sexo="
        static let URL_lomastrendyDetalle = "https://trendyadvisor.com/servicios/getProductosMT.php?idmastrendy="
        static let URL_ProdSubcat = "http://trendyadvisor.com/servicios/getProductos.php?id_subcategoria="
        static let URL_ProdSearch = "http://trendyadvisor.com/servicios/getProductos.php?sexo="
        static let URL_FacebookAddr = "https://www.facebook.com/TrendyAdvisor/?ref=hl"
        static let URL_TwitterAddr = "https://twitter.com/TrendyAdvisor"
        static let URL_InstagramAddr = "https://www.instagram.com/trendyadvisor"
        static let URL_privacidad = "https://trendyadvisor.com/privacidad.php"
        static let URL_CategoriaTienda = "https://trendyadvisor.com/servicios/getCategoriaTienda.php"
        static let URL_CategoriaMarca = "https://trendyadvisor.com/servicios/getCategoriaMarca.php"
        static let URL_FiltroAvanzado = "https://trendyadvisor.com/servicios/buscarprendasnew.php"
        //static let URL_registroUsuario = "http://192.168.1.67/trendy/servicios/registroapp.php?"
        static let URL_registroUsuario = "https://trendyadvisor.com/servicios/registroapp.php"
        static let URL_loginUsuario = "https://trendyadvisor.com/servicios/login.php?"
        static let URL_registroLoginFB = "https://trendyadvisor.com/servicios/loginfacebook.php"
        static let URL_disparaProcesoLikes = "https://trendyadvisor.com/servicios/process.php"
        //static let URL_updateUsuario = "http://192.168.1.67/trendy/servicios/editarusuario.php"
        static let URL_updateUsuario = "https://trendyadvisor.com/servicios/editarusuario.php"
        static let URL_forgotPassword = "https://trendyadvisor.com/servicios/recovery.php?email="
        static let URL_verificaProducto = "https://trendyadvisor.com/servicios/validacionproducto.php?idprenda="
        static let URL_categoriasSubcategorias = "https://trendyadvisor.com/servicios/getcatsub.php?sexo="
        
        
        struct itelligent {
            static let base = "http://trendyadvisorcie.eu-west-1.elasticbeanstalk.com/test/"
            static let registro_usuario = "usuario/registrar?"
            static let registro_interaccion = "interaccion/registrar?"
        }
    }
    
    struct paletaColores {
        static let azulBtnFacebook = UIColor(red: 59/255, green: 87/255, blue: 157/255, alpha: 1)
        static let tabbarbackground = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        static let backgroundPresentationCardButton = UIColor(red: 134/255, green: 8/255, blue: 55/255, alpha: 1)
        static let verdeLikeCorazon = UIColor(red: 116/255, green: 176/255, blue: 101/255, alpha: 1)
        static let rojoTrendy = UIColor(red: 159/255, green: 15/255, blue: 68/255, alpha: 1)
    }
    
    struct tamaños {
        static let anchoTextFieldLine : CGFloat = 1
        static let altoStatusBar = UIApplication.shared.statusBarFrame.height
        static let screenBounds = UIScreen.main.bounds
        static let tamanioPantalla   = screenBounds.size
        static let anchoPantalla  = tamanioPantalla.width
        static let altoPantalla = tamanioPantalla.height
        
        struct fuentes {
            static let LabelFontSize = UIFont.labelFontSize
            
            struct minus {
                static let LabelFontSize_1 = UIFont.labelFontSize - 1
                static let LabelFontSize_2 = UIFont.labelFontSize - 2
                static let LabelFontSize_3 = UIFont.labelFontSize - 3
                static let LabelFontSize_4 = UIFont.labelFontSize - 4
            }
            
            struct plus {
                static let LabelFontSize_1 = UIFont.labelFontSize + 1
                static let LabelFontSize_2 = UIFont.labelFontSize + 2
                static let LabelFontSize_3 = UIFont.labelFontSize + 3
                static let LabelFontSize_4 = UIFont.labelFontSize + 4
            }
            
        }
    }
    
    struct imagenes {
        static let imgFooterHome = UIImage(named: "ic_home")
        static let imgFooterSearch = UIImage(named: "ic_search")
        static let imgFooterCloset = UIImage(named: "ic_closet")
        static let imgFooterProfile = UIImage(named: "ic_profile")
        static let imgPlaceHolderProfile = UIImage(named: "ic_placeholder_profile")
    }
    
    struct guestData {
        static let id = 1924
        static let email = "invitado@trendyadvisor.com"
        static let nombre = "Invitado"
        static let apellidos = "Trendy"
        static let sexo = Sexo.Mujer
        static let mensajeLogin = "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión"
        static let mensajeEditarPerfil = "No se puede editar el perfil de invitado"
        static let mensajeMasTrendy = "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión"
        static let mensajeAgregarArmario = "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión"
        static let mensajeAccesoArmario = "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión"
        static let mensajeNotificaciones = "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión"
    }
    
}
