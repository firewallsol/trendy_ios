//
//  ProfileVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import FacebookLogin

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var scrollView : UIScrollView!
    var viewTop : UIView!
    var imgUsuario : UIImageView!
    var lblNombre : UILabel!
    var lblCorreo : UILabel!
    var tablePerfil : UITableView!
    
    var insetBottom : CGFloat = 0
    var altoNavBar : CGFloat = 0
    
    var editarPerfilCell =  UITableViewCell()
    //var amigosCell = UITableViewCell()
    var alertaOfertasCell = UITableViewCell()
    var notificacionesCell = UITableViewCell()
    var facebookCell = UITableViewCell()
    let imageFacebook = UIImageView()
    var instagramCell = UITableViewCell()
    var twitterCell = UITableViewCell()
    var comparteCell = UITableViewCell()
    //var ayudaCell = UITableViewCell()
    var privacidadCell = UITableViewCell()
    var cerrarSesionCell = UITableViewCell()
    
    var checkNotificaciones : Bool = false
    var checkOfertas : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)! + 10
        
        (self.checkNotificaciones, self.checkOfertas) = Functions().getStatusNotifications()
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Perfil"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.view.backgroundColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Functions().isUserLogged() {
            self.setData()
        }
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        
        //top con imagen, nombre y correo
        xframe.origin = CGPoint(x: 0, y: self.altoNavBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.anchoPantalla * 0.5)
        self.viewTop = UIView(frame: xframe)
        self.viewTop.backgroundColor = UIColor.white
        
        //imagen 
        xframe.origin = CGPoint(x: 0, y: 10)
        let anchoAltoImagen = self.viewTop.frame.size.height * 0.6
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgUsuario = UIImageView(frame: xframe)
        //cargar la imagen aca por kingfisher
        self.imgUsuario.image = Constantes.imagenes.imgPlaceHolderProfile
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        self.imgUsuario.layer.borderColor = UIColor.lightGray.cgColor
        self.imgUsuario.layer.borderWidth = 0.5
        self.imgUsuario.clipsToBounds = true
        self.viewTop.addSubview(self.imgUsuario)
        self.imgUsuario.center.x = self.viewTop.frame.size.width / 2
        
        //label correo
        xframe.origin = CGPoint(x: 0, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.lblCorreo = UILabel(frame: xframe)
        self.lblCorreo.text = ""
        self.lblCorreo.textAlignment = .center
        self.viewTop.addSubview(self.lblCorreo)
        self.lblCorreo.frame.origin.y = self.viewTop.frame.size.height - (self.lblCorreo.frame.size.height + 5)
       
        
        //label nombre
        xframe.origin = CGPoint(x: 0, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.lblNombre = UILabel(frame: xframe)
        self.lblNombre.text = ""
        self.lblNombre.textAlignment = .center
        self.viewTop.addSubview(self.lblNombre)
        let originnombre = (self.imgUsuario.frame.maxY + self.lblCorreo.frame.minY) / 2
        self.lblNombre.frame.origin.y = originnombre - 10
        self.lblNombre.center.x = Constantes.tamaños.anchoPantalla / 2
        
        
        self.scrollView.addSubview(self.viewTop)
        
        //tableview
        xframe.origin = CGPoint(x: 0, y: self.viewTop.frame.maxY)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 200)
        self.tablePerfil = UITableView(frame: xframe, style: .grouped)
        self.tablePerfil.backgroundColor = UIColor.white
        self.tablePerfil.delegate = self
        self.tablePerfil.dataSource = self
        self.tablePerfil.isScrollEnabled = false
        
        self.scrollView.addSubview(self.tablePerfil)
        
        //editar perfil
        self.editarPerfilCell.textLabel?.text = "Editar Perfil"
        self.editarPerfilCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.editarPerfilCell.selectionStyle = .none
        
        //amigos
        /*
        self.amigosCell.textLabel?.text = "Amigos"
        self.amigosCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.amigosCell.selectionStyle = .none
        */
        
        //alerta 
        self.alertaOfertasCell.textLabel?.text = "Alerta Ofertas"
        self.alertaOfertasCell.accessoryType = UITableViewCellAccessoryType.none
        if self.checkOfertas {
            self.alertaOfertasCell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        self.alertaOfertasCell.selectionStyle = .none
        
        //notificaciones
        self.notificacionesCell.textLabel?.text = "Notificaciones"
        self.notificacionesCell.accessoryType = UITableViewCellAccessoryType.none
        if self.checkNotificaciones {
            self.notificacionesCell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        self.notificacionesCell.selectionStyle = .none
        
        let itemSize = CGSize(width:30, height: 30)
        let imageRect = CGRect(x: 0, y: 0, width: itemSize.width, height: itemSize.height)
        
        //facebook
        self.facebookCell.imageView?.image = Asset.icFacebook.image
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        self.facebookCell.imageView!.image?.draw(in: imageRect)
        self.facebookCell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.facebookCell.imageView?.sizeToFit()
        self.facebookCell.textLabel?.text = "Facebook"
        self.facebookCell.selectionStyle = .none
        
        //instagram
        self.instagramCell.imageView?.image = Asset.icInstagram.image
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        self.instagramCell.imageView!.image?.draw(in: imageRect)
        self.instagramCell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.instagramCell.textLabel?.text = "Instagram"
        self.instagramCell.selectionStyle = .none
        
        //twitter
        self.twitterCell.imageView?.image = Asset.icTwitter.image
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        self.twitterCell.imageView!.image?.draw(in: imageRect)
        self.twitterCell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.twitterCell.textLabel?.text = "Twitter"
        self.twitterCell.selectionStyle = .none
        
        //comparte
        self.comparteCell.imageView?.image = Asset.icShareGreen.image
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        self.comparteCell.imageView!.image?.draw(in: imageRect)
        self.comparteCell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.comparteCell.textLabel?.text = "Comparte"
        self.comparteCell.selectionStyle = .none
        
        //ayuda
        /*
        self.ayudaCell.textLabel?.text = "Ayuda"
        self.ayudaCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.ayudaCell.selectionStyle = .none
        */
 
        //privacidad
        self.privacidadCell.textLabel?.text = "Ver condiciones y privacidad"
        self.privacidadCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.privacidadCell.selectionStyle = .none
        
        //cerrar sesion
        self.cerrarSesionCell.textLabel?.text = "Cerrar sesión"
        self.cerrarSesionCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.cerrarSesionCell.selectionStyle = .none
        
        self.view.addSubview(self.scrollView)
        
        self.tablePerfil.reloadData()
        self.tablePerfil.layoutIfNeeded()
        
        let contentSizeTemp = self.tablePerfil.contentSize
        self.tablePerfil.frame.size.height = contentSizeTemp.height
        
        self.scrollView.contentSize.height = self.tablePerfil.frame.maxY + (((self.tabBarController?.tabBar.frame.size.height)!) / 2)
    }
    
    func setData(){
    
        let datos = Functions().getDataUsuario()
        //correo
        self.lblCorreo.text = datos["emailUsr"] as! String?
        self.lblCorreo.adjustsFontSizeToFitWidth = true
        self.lblCorreo.sizeToFit()
        self.lblCorreo.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //nombre
        self.lblNombre.text = datos["nombreUsr"] as? String
        self.lblNombre.adjustsFontSizeToFitWidth = true
        self.lblNombre.sizeToFit()
        self.lblNombre.center.x = Constantes.tamaños.anchoPantalla / 2
        if (datos["urlImg"] as? String ) != "" {
            self.imgUsuario.kf.indicatorType = .activity
            let url = URL(string: (datos["urlImg"] as! String ))
            self.imgUsuario.kf.setImage(with: url)
            self.imgUsuario.setNeedsDisplay()
        }
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return 2
        case 2: return 4
        case 3: return 2
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: //editar perfil, amigos
            switch indexPath.row {
            case 0: return self.editarPerfilCell
            //case 1: return self.amigosCell
            default: fatalError("fila desconocida en seccion 0")
            }
        break
        case 1: //configuracion: alerta, notificaciones
            switch indexPath.row {
            case 0: return self.alertaOfertasCell
            case 1: return self.notificacionesCell
            default: fatalError("fila desconocida en seccion 1")
            }
        break
        case 2: //social: facebook, instagram, twitter, comparte
            switch indexPath.row {
            case 0: return self.facebookCell
            case 1: return self.instagramCell
            case 2: return self.twitterCell
            case 3: return self.comparteCell
            default: fatalError("fila desconocida en seccion 2")
            }
        case 3: //nosotros: ayuda, privacidad, cerrar sesion
            switch indexPath.row {
            //case 0: return self.ayudaCell
            case 0: return self.privacidadCell
            case 1: return self.cerrarSesionCell
            default: fatalError("fila desconocida en seccion 3")
            }
        default: fatalError("Unknown section")
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(section) {
        case 0: return nil
        case 1: return "Configuración"
        case 2: return "Social"
        case 3: return "Nosotros"
        default: fatalError("Unknown section")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat.leastNormalMagnitude
        } else {
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: //editar perfil, amigos
            switch indexPath.row {
            case 0:
                if Functions().isGuestUser() {
                    self.mensajeSimple(mensaje: Constantes.guestData.mensajeEditarPerfil, dismiss: false)
                } else {
                    self.pushVC(elVC: EditProfileVC())
                }
                
                break
            
            default: fatalError("fila desconocida en seccion 0")
            }
            break
        case 1: //configuracion: alerta, notificaciones
            if Functions().isGuestUser() {
                self.mensajeSimple(mensaje: Constantes.guestData.mensajeNotificaciones, dismiss: false)
            } else {
                switch indexPath.row {
                case 0: self.checkUnckeckCell(index: indexPath)
                case 1: self.checkUnckeckCell(index: indexPath)
                default: fatalError("fila desconocida en seccion 1")
                }
            }
            
            break
        case 2: //social: facebook, instagram, twitter, comparte
            switch indexPath.row {
            case 0: self.gotoWebView(sender: "FB"); break;
            case 1: self.gotoWebView(sender: "INS"); break;
            case 2: self.gotoWebView(sender: "TW"); break;
            case 3: self.shareApp(); break;
            default: fatalError("fila desconocida en seccion 2")
            }
        case 3: //nosotros: ayuda, privacidad, cerrar sesion
            switch indexPath.row {
            //case 0: print("ayuda"); break;
            case 0: self.gotoWebView(sender: "PRIV"); break;
            case 1: self.cerrarSesion(); break;
            default: fatalError("fila desconocida en seccion 3")
            }
        default: fatalError("Unknown section")
            
        }
    }
    
    func gotoWebView(sender: String){
        let webviewvc = GeneralWebViewVC()
        webviewvc.altoTabBar = (self.tabBarController?.tabBar.frame.size.height)!
        webviewvc.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)!
        switch sender {
        case "FB":
            webviewvc.URLParam = Constantes.URLS.URL_FacebookAddr
            webviewvc.titulo = "Facebook"
            break
        case "TW":
            webviewvc.URLParam = Constantes.URLS.URL_TwitterAddr
            webviewvc.titulo = "Twitter"
            break
        case "INS":
            webviewvc.URLParam = Constantes.URLS.URL_InstagramAddr
            webviewvc.titulo = "Instagram"
            break
        case "PRIV":
            webviewvc.URLParam = Constantes.URLS.URL_privacidad
            webviewvc.titulo = "Privacidad"
            break
        default:
            break
        }
        self.pushVC(elVC: webviewvc)
        
    }
    
    func pushVC(elVC: UIViewController){
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func checkUnckeckCell(index: IndexPath){
        //seccion 1 - row 0 : Ofertas
        //seccion 1 - row 1 : notificaciones
        var mensaje = "Se ha "
        var msgSuscrito = "suscrito para recibir "
        var msgCanal = "Notificaciones"
        var xcelda = topicNotificacion.general
        if index.row == 0 {
            xcelda = .ofertas
            msgCanal = "Ofertas"
        }
        
        let cell = self.tablePerfil.cellForRow(at: index)!
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            Functions().unSubscribeTo(xtopic: xcelda)
            mensaje = "Ya no recibira mas "
            msgSuscrito = ""
        } else {
            cell.accessoryType = .checkmark
            Functions().subscribeTo(xtopic: xcelda)
        }
        
        self.mensajeSimple(mensaje: mensaje + msgSuscrito + msgCanal)
    }
    
    func shareApp(){
        let firstActivityItem = "TrendyAdvisor IOS"
        let secondActivityItem : NSURL = NSURL(string: "https://trendyadvisor.com/app/")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.unknown
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func cerrarSesion(){
        
        Functions().borraTodoCierraSesion()
        
        let lmanager = LoginManager()
        lmanager.logOut()
        
        self.navigationController?.viewControllers.removeAll()
        
        self.navigationController?.dismiss(animated: false, completion: nil)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoCoverScreen()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
