//
//  ClosetVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Segmentio
import CollectionViewWaterfallLayout

class ClosetVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CollectionViewWaterfallLayoutDelegate {
    
    var scrollHorizontal : UIScrollView!
    var segmentioView: Segmentio!
    var closetCollection : UICollectionView!
    
    var datosCollectionx = [itemPrenda]()
    var datosCollectionBackup = [itemPrenda]()
    
    let cellIdentifier = "celdaDetalleCat"
    
    var insetBottom : CGFloat = 0
    var altoNavBar : CGFloat = 0
    
    var segmentioOptions = SegmentioOptions()
    
    var filterApplied = false
    var segmentedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)! + 10
        
        self.segmentioOptions = SegmentioOptions(
        backgroundColor: .black,
        maxVisibleItems: 3,
        scrollEnabled: true,
        indicatorOptions: SegmentioIndicatorOptions(type: .bottom, ratio: 1, height: 2, color: Constantes.paletaColores.rojoTrendy),
        horizontalSeparatorOptions: nil,
        verticalSeparatorOptions: nil,
        imageContentMode: .scaleAspectFit,
        labelTextAlignment: .center,
        labelTextNumberOfLines: 2,
        segmentStates: self.setHorizontalBarStates(),
        animationDuration: 0.2
        )
        
        self.datosCollectionx = Functions().getItemsArmario()
        self.datosCollectionBackup = self.datosCollectionx
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Mi Armario"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
    }
    
    func reloadData(){
        self.datosCollectionx = Functions().getItemsArmario()
        self.datosCollectionBackup = self.datosCollectionx
        if self.filterApplied {
            self.filterArmario(categoria: self.segmentioView.segmentioItems[self.segmentedIndex].title!)
        }
        let range = Range(uncheckedBounds: (0, self.closetCollection.numberOfSections))
        let indexSet = IndexSet(integersIn: range)
        self.closetCollection.performBatchUpdates({
            self.closetCollection.reloadSections(indexSet)
        }, completion: nil)
        
        self.segmentioView.setup(content: self.fillHorizontalBar(), style: .onlyLabel, options: self.segmentioOptions)
        if !self.datosCollectionx.isEmpty {
            self.segmentioView.selectedSegmentioIndex = self.segmentedIndex
            self.segmentioView.layoutSubviews()
        }
        
    }
    
    func alertaBorrarArmario(){
        if self.datosCollectionx.isEmpty {
            let alert = UIAlertController(title: "TrendyAdvisor", message: Constantes.strings.mensajeArmarioVacio, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "TrendyAdvisor", message: Constantes.strings.mensajeVaciarArmario, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler:{ action in
                Functions().borraTodoArmario()
                self.filterApplied = false
                self.segmentedIndex = 0
                self.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func buildInterface(){
        
        self.view.backgroundColor = UIColor.black
        
        //nav button
        let btnBorrarTodo = UIButton(type: .custom)
        btnBorrarTodo.setImage(Asset.icTrashWhite.image, for: .normal)
        btnBorrarTodo.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBorrarTodo.addTarget(self, action: #selector(self.alertaBorrarArmario), for: .touchUpInside)
        let itemR = UIBarButtonItem(customView: btnBorrarTodo)
        self.navigationItem.setRightBarButton(itemR, animated: true)
        
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 0, y: self.altoNavBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 50)
        //segmented view top
        self.segmentioView = Segmentio(frame: xframe)
        self.segmentioView.setup(content: self.fillHorizontalBar(), style: .onlyLabel, options: self.segmentioOptions)
        self.view.addSubview(self.segmentioView)
        
        if !self.datosCollectionx.isEmpty {
            self.segmentioView.selectedSegmentioIndex = 0
        }
        self.segmentioView.valueDidChange = { segmentio, xsegmentIndex in
            self.segmentedIndex = xsegmentIndex
            self.filterApplied = (self.segmentedIndex == 0 ? false : true)
            self.reloadData()
        }
        
        //layout collection
        let layout = CollectionViewWaterfallLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: ((self.tabBarController?.tabBar.frame.size.height)! * 2) + 10, right: 10)
        layout.columnCount = 2
        //collection view
        xframe.origin = CGPoint(x: 0, y: self.segmentioView.frame.maxY)
        let altocollection = self.view.frame.height - self.segmentioView.frame.maxY
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altocollection)
        self.closetCollection = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.closetCollection.dataSource = self
        self.closetCollection.delegate = self
        self.closetCollection.backgroundColor = UIColor.black
        self.closetCollection.register(CeldaDetalleCat.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.closetCollection)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosCollectionx.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaDetalleCat
        cell.backgroundColor = UIColor.white
        var xframe = CGRect.zero
        
        xframe = cell.frame
        cell.container.frame = xframe
        cell.container.backgroundColor = UIColor.white
        cell.container.layer.cornerRadius = 10
        cell.container.center.y = cell.frame.size.height / 2
        cell.container.center.x = cell.frame.size.width / 2
        
        let itemActual = self.datosCollectionx[indexPath.item]
        
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.9, height: cell.container.frame.size.height * 0.75)
        cell.imagen.frame = xframe
        cell.imagen.center.x = cell.frame.size.width / 2
        cell.imagen.contentMode = .scaleToFill
        let urlImg = URL(string:itemActual.foto)
        //cell.imagen.kf.setImage(with: urlImg, options: [.transition(.fade(0.2))])
        cell.imagen.kf.setImage(with: urlImg)
        cell.imagen.setNeedsDisplay()
        cell.imagen.center.x = cell.container.frame.size.width / 2
        
        //titulo
        xframe.origin = CGPoint(x: 5, y: cell.imagen.frame.maxY + 7)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblTitulo.frame = xframe
        cell.lblTitulo.text = itemActual.nombreMarca
        cell.lblTitulo.font = UIFont.boldSystemFont(ofSize: cell.lblTitulo.font.pointSize)
        cell.lblTitulo.adjustsFontSizeToFitWidth = true
        //cell.lblTitulo.sizeToFit()
        
        
        //precio
        xframe.origin = CGPoint(x: 5, y: cell.lblTitulo.frame.maxY + 4)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblPrecio.frame = xframe
        if #available(iOS 8.2, *) {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightThin)
        } else {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15)
        }
        
        if itemActual.conDesc {
            cell.lblPrecio.attributedText = Functions().getStringPrecio(precio: itemActual.precio, descuento: itemActual.descuento)
        } else {
            
            let strprecio = "\(itemActual.precio!) €"
            cell.lblPrecio.attributedText = NSAttributedString(string: strprecio)
        }
        
        cell.lblPrecio.adjustsFontSizeToFitWidth = true
        
        //like
        let anchoAltoLike = cell.container.frame.size.width * 0.22
        let yposlike = cell.imagen.frame.maxY + (((cell.container.frame.size.height - cell.imagen.frame.maxY) / 2) - (anchoAltoLike / 2) )
        let xposlike = cell.container.frame.size.width - (anchoAltoLike + 10)
        
        xframe.origin = CGPoint(x: xposlike, y: yposlike)
        xframe.size = CGSize(width: anchoAltoLike, height: anchoAltoLike)
        cell.imgLike.removeFromSuperview()
        cell.imgLike = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgLike)
        cell.imgLike.image = Asset.icDelete.image
        cell.imgLike.setNeedsDisplay()
        let tapImgLike = UITapGestureRecognizer(target: self, action: #selector(self.alertRemoveFromCloset(sender:)))
        cell.imgLike.isUserInteractionEnabled = true
        cell.imgLike.addGestureRecognizer(tapImgLike)
        cell.imgLike.tag = indexPath.item
        
        //descuento
        let anchoAltoDesc = cell.container.frame.size.width * 0.3
        let yposDesc = cell.imagen.frame.minY
        let xposDesc = cell.imagen.frame.maxX - (anchoAltoDesc + 5)
        cell.imgDescuento.removeFromSuperview()
        xframe.origin = CGPoint(x: xposDesc, y: yposDesc)
        xframe.size = CGSize(width: anchoAltoDesc, height: anchoAltoDesc)
        cell.imgDescuento = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgDescuento)
        cell.imgDescuento.image = Asset.icDiscount.image
        cell.imgDescuento.setNeedsDisplay()
        
        //lbl descuento
        cell.lblDesc.removeFromSuperview()
        cell.lblDesc = UILabel(frame: cell.imgDescuento.frame)
        cell.imgDescuento.addSubview(cell.lblDesc)
        cell.lblDesc.numberOfLines = 2
        cell.lblDesc.textColor = UIColor.white
        cell.lblDesc.textAlignment = .center
        cell.lblDesc.text = ""
        cell.lblDesc.font = UIFont.systemFont(ofSize: cell.lblDesc.font.pointSize - 2)
        
        if itemActual.conDesc {
            cell.imgDescuento.isHidden = false
            cell.lblDesc.isHidden = false
            cell.lblDesc.text = Functions().getStringDescuento(precio: itemActual.precio, descuento: itemActual.descuento)
            cell.lblDesc.sizeToFit()
            cell.lblDesc.center.x = cell.imgDescuento.frame.size.width / 2
            cell.lblDesc.center.y = cell.imgDescuento.frame.size.height / 2
        } else {
            cell.imgDescuento.isHidden = true
            cell.lblDesc.isHidden = true
        }
        
        
        cell.dropShadow()
        
        cell.setNeedsDisplay()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let itemActual = self.datosCollectionx[indexPath.item]
        
        var elTamaño = CGSize.zero
        let anchoCollView = (collectionView.frame.size.width) / 2 //2.08
        let anchoCelda = anchoCollView - 20
        
        let tamañoImagen = Functions().scaleImageSize(originalSize: CGSize(width: itemActual.foto_ancho, height: itemActual.foto_alto), scaledToWidth: anchoCelda * 0.9)
        
        var alto : CGFloat = 0
        
        if itemActual.foto_ancho < itemActual.foto_alto {
            alto = tamañoImagen.height + (tamañoImagen.height * 0.1) + 20
        } else {
            alto = tamañoImagen.height + (tamañoImagen.height * 0.35) + 50
        }
        
        elTamaño.height = max(alto, 225)
        elTamaño.width = anchoCelda
        
        //print("tamaño de imagen: \(itemActual.foto_ancho), \(itemActual.foto_alto)")
        //print("tamaño de celda: \(elTamaño)")
        
        return elTamaño

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemActual = self.datosCollectionx[indexPath.item]
        self.setLoaderAnimation()
        Functions().verificaProducto(id_producto: itemActual.id){exito, item, error in
            self.stopLoaderAnimation()
            if exito {
                if let newItem = item {
                    self.datosCollectionx[indexPath.item] = newItem
                }
                let detalle = ProductDetailVC()
                detalle.itemActual = self.datosCollectionx[indexPath.item]
                self.navigationController?.pushViewController(detalle, animated: true)
                detalle.buildInterface()
            } else {
                if let _ = error {
                    self.mensajeSimple(mensaje: "Ocurrio un error al verificar la disponibilidad de este producto. Verifique su conexión a internet")
                } else {
                    let mensaje = "Este producto ya no esta disponible, será eliminado de su armario"
                    
                    let alert = UIAlertController(title: "TrendyAdvisor", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK    ", style: UIAlertActionStyle.default, handler:{ action in
                            self.removeFromCloset(itemIndex: indexPath.item)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
        }
    }
    
    func fillHorizontalBar()-> [SegmentioItem]{
        let titles = Functions().getCategoriasArmario()
        
        var titleBarItems = [SegmentioItem]()
        
        if !titles.isEmpty {
            for item in titles {
                let unSitem = SegmentioItem(title: item, image: nil)
                titleBarItems.append(unSitem)
            }
        }
        return titleBarItems
    }
    
    func setHorizontalBarStates()->SegmentioStates{
        let defaultS = SegmentioState(backgroundColor: UIColor.black, titleFont: UIFont.systemFont(ofSize: UIFont.systemFontSize), titleTextColor: .white)
        return (defaultState: defaultS, selectedState: defaultS, highlightedState: defaultS)
    }
    
    func alertRemoveFromCloset(sender: UITapGestureRecognizer){
        
        let alert = UIAlertController(title: "TrendyAdvisor", message: Constantes.strings.mensajeEliminarPrenda, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler:{ action in
            let index = sender.view?.tag
            self.removeFromCloset(itemIndex: index!)
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removeFromCloset(itemIndex: Int){
        let msg = Functions().agregarQuitarDeArmario(id_prenda: self.datosCollectionx[itemIndex].id, xjson: self.datosCollectionx[itemIndex].json, idMarca: self.datosCollectionx[itemIndex].idMarca)
        if msg != "" {
            print(msg)
        } else {
            self.reloadData()
        }
        
    }
    
    func filterArmario(categoria: String){
        if self.filterApplied {
            let tempData = self.datosCollectionBackup
            self.datosCollectionx.removeAll()
            for item in tempData {
                if item.categoria == categoria {
                    self.datosCollectionx.append(item)
                }
            }
            if self.datosCollectionx.isEmpty {
                self.datosCollectionx = self.datosCollectionBackup
                self.segmentedIndex = 0
            }
        } else {
            self.datosCollectionx = self.datosCollectionBackup
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
