//
//  SwipeViewControllerVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 05/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import ZLSwipeableViewSwift
import Kingfisher

class SwipeViewControllerVC: UIViewController {

    var swipeableView: ZLSwipeableView!
    var tutorialSwipeable: ZLSwipeableView!
    var cardIndex = 0
    var leftView : UIView!
    var rightView : UIView!
    var estaAnimandoIzq = false
    var estaAnimandoDer = false
    var titleHeader = ""
    var CatTrendy : CatMasTrendy!
    var itemsTrendy = [itemPrenda]()
    var showTutorialBand = true
    var arrayTarjetas = [UIView]()
    var lblNoMasProductos : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLoaderAnimation()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        Functions().getLoMastrendyDetailItems(id_cat: CatTrendy.id){ items, error in
            if let xerror = error {
                print("Ha ocurrido un problema: \(xerror)")
            } else {
                self.itemsTrendy = items
                self.buildSwipeableView()
                self.llenaArrayTarjetas()
                self.buildInterface()
            }
        }
        
        self.view.backgroundColor = UIColor.black
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = CatTrendy.titulo
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func llenaArrayTarjetas(){
        self.arrayTarjetas.removeAll()
        self.arrayTarjetas.append(self.getPresentationCard())
        var index = 0
        for _ in self.itemsTrendy {
            self.arrayTarjetas.append(self.getItemCard(elIndex: index))
            index += 1
        }
    }
    
    func buildSwipeableView(){
        self.swipeableView = ZLSwipeableView()
        var xframe = CGRect.zero
        xframe.size = CGSize(width: self.view.frame.size.width * 0.82, height: self.view.frame.size.height * 0.75)
        self.swipeableView.frame = xframe
        view.addSubview(self.swipeableView)
        
    }
    
    func buildInterface(){
        
        //nav button
        let btnRewind = UIButton(type: .custom)
        btnRewind.setImage(Asset.icBackCard.image, for: .normal)
        btnRewind.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        btnRewind.addTarget(self, action: #selector(self.rewindCards), for: .touchUpInside)
        let itemR = UIBarButtonItem(customView: btnRewind)
        self.navigationItem.setRightBarButton(itemR, animated: true)
        
        self.swipeableView.numberOfHistoryItem = UInt(self.arrayTarjetas.count)
        self.swipeableView.allowedDirection = Direction.Horizontal
        self.swipeableView.minTranslationInPercent = 0.7
        self.swipeableView.minVelocityInPointPerSecond = 250
        let activeViews : UInt = (UInt(self.arrayTarjetas.count >= 10 ? 10 : self.arrayTarjetas.count))
        self.swipeableView.numberOfActiveView = activeViews
        
        self.swipeableView.nextView = {
            return self.nextCardView()
        }
        
        self.swipeableView.center.x = self.view.frame.size.width / 2
        self.swipeableView.center.y = (self.view.frame.size.height / 2) + 10
        
        
        //left view
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 10)
        let anchoLeftView = Constantes.tamaños.anchoPantalla * 0.17
        xframe.size = CGSize(width: anchoLeftView, height: anchoLeftView * 1.2)
        self.leftView = UIView(frame: xframe)
        self.leftView.frame.origin.x = 0 - self.leftView.frame.size.width - 5
        self.leftView.center.y = self.view.frame.height / 2
        self.leftView.backgroundColor = UIColor.white
        self.leftView.layer.cornerRadius = 10
        
        //imagen
        xframe.size = CGSize(width: self.leftView.frame.size.width * 0.6, height: self.leftView.frame.size.height * 0.6)
        let imgLeftView = UIImageView()
        imgLeftView.contentMode = .scaleAspectFill
        self.leftView.addSubview(imgLeftView)
        imgLeftView.frame = xframe
        imgLeftView.image = Asset.icGarbage.image
        imgLeftView.center.x = self.leftView.frame.size.width / 2
        imgLeftView.center.y = self.leftView.frame.size.height / 2
        
        
        self.view.addSubview(self.leftView)
        self.leftView.isHidden = true
        
        //rightView
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: anchoLeftView, height: anchoLeftView * 1.2)
        self.rightView = UIView(frame: xframe)
        self.rightView.frame.origin.x = self.view.frame.size.width + 5 + self.rightView.frame.size.width
        self.rightView.center.y = self.view.frame.height / 2
        self.rightView.backgroundColor = UIColor.white
        self.rightView.layer.cornerRadius = 10
        self.view.addSubview(self.rightView)
        
        
        //imagen =
        xframe.size = CGSize(width: self.rightView.frame.size.width * 0.6, height: self.rightView.frame.size.height * 0.6)
        let imgRightView = UIImageView()
        imgRightView.contentMode = .scaleAspectFill
        self.rightView.addSubview(imgRightView)
        imgRightView.frame = xframe
        imgRightView.image = Asset.icHeartPressed.image
        imgRightView.center.x = self.rightView.frame.size.width / 2
        imgRightView.frame.origin.y = 2
        
        //label
        let lblRightView = UILabel()
        self.rightView.addSubview(lblRightView)
        lblRightView.text = "Trendy it!"
        lblRightView.frame.size.width = self.rightView.frame.size.width * 0.95
        lblRightView.frame.size.height = self.rightView.frame.size.height - imgRightView.frame.maxY
        lblRightView.adjustsFontSizeToFitWidth = true
        
        lblRightView.frame.origin.y = imgRightView.frame.maxY + 2
        lblRightView.center.x = self.rightView.frame.size.width / 2
        lblRightView.textColor = Constantes.paletaColores.verdeLikeCorazon
        
        self.rightView.isHidden = true
        
        swipeableView.didStart = {view, location in
            //print("Did start swiping view at location: \(location)")
            
        }
        swipeableView.swiping = {view, location, translation in
            //print("Swiping at view location: \(location) translation: \(translation)")
            
            if let _ = view as? CardView {
                if location.x <= 90 && !self.estaAnimandoIzq {
                    self.leftView.isHidden = false
                    self.animateEnterView(cual: "I")
                } else if location.x >= self.view.frame.size.width - 150 && !self.estaAnimandoDer {
                    self.rightView.isHidden = false
                    self.animateEnterView(cual: "D")
                }
            }
        }
        swipeableView.didEnd = {view, location in
            //print("Did end swiping view at location: \(location)")
        }
        swipeableView.didSwipe = {view, direction, vector in
            //print("Did swipe view in direction: \(direction), vector: \(vector)")
            if let _ = view as? CardView {
                let itemAdd = self.itemsTrendy[view.tag]
                if direction == .Right {
                    self.rightView.isHidden = false
                    self.animateEnterView(cual: "D")
                    
                    if !Functions().existeIdEnArmario(id_prenda: itemAdd.id){
                        let msg = Functions().agregarQuitarDeArmario(id_prenda: itemAdd.id, xjson: itemAdd.json, registrarenit: false, idMarca: itemAdd.idMarca)
                        if  msg != "" {
                            print(msg)
                        }
                    }
                    
                    Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "MeGusta", ridMarca: itemAdd.idMarca){ exito, error in
                        //
                        
                    }
                    Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "MeGusta", ridMarca: "\(itemAdd.id!)"){ exito, error in
                        //
                        
                    }
                    
                } else if direction == .Left {
                    Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "NoMeGusta", ridMarca: itemAdd.idMarca){ exito, error in
                        //
                        
                    }
                    Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "NoMeGusta", ridMarca: "\(itemAdd.id!)"){ exito, error in
                        //
                        
                    }
                    self.leftView.isHidden = false
                    self.animateEnterView(cual: "I")
                }
            }
            
        }
        swipeableView.didCancel = {view in
            //print("Did cancel swiping view")
            if let _ = view as? CardView {
                self.animateOutView(cual: "D")
                self.animateOutView(cual: "I")
            }
        }
        swipeableView.didTap = {view, location in
            //print("Did tap at location \(location)")
            if let _ = view as? CardView {
                self.gotoDetail(index: view.tag)
            }
        }
        swipeableView.didDisappear = { view in
            //print("Did disappear swiping view")
            
            if let _ = view as? CardView {
                self.animateOutView(cual: "D")
                self.animateOutView(cual: "I")
            }
        }
        
        self.showTutorialBand = !Functions().getVistoTutorial()
        
        if self.showTutorialBand {
            showTutorial()
        }
        
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: 50)
        self.lblNoMasProductos = UILabel(frame: xframe)
        self.lblNoMasProductos.textColor = UIColor.white
        self.lblNoMasProductos.numberOfLines = 2
        self.lblNoMasProductos.text = "No existen más productos"
        self.lblNoMasProductos.textAlignment = .center
        self.lblNoMasProductos.sizeToFit()
        self.lblNoMasProductos.isHidden = true
        self.view.addSubview(self.lblNoMasProductos)
        self.view.sendSubview(toBack: self.lblNoMasProductos)
        self.lblNoMasProductos.center.x = Constantes.tamaños.anchoPantalla / 2
        self.lblNoMasProductos.center.y = Constantes.tamaños.altoPantalla / 2
        
        if self.isLoaderAnimating(){
            UIView.animate(withDuration: 0.3, animations: {
                self.stopLoaderAnimation()
                self.lblNoMasProductos.isHidden = false
            })
        }

    }
    
    func rewindCards(){
        
        if self.cardIndex >= 0 {
            
            self.swipeableView.rewind()
            
            self.cardIndex = (self.cardIndex == 0 ? 0 : self.cardIndex - 1)
            
            print("indice de cardIndex en rewind: \(self.cardIndex)")
            
            //let id = self.itemsTrendy[self.cardIndex]
            
            //if Functions().existeIdEnArmario(id_prenda: <#T##Int#>){
            //    if Functions().agregarQuitarDeArmario(id_prenda: <#T##Int#>, xjson: "")
            //}
            
        }
        
    }
    
    func gotoDetail(index: Int){
        let detailView = ProductDetailVC()
        detailView.desdeMT = true
        detailView.itemActual = self.itemsTrendy[index]
        self.navigationController?.pushViewController(detailView, animated: true)
        detailView.buildInterface()
    }
    
    func showTutorial(){
        self.tutorialSwipeable = ZLSwipeableView()
        self.navigationController?.navigationBar.layer.zPosition = -1
        var xframe = CGRect.zero
        var altoView = self.view.frame.size.height - (self.navigationController?.navigationBar.frame.size.height)!
        altoView -= Constantes.tamaños.altoStatusBar + 5
        altoView -= (self.tabBarController?.tabBar.frame.size.height)!
        //xframe.size = CGSize(width: self.view.frame.size.width * 0.85, height: altoView)
        xframe.size = CGSize(width: self.view.frame.size.width * 0.9, height: self.view.frame.size.height * 0.9)
        //let ypos = (self.navigationController?.navigationBar.frame.size.height)! + 5 + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 10, y: Constantes.tamaños.altoStatusBar + 5)
        self.tutorialSwipeable.frame = xframe
        view.addSubview(self.tutorialSwipeable)
        self.tutorialSwipeable.allowedDirection = Direction.Horizontal
        self.tutorialSwipeable.numberOfActiveView = 1
        self.tutorialSwipeable.nextView = {
            self.getTutorialView()
        }
        
        self.tutorialSwipeable.loadViews()
        
        self.tutorialSwipeable.center.x = self.view.frame.size.width / 2
        
        self.tutorialSwipeable.didDisappear = { view in
            self.tutorialSwipeable.removeFromSuperview()
            self.navigationController?.navigationBar.layer.zPosition = 0
        }
        self.navigationController?.view.bringSubview(toFront: self.tutorialSwipeable)
    }
    
    func getTutorialView()->UIView? {
        
        if !self.showTutorialBand {
            return nil
        }
        
        self.showTutorialBand = false
        
        let cardView = UIView(frame: self.tutorialSwipeable.bounds)
        
        //imagen
        let imgFondo = UIImageView(frame: cardView.bounds)
        cardView.addSubview(imgFondo)
        imgFondo.image = Asset.tutorialBackground.image
        imgFondo.contentMode = .scaleAspectFill
        imgFondo.setNeedsDisplay()
        imgFondo.clipsToBounds = true
        
        var xframe = CGRect.zero
        //label titulo
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: cardView.frame.size.width * 0.9, height: 50)
        let lblTitulo = UILabel()
        lblTitulo.numberOfLines = 2
        cardView.addSubview(lblTitulo)
        lblTitulo.frame = xframe
        lblTitulo.text = "Te mostraremos una serie de productos."
        lblTitulo.font = UIFont.boldSystemFont(ofSize: 20)
        
        lblTitulo.textAlignment = .center
        lblTitulo.textColor = UIColor.white
        lblTitulo.sizeToFit()
        lblTitulo.center.x = cardView.frame.size.width / 2
        
        
        //btn swipe derecha
        xframe.origin = CGPoint(x: 10, y: lblTitulo.frame.maxY + 10)
        xframe.size = CGSize(width: cardView.frame.size.height * 0.2, height: cardView.frame.size.height * 0.2)
        let imgSwipeRight = UIImageView(frame: xframe)
        imgSwipeRight.image = Asset.icSwipeRight.image
        imgSwipeRight.contentMode = .scaleAspectFill
        cardView.addSubview(imgSwipeRight)
        imgSwipeRight.center.x = cardView.frame.size.width / 2
        
        //label si te gusta
        xframe.origin = CGPoint(x: 10, y: imgSwipeRight.frame.maxY + 5)
        xframe.size = CGSize(width: cardView.frame.size.width * 0.8, height: 50)
        let lblDerecha = UILabel()
        lblDerecha.numberOfLines = 2
        cardView.addSubview(lblDerecha)
        lblDerecha.frame = xframe
        lblDerecha.text = "Si te gusta lo que ves, desliza a la derecha"
        lblDerecha.textAlignment = .center
        lblDerecha.textColor = UIColor.white
        lblDerecha.sizeToFit()
        lblDerecha.center.x = cardView.frame.size.width / 2
        
        //btn swipe izquierda
        xframe.origin = CGPoint(x: 10, y: lblDerecha.frame.maxY + 5)
        xframe.size = CGSize(width: cardView.frame.size.height * 0.2, height: cardView.frame.size.height * 0.2)
        let imgSwipeLeft = UIImageView(frame: xframe)
        imgSwipeLeft.image = Asset.icSwipeLeft.image
        imgSwipeLeft.contentMode = .scaleAspectFill
        cardView.addSubview(imgSwipeLeft)
        imgSwipeLeft.center.x = cardView.frame.size.width / 2
        
        //label si no te gusta
        xframe.origin = CGPoint(x: 10, y: imgSwipeLeft.frame.maxY + 5)
        xframe.size = CGSize(width: cardView.frame.size.width * 0.8, height: 50)
        let lblIzquierda = UILabel()
        lblIzquierda.numberOfLines = 2
        cardView.addSubview(lblIzquierda)
        lblIzquierda.frame = xframe
        lblIzquierda.text = "Si no te gusta, desliza a la izquierda"
        lblIzquierda.textAlignment = .center
        lblIzquierda.textColor = UIColor.white
        lblIzquierda.sizeToFit()
        lblIzquierda.center.x = cardView.frame.size.width / 2
        
        //boton
        xframe.origin = CGPoint(x: 10, y: lblIzquierda.frame.maxY + 10)
        xframe.size = CGSize(width: cardView.frame.size.width * 0.7, height: cardView.frame.size.width * 0.15)
        let btnComienza = UIButton(frame: xframe)
        cardView.addSubview(btnComienza)
        btnComienza.backgroundColor = Constantes.paletaColores.backgroundPresentationCardButton
        btnComienza.setTitle("Comenzar ya!", for: .normal)
        btnComienza.setTitleColor(UIColor.white, for: .normal)
        btnComienza.layer.cornerRadius = 18
        btnComienza.addTarget(self, action: #selector(self.tapBtnComienza), for: .touchUpInside)
        btnComienza.center.x = cardView.frame.size.width / 2
        
        return cardView
    }
    
    func tapBtnComienza(){
        self.tutorialSwipeable.swipeTopView(inDirection: .Right)
    }
    
    func tapBtnPresentacion(){
        self.swipeableView.swipeTopView(inDirection: .Right)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nextCardView() -> UIView? {
        
        if self.cardIndex >= self.arrayTarjetas.count {
            
            return nil
        }
        
        var Card = UIView()
        
        Card = self.arrayTarjetas[self.cardIndex]
        
        self.cardIndex = (self.cardIndex == self.arrayTarjetas.count ? self.cardIndex : self.cardIndex + 1)
        //print("en next card self.cardIndex: \(self.cardIndex)")
        return Card
    }
    
    func getPresentationCard()-> UIView {
        let cardView = PresentationCardView(frame: swipeableView.bounds)
        
        //imagen
        let urlImg = URL(string: self.CatTrendy.foto_url)
        cardView.imgFondo.kf.indicatorType = .activity
        cardView.imgFondo.kf.setImage(with: urlImg)
        cardView.imgFondo.contentMode = .scaleAspectFill
        cardView.imgFondo.setNeedsDisplay()
        cardView.clipsToBounds = true
        
        //descrip
        cardView.lblDescrip.text = self.CatTrendy.descripcion
        cardView.lblDescrip.frame.size.width = cardView.frame.size.width * 0.8
        cardView.lblDescrip.frame.size.height = cardView.frame.size.height * 0.8
        cardView.lblDescrip.sizeToFit()
        cardView.lblDescrip.center.x = cardView.frame.size.width / 2
        cardView.lblDescrip.center.y = (cardView.frame.size.height / 2) - (cardView.lblDescrip.frame.size.height / 3)
        
        //num prods 
        cardView.lblNumProds.text = "\(self.itemsTrendy.count) PRODUCTOS"
        cardView.lblNumProds.frame.size.width = cardView.frame.size.width * 0.8
        cardView.lblNumProds.frame.size.height = cardView.frame.size.height * 0.8
        cardView.lblNumProds.sizeToFit()
        cardView.lblNumProds.center.x = cardView.frame.size.width / 2
        cardView.lblNumProds.frame.origin.y = cardView.lblDescrip.frame.maxY + 20
        
        
        //boton
        cardView.btnDiscover.center.x = cardView.frame.size.width / 2
        cardView.btnDiscover.frame.origin.y = cardView.lblNumProds.frame.maxY + 20
        cardView.btnDiscover.addTarget(self, action: #selector(self.tapBtnPresentacion), for: .touchUpInside)
        
        return cardView
    }
    
    func getItemCard(elIndex: Int)-> UIView {
        let cardView = CardView(frame: swipeableView.bounds)
        let itemActual = self.itemsTrendy[elIndex]
        
        //imagen
        let urlImg = URL(string: itemActual.foto)
        cardView.imgFondo.kf.indicatorType = .activity
        cardView.imgFondo.kf.setImage(with: urlImg)
        cardView.imgFondo.setNeedsDisplay()
        //cardView.imgFondo.contentMode = .scaleToFill
        cardView.imgFondo.contentMode = .scaleAspectFill
        cardView.imgFondo.clipsToBounds = true
        cardView.clipsToBounds = true
        
        //precio
        if itemActual.conDesc {
            cardView.lblPrecio.attributedText = Functions().getStringPrecio(precio: itemActual.precio, descuento: itemActual.descuento)
        } else {
            cardView.lblPrecio.attributedText = NSAttributedString(string:"\(itemActual.precio!) €")
        }
        
        cardView.lblPrecio.sizeToFit()
        let posx = cardView.frame.size.width - cardView.lblPrecio.frame.size.width - 10
        cardView.lblPrecio.frame.origin = CGPoint(x: posx, y: cardView.imgFondo.frame.size.height + 30)
        
        //descuento
        if itemActual.conDesc {
            cardView.backDescuento.isHidden = false
            cardView.lblDescuento.isHidden = false
            cardView.lblDescuento.text = Functions().getStringDescuento(precio: itemActual.precio, descuento: itemActual.descuento)
            cardView.sizeToFit()
            cardView.lblDescuento.center.x = cardView.backDescuento.frame.size.width / 2
            cardView.lblDescuento.center.y = cardView.backDescuento.frame.size.height / 2
        }
        
        //modelo
        cardView.lblModelo.text = itemActual.nombre!
        cardView.lblModelo.frame.size.width = cardView.frame.size.width * 0.9
        cardView.lblModelo.frame.size.height = 18
        cardView.lblModelo.frame.origin = CGPoint(x: 10, y: cardView.imgFondo.frame.maxY + 15)
        
        //marca
        cardView.lblMarca.text = itemActual.nombreMarca!
        cardView.lblMarca.frame.size.width = (cardView.frame.size.width * 0.95) - (cardView.lblPrecio.frame.size.width + 10)
        cardView.lblMarca.frame.size.height = 20
        cardView.lblMarca.frame.origin = CGPoint(x: 10, y: cardView.lblModelo.frame.maxY + 10)
        
        cardView.lblPrecio.frame.origin.y = cardView.lblMarca.frame.origin.y
        
        cardView.tag = elIndex
        
        return cardView
    }
    
    func animateEnterView(cual: String){
        switch cual {
        case "I":
            self.estaAnimandoIzq = true
            self.animateOutView(cual: "D")
            
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
                self.leftView.frame.origin.x = 1
            }, completion: { finised in
                self.estaAnimandoIzq = false
            })
            break
        case "D":
            self.estaAnimandoDer = true
            self.animateOutView(cual: "I")
            
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
                self.rightView.frame.origin.x = self.view.frame.size.width - (self.rightView.frame.size.width + 1)
            }, completion: { finised in
                self.estaAnimandoDer = false
            })
            break
        default:
            break
        }
        
    }
    
    func animateOutView(cual: String){
        switch cual {
        case "I":
            self.estaAnimandoIzq = false
            self.leftView.isHidden = true
            self.leftView.frame.origin.x = 0 - self.leftView.frame.size.width - 1
            break
        case "D":
            self.estaAnimandoDer = false
            self.rightView.isHidden = true
            self.rightView.frame.origin.x = self.view.frame.size.width + 1 + self.rightView.frame.size.width
            break
        default:
            break
        }
    }

}
