//
//  MainScreenVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyGif

class MainScreenVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var mainCollectionView : UICollectionView!
    let cellIdentifier = "celdaMain"
    
    var datosMain = [CatMasTrendy]()
    
    var insetTop : CGFloat = 0
    var insetBottom : CGFloat = 0
    
    var headerBar : UIView!
    var preHeaderBar : UIView!
    var imgLogo : UIImageView!
    
    var altoHeader : CGFloat = 0
    
    var imageLoader : UIImageView!
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions().validateNotifications()
        
        self.view.backgroundColor = UIColor.black
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.altoHeader = Constantes.tamaños.anchoPantalla * 0.15
        
        self.insetTop = Constantes.tamaños.altoStatusBar + self.altoHeader
        
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)! + 5
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.tintColor = UIColor.white
        self.refreshControl.addTarget(self, action: #selector(self.loadData), for: UIControlEvents.valueChanged)
        
        self.buildInterface()
        
        //set loader
        var xframe = CGRect.zero
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.3, height: Constantes.tamaños.anchoPantalla * 0.3)
        self.imageLoader = UIImageView(frame: xframe)
        self.view.addSubview(self.imageLoader)
        self.imageLoader.center.x = self.view.frame.size.width / 2
        self.imageLoader.center.y = self.view.frame.size.height / 2
        let gifManager = SwiftyGifManager(memoryLimit: 10)
        let gif = UIImage(gifName: "playera_gif_v1.gif")
        self.imageLoader.setGifImage(gif, manager: gifManager)
        self.imageLoader.isHidden = true
        
        self.loadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.setStatusBarHidden(false, with: .none)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.title = ""
    }
    
    func loadData(){
        self.refreshControl.endRefreshing()
        
        self.imageLoader.isHidden = false
        
        Functions().getLoMasTrendy(){ arrayMasTrendy, error in
            if let xerror = error {
                print("Ocurrio un error al cargar los datos \(xerror)")
            } else {
                self.datosMain.removeAll()
                self.datosMain = arrayMasTrendy
                self.mainCollectionView.reloadData()
                let when = DispatchTime.now() + 1.5
                DispatchQueue.main.asyncAfter(deadline: when){
                    if !self.imageLoader.isHidden {
                        self.imageLoader.isHidden = true
                    }
                }
                
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func buildInterface(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        self.mainCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        self.mainCollectionView.dataSource = self
        self.mainCollectionView.delegate = self
        
        
        self.mainCollectionView.register(CeldaMainCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.mainCollectionView)
        self.mainCollectionView.addSubview(self.refreshControl)
        
        //header
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoStatusBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoHeader)
        self.headerBar = UIView(frame: xframe)
        self.headerBar.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        self.view.addSubview(self.headerBar)
        
        //img logo
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: altoHeader * 0.7)
        self.imgLogo = UIImageView(frame: xframe)
        self.imgLogo.image = Asset.trendyAdvisorLogo.image
        self.imgLogo.contentMode = .scaleAspectFit
        self.headerBar.addSubview(self.imgLogo)
        self.imgLogo.center.x = self.headerBar.frame.size.width / 2
        self.imgLogo.center.y = self.headerBar.frame.size.height / 2
        
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoStatusBar)
        self.preHeaderBar = UIView(frame: xframe)
        self.preHeaderBar.backgroundColor = UIColor.black
        self.view.addSubview(self.preHeaderBar)
        
        self.view.bringSubview(toFront: self.preHeaderBar)
        self.view.bringSubview(toFront: self.headerBar)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosMain.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaMainCell
        let itemMasT = self.datosMain[indexPath.item]
        let urlimg = URL(string: itemMasT.foto_url)
        cell.imagenFondo.frame = cell.frame
        cell.imagenFondo.kf.indicatorType = .activity
        cell.imagenFondo.kf.setImage(with: urlimg)
        cell.imagenFondo.contentMode = .scaleAspectFill
        cell.imagenFondo.center.x = cell.frame.size.width / 2
        cell.imagenFondo.center.y = cell.frame.size.height / 2
        cell.imagenFondo.clipsToBounds = true
        cell.imagenFondo.setNeedsDisplay()
        
        cell.lblCategoria.text = itemMasT.titulo
        var xframe = CGRect.zero
        xframe.size = CGSize(width: cell.frame.size.width * 0.8, height: cell.frame.size.height * 0.5)
        cell.lblCategoria.frame = xframe
        cell.lblCategoria.numberOfLines = 0
        cell.lblCategoria.font = cell.lblCategoria.font.withSize(22)
        cell.lblCategoria.sizeToFit()
        cell.lblCategoria.textAlignment = .center
        cell.lblCategoria.center.x = cell.frame.size.width / 2
        cell.lblCategoria.center.y = cell.frame.size.height / 2
        cell.lblCategoria.textColor = UIColor.white
        
        cell.lblCategoria.shadowColor = UIColor.black
        
        
        cell.bringSubview(toFront: cell.lblCategoria)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        let catActual = self.datosMain[indexPath.item]
        
        let anchoCollView = collectionView.frame.size.width - 20
        
        switch catActual.tipo {
        case "h":
            elTamaño = CGSize(width: anchoCollView, height: anchoCollView * 0.75)
            break
        case "v":
            elTamaño = CGSize(width: anchoCollView / 2.07, height: anchoCollView * 0.75)
            break
        default: break
        }
        
        return elTamaño
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        return UIEdgeInsets(top: self.insetTop + 5, left: 10, bottom: self.insetBottom, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Functions().isGuestUser(){
            self.mensajeSimple(mensaje: Constantes.guestData.mensajeMasTrendy, dismiss: false)
        } else {
            let catActual = self.datosMain[indexPath.item]
            let swipevc = SwipeViewControllerVC()
            swipevc.CatTrendy = catActual
            self.navigationController?.pushViewController(swipevc, animated: true)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
