//
//  ProductDetailVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 01/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import ImageSlideshow

class ProductDetailVC: UIViewController {
    
    var imageSlider : ImageSlideshow!
    var scrollViewBottom : UIScrollView!
    var lblPrecio : UILabel!
    var lblDescripcion : UILabel!
    var btnComprar : UIView!
    var btnArmario : UIView!
    var lblBtnArmario : UILabel!
    var titulo = ""
    var imgMarca : UIImageView!
    
    var itemActual : itemPrenda!
    
    var desdeMT : Bool = false //viene de lo mas trendy

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = self.itemActual.nombre
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.view.backgroundColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func getImageSources()-> [KingfisherSource] {
        var source = [KingfisherSource]()
        for item in self.itemActual.fotos {
            if let _ = item.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed){
                if let kfs = KingfisherSource(urlString: item) {
                    source.append(kfs)
                }
            }
        
           
        }
        return source
    }
    
    func buildInterface(){
        
        //nav button
        let btnShare = UIButton(type: .custom)
        btnShare.setImage(Asset.buttonShareWhite.image, for: .normal)
        btnShare.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShare.addTarget(self, action: #selector(self.shareProd), for: .touchUpInside)
        let itemR = UIBarButtonItem(customView: btnShare)
        self.navigationItem.setRightBarButton(itemR, animated: true)
        
        self.view.backgroundColor = UIColor.white
        var xframe = CGRect.zero
        //let posy = Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)! + 5
        //imageSlider
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla * 0.6)
        self.imageSlider = ImageSlideshow(frame: xframe)
        self.imageSlider.contentScaleMode = .scaleAspectFill
        self.imageSlider.setImageInputs(self.getImageSources())
        self.imageSlider.preload = .all
        self.imageSlider.activityIndicator = DefaultActivityIndicator()
        
        self.view.addSubview(self.imageSlider)
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        self.imageSlider.addGestureRecognizer(tapImage)
        
        //scrollview
        xframe.origin = CGPoint(x: 0, y: self.imageSlider.frame.maxY + 2)
        let altoScroll = Constantes.tamaños.altoPantalla - self.imageSlider.frame.size.height
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height:  altoScroll)
        self.scrollViewBottom = UIScrollView(frame: xframe)
        
        //precio
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: 100, height: 30)
        self.lblPrecio = UILabel(frame: xframe)
        if itemActual.conDesc {
            self.lblPrecio.attributedText = Functions().getStringPrecio(precio: itemActual.precio, descuento: itemActual.descuento)
        } else {
            let strprecio = "\(itemActual.precio!) €"
            self.lblPrecio.attributedText = NSAttributedString(string: strprecio)
        }
        self.lblPrecio.sizeToFit()
        self.scrollViewBottom.addSubview(self.lblPrecio)
        
        //imagen marca
        let altoImgMarca = Constantes.tamaños.anchoPantalla * 0.1
        let anchoImgMarca = altoImgMarca * 4
        let xposImgMarca = Constantes.tamaños.anchoPantalla - (anchoImgMarca + 10)
        xframe.origin = CGPoint(x: xposImgMarca, y: 20)
        xframe.size = CGSize(width: anchoImgMarca, height: altoImgMarca)
        self.imgMarca = UIImageView(frame: xframe)
        self.scrollViewBottom.addSubview(self.imgMarca)
        self.imgMarca.contentMode = .scaleAspectFit
        let urlImgMarca = URL(string: itemActual.marca)
        self.imgMarca.kf.setImage(with: urlImgMarca)
        self.imgMarca.kf.indicatorType = .activity
        self.imgMarca.center.y = self.lblPrecio.frame.minY + (self.lblPrecio.frame.size.height / 2)
        
        //descripcion
        xframe.origin = CGPoint(x: 10, y: self.lblPrecio.frame.maxY + 15)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.lblDescripcion = UILabel(frame: xframe)
        self.lblDescripcion.numberOfLines = 0
        self.lblDescripcion.textColor = UIColor.gray
        self.lblDescripcion.text = itemActual.detalleTexto
        self.lblDescripcion.sizeToFit()
        self.scrollViewBottom.addSubview(self.lblDescripcion)
        
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.17
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.9
        //boton comprar
        xframe.origin = CGPoint(x: 10, y: self.lblDescripcion.frame.maxY + 10)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnComprar = UIView(frame: xframe)
        self.btnComprar.backgroundColor = UIColor.lightGray
        self.btnComprar.layer.borderWidth = 0.5
        self.btnComprar.layer.borderColor = UIColor.black.cgColor
        self.btnComprar.layer.cornerRadius = 5
        //label
        xframe.size = CGSize(width: anchoBoton * 0.8, height: altoBoton)
        let labelBtnComprar = UILabel(frame: xframe)
        labelBtnComprar.text = "COMPRA ESTE PRODUCTO EN LA TIENDA"
        labelBtnComprar.adjustsFontSizeToFitWidth = true
        self.btnComprar.addSubview(labelBtnComprar)
        labelBtnComprar.center.x = self.btnComprar.frame.size.width / 2
        labelBtnComprar.center.y = self.btnComprar.frame.size.height / 2
        
        self.scrollViewBottom.addSubview(self.btnComprar)
        self.btnComprar.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //añadir a armario
        xframe.origin = CGPoint(x: 10, y: self.btnComprar.frame.maxY + 10)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnArmario = UIView(frame: xframe)
        self.btnArmario.backgroundColor = UIColor.white
        self.btnArmario.layer.borderWidth = 0.5
        self.btnArmario.layer.borderColor = UIColor.black.cgColor
        self.btnArmario.layer.cornerRadius = 5
        //label
        xframe.size = CGSize(width: anchoBoton * 0.5, height: altoBoton)
        self.lblBtnArmario = UILabel(frame: xframe)
        self.lblBtnArmario.text = (Functions().existeIdEnArmario(id_prenda: self.itemActual.id) ? Constantes.strings.strElimiarArmario : Constantes.strings.strAñadirArmario)
        self.btnArmario.addSubview(self.lblBtnArmario)
        self.adjustLabelArmario()
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        let sizeimg = self.btnArmario.frame.size.height * 0.5
        xframe.size = CGSize(width: sizeimg, height: sizeimg)
        let imgArmario = UIImageView(frame: xframe)
        imgArmario.contentMode = .scaleAspectFit
        imgArmario.image = Constantes.imagenes.imgFooterCloset
        self.btnArmario.addSubview(imgArmario)
        imgArmario.center.y  = self.btnArmario.frame.size.height / 2
        
        self.scrollViewBottom.addSubview(self.btnArmario)
        self.btnArmario.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //acciones
        let tapBtnArmario = UITapGestureRecognizer(target: self, action: #selector(self.AddRemoveFromCloset(sender:)))
        self.btnArmario.isUserInteractionEnabled = true
        self.btnArmario.addGestureRecognizer(tapBtnArmario)
        
        let tapBtnTienda = UITapGestureRecognizer(target: self, action: #selector(self.gotoTienda(sender:)))
        self.btnComprar.isUserInteractionEnabled = true
        self.btnComprar.addGestureRecognizer(tapBtnTienda)
        
        self.view.addSubview(self.scrollViewBottom)
        
        //LSS - CHECAR
        self.scrollViewBottom.contentSize.height = self.btnArmario.frame.maxY + 10 + (self.tabBarController?.tabBar.frame.size.height)!
        
        
        Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "FichaProducto", ridMarca: self.itemActual.idMarca){ exito, error in
            //
            
        }
        
        if self.desdeMT {
            Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "FichaProducto", ridMarca: "\(self.itemActual.id)"){ exito, error in
                //
                
            }
        }
    }
    
    func imageTapped(sender: UITapGestureRecognizer) {
        let fsg = FullScreenGalleryVC()
        fsg.titulo = self.itemActual.nombre
        fsg.fotos = self.imageSlider.images as! [KingfisherSource]
        self.navigationController?.pushViewController(fsg, animated: true)
    }
    
    func AddRemoveFromCloset(sender: UITapGestureRecognizer){
        
        if Functions().isGuestUser() {
            self.mensajeSimple(mensaje: Constantes.guestData.mensajeAgregarArmario, dismiss: false)
        } else {
            let msg = Functions().agregarQuitarDeArmario(id_prenda: self.itemActual.id, xjson: self.itemActual.json, idMarca: self.itemActual.idMarca)
            if msg != "" {
                print(msg)
            } else {
                if self.lblBtnArmario.text == Constantes.strings.strAñadirArmario {
                    self.lblBtnArmario.text = Constantes.strings.strElimiarArmario
                } else {
                    self.lblBtnArmario.text = Constantes.strings.strAñadirArmario
                }
                self.adjustLabelArmario()
            }
        }
        
    }
    
    func adjustLabelArmario(){
        self.lblBtnArmario.adjustsFontSizeToFitWidth = true
        self.lblBtnArmario.sizeToFit()
        self.lblBtnArmario.center.x = self.btnArmario.frame.size.width / 2
        self.lblBtnArmario.center.y = self.btnArmario.frame.size.height / 2
    }
    
    func gotoTienda(sender: UITapGestureRecognizer){
        
        Functions().itelligentInteraccionRegistrar(ttipoInteraccion: "ComprarProducto", ridMarca: self.itemActual.idMarca){ exito, error in
            //
            
        }
        
        let webviewvc = GeneralWebViewVC()
        webviewvc.URLParam = self.itemActual.urlTienda
        webviewvc.titulo = self.itemActual.nombre
        webviewvc.altoTabBar = (self.tabBarController?.tabBar.frame.size.height)!
        webviewvc.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)!
        
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.pushViewController(webviewvc, animated: true)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func shareProd(){
        let firstActivityItem = self.itemActual.urlTienda
        let secondActivityItem : NSURL = NSURL(string: self.itemActual.urlTienda)!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem!, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.unknown
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
