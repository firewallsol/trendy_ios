//
//  AllBrandsTableVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class AllBrandsTableVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate {
    
    var arrayTodasMarcas = [Marca_support]()
    var arrayMarcas = [Marca_support]()
    var filteredMarcas = [Marca_support]()
    var selectedIndexMarcas = [Int]()
    var tipoFiltro = ""
    
    var tableMarcas : UITableView!
    
    var cellName = "celdaMarcas"
    
    let notifBackAllBrands = Notification.Name("BackAllBrands")
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.returnKeyType = .done
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = "MARCAS"
        
        let rightbbitem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(self.aplicarCambios(sender:)))
        
        self.navigationItem.rightBarButtonItem = rightbbitem
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = ""
        
        let params = ["newIndex" : self.selectedIndexMarcas]
        
        NotificationCenter.default.post(name: self.notifBackAllBrands, object: params)
    }
    
    func filterMarcas(){
        self.arrayMarcas.removeAll()
        
        for item in self.arrayTodasMarcas {
            if (item.tipo == self.tipoFiltro) {
                self.arrayMarcas.append(item)
            }
        }
        self.tableMarcas.reloadData()
    }
    
    func aplicarCambios(sender: UIBarButtonItem){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func buildInterface(){
        let yposinicial = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.tableMarcas = UITableView(frame: self.view.frame)
        self.tableMarcas.frame.origin.y = yposinicial
        self.tableMarcas.register(UITableViewCell.self, forCellReuseIdentifier: self.cellName)
        self.view.addSubview(self.tableMarcas)
        self.tableMarcas.delegate = self
        self.tableMarcas.dataSource = self
        self.tableMarcas.tableFooterView = UIView(frame: CGRect.zero)
        let padding = (self.tabBarController?.tabBar.frame.size.height)! + yposinicial
        self.tableMarcas.contentInset = UIEdgeInsetsMake(0, 0, padding, 0)
        self.tableMarcas.tableHeaderView = self.searchController.searchBar
        
        
        
    }
    
    //tableview datasource y delegate 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive {
            return self.filteredMarcas.count
        } else {
            return self.arrayMarcas.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellName, for: indexPath) 
        cell.selectionStyle = .none
        cell.tintColor = Constantes.paletaColores.rojoTrendy
        var laMarca = Marca_support()
        if self.searchController.isActive {
            laMarca = self.filteredMarcas[indexPath.row]
        } else {
            laMarca = self.arrayMarcas[indexPath.row]
        }
        
        cell.textLabel?.text = laMarca.nombre
        
        if let _ = self.selectedIndexMarcas.index(of: Int(laMarca.id)!){
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        var laMarca = Marca_support()
        if self.searchController.isActive {
            laMarca = self.filteredMarcas[indexPath.row]
        } else {
            laMarca = self.arrayMarcas[indexPath.row]
        }
        
        if let index = self.selectedIndexMarcas.index(of: Int(laMarca.id)!){
            self.selectedIndexMarcas.remove(at: index)
            cell?.accessoryType = .none
        } else {
            self.selectedIndexMarcas.append(Int(laMarca.id)!)
            cell?.accessoryType = .checkmark
        }
    }
    
    //tableview datasource y delegate -- fin
    
    //search controller
    func updateSearchResults(for searchController: UISearchController) {
        self.filteredMarcas.removeAll(keepingCapacity: false)
        let texto = searchController.searchBar.text?.lowercased()
        for item in self.arrayMarcas {
            if item.nombre.lowercased().range(of: texto!) != nil {
                self.filteredMarcas.append(item)
            }
        }
        self.tableMarcas.reloadData()
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        self.tableMarcas.reloadData()
    }
    
    //search

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
