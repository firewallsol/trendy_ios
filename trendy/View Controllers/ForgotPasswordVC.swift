//
//  ForgotPasswordVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {
    
    var scrollView : UIScrollView!
    var txtCorreo : UITextField!
    var btnListo : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        Functions().borraTodoCierraSesion()
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.isTranslucent = false
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.stopLoaderAnimation()
    }
    
    func buildInterface(){
        self.title = "Recuperar Contraseña"
        self.view.backgroundColor = UIColor.white
        
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.7
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.15
        
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.9
        let altoCampo = Constantes.tamaños.anchoPantalla * 0.1
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //correo
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 30)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtCorreo = UITextField(frame: xframe)
        self.txtCorreo.placeholder = "Correo"
        self.scrollView.addSubview(self.txtCorreo)
        self.txtCorreo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtCorreo.keyboardType = .emailAddress
        self.txtCorreo.autocorrectionType = .no
        self.txtCorreo.autocapitalizationType = .none
        
        let borderCorreo = CALayer()
        borderCorreo.borderColor = UIColor.darkGray.cgColor
        borderCorreo.frame = CGRect(x: 0, y: self.txtCorreo.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtCorreo.frame.size.width, height: self.txtCorreo.frame.size.height)
        
        borderCorreo.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtCorreo.layer.addSublayer(borderCorreo)
        self.txtCorreo.layer.masksToBounds = true
        self.txtCorreo.delegate = self
        
        //label instrucciones
        xframe.origin = CGPoint(x: 10, y: self.txtCorreo.frame.maxY + 30)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        let lblInstrucciones = UILabel(frame: xframe)
        lblInstrucciones.numberOfLines = 0
        lblInstrucciones.text = Constantes.strings.textLabelRecuperarContra
        lblInstrucciones.sizeToFit()
        self.scrollView.addSubview(lblInstrucciones)
        lblInstrucciones.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //boton listo
        xframe.origin = CGPoint(x: 10, y: lblInstrucciones.frame.maxY + 20)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnListo = UIView(frame: xframe)
        self.btnListo.layer.cornerRadius = 5
        self.btnListo.layer.borderWidth = 0.5
        self.btnListo.layer.borderColor = UIColor.black.cgColor
        self.scrollView.addSubview(self.btnListo)
        self.btnListo.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //label btn
        let lblBtnSignUp = UILabel(frame: self.btnListo.frame)
        lblBtnSignUp.text = "LISTO"
        lblBtnSignUp.sizeToFit()
        self.scrollView.addSubview(lblBtnSignUp)
        lblBtnSignUp.center.x = self.btnListo.frame.minX + (self.btnListo.frame.size.width / 2)
        lblBtnSignUp.center.y = self.btnListo.frame.minY + (self.btnListo.frame.size.height / 2)
        
        //sombra
        
        self.btnListo.layer.shadowColor = UIColor.darkGray.cgColor
        self.btnListo.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.btnListo.layer.shadowOpacity = 1
        self.btnListo.layer.shadowRadius = 0.5
        let tapListo = UITapGestureRecognizer(target: self, action: #selector(self.enviaDatos))
        self.btnListo.isUserInteractionEnabled = true
        self.btnListo.addGestureRecognizer(tapListo)
        
        
        self.view.addSubview(self.scrollView)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func validaForma()->Bool{
        var mensaje = ""
        
        let strcorreo = self.txtCorreo.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        //correo
        if (strcorreo == ""){
            mensaje = "El campo \"Correo\", es obligatorio"
            
        }
        
        if mensaje == "" && !Functions().isValidEmailAddress(text: strcorreo!) {
            mensaje = "El correo electronico no es válido"
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    func enviaDatos(){
        if (!self.validaForma()){
            return
        }
        self.setLoaderAnimation()
        
        let strcorreo = self.txtCorreo.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Functions().forgotPasswordSVC(rcorreo: strcorreo!){ exito, msg, error in
            self.stopLoaderAnimation()
            var mensaje = "Se envio el correo de recuperación satisfactoriamente"
            if exito {
                if msg != "" {
                    mensaje = msg
                }
                let alert = UIAlertController(title: "TrendyAdvisor", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                    alert: UIAlertAction!) in
                    _ = self.navigationController?.popViewController(animated: true)
                })
                )
                
                self.present(alert, animated: true, completion: nil)
            } else {
                if let xerror = error {
                    print("Ocurrio un error \(xerror)")
                }
                mensaje = "Ocurrio un error al intentar enviar el correo, revise su conexión a internet"
                if msg != "" {
                    mensaje = msg
                }
                self.mensajeSimple(mensaje: mensaje)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
