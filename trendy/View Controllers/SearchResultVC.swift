//
//  SearchResultVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 13/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import DownPicker
import Kingfisher
import CollectionViewWaterfallLayout

class SearchResultVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CollectionViewWaterfallLayoutDelegate, UITextFieldDelegate {
    
    var containerHeader : UIView!
    var headerBar : UIView!
    var imageSearch : UIImageView!
    var txtSearch : UITextField!
    var txtPicker : UITextField!
    var pickerPersona : DownPicker!
    
    var collectionResult : UICollectionView!
    
    var datosCollectionx = [itemPrenda]()
    
    let cellIdentifier = "celdaDetalleCat"
    
    var tipoSexo = Sexo.Mujer
    
    var insetBottom : CGFloat = 0
    var altoNavBar : CGFloat = 0
    
    var pickerIndex = 0
    var stringSearch = ""
    
    var itemsCounter = 20 //siempre 20 en 20
    
    var isDataLoading = false
    
    var newSearch = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)!
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Búsqueda"
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
        
    }
    
    func loadData(){
        
        self.isDataLoading = true
        
        self.setLoaderAnimation()
        
        let elSexo =  Functions().getCurrentSexType(strSexo: self.txtPicker.text! ) 
        let elString = self.txtSearch.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Functions().getResultBusqueda(tipoSexo: elSexo, strBusqueda: elString!, indice: self.itemsCounter){arrayItems, xerror in
            if let xxerror = xerror {
                print("Ocurrio un error al cargar los datos \(xxerror)")
            } else {
                self.stopLoaderAnimation()
                if !arrayItems.isEmpty {
                    if self.newSearch {
                        self.datosCollectionx.removeAll()
                        self.newSearch = false
                    }
                    self.itemsCounter += 20
                    self.datosCollectionx.append(contentsOf: arrayItems)
                    let range = Range(uncheckedBounds: (0, self.collectionResult.numberOfSections))
                    let indexSet = IndexSet(integersIn: range)
                    self.collectionResult.performBatchUpdates({
                        self.collectionResult.reloadSections(indexSet)
                    }, completion: {(finished) -> Void in
                        self.isDataLoading = false
                    })
                    
                } else {
                    if self.datosCollectionx.count == 0 {
                        let alert = UIAlertController(title: "TrendyAdvisor", message: Constantes.strings.mensajeSinresultados, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if !(textField.text?.isEmpty)! {
            self.newSearch = true
            self.itemsCounter = 0
            self.loadData()
        }
        return true
    }
    
    func buildInterface(){
        self.view.backgroundColor = UIColor.white
        
        let altoContainerPrincipal = Constantes.tamaños.anchoPantalla * 0.22
        
        var xframe = CGRect.zero
        
        //container
        xframe.origin = CGPoint(x: 0, y: self.altoNavBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoContainerPrincipal)
        self.containerHeader = UIView(frame: xframe)
        self.containerHeader.backgroundColor = UIColor.white
        
        //container2
        xframe.origin = CGPoint(x: 10, y: 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: altoContainerPrincipal * 0.7)
        let containerST = UIView(frame: xframe)
        self.containerHeader.addSubview(containerST)
        containerST.center.y = self.containerHeader.frame.height / 2
        containerST.layer.borderColor = UIColor.gray.cgColor
        containerST.layer.borderWidth = 0.5
        containerST.layer.cornerRadius = 5
        
        
        //imagen
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size  = CGSize(width: containerST.frame.size.height * 0.5, height: containerST.frame.size.height * 0.5)
        self.imageSearch = UIImageView(frame: xframe)
        self.imageSearch.contentMode = .scaleAspectFit
        self.imageSearch.image = Constantes.imagenes.imgFooterSearch
        containerST.addSubview(self.imageSearch)
        self.imageSearch.center.y = containerST.frame.height / 2
        
        //textfield busqueda
        let anchoTextField = containerST.frame.size.width - (self.imageSearch.frame.size.width + 15)
        xframe.origin = CGPoint(x: imageSearch.frame.maxX + 5, y: 5)
        xframe.size = CGSize(width: anchoTextField, height: containerST.frame.size.height * 0.7)
        self.txtSearch = UITextField(frame: xframe)
        self.txtSearch.placeholder = "Buscar"
        containerST.addSubview(self.txtSearch)
        self.txtSearch.center.y = containerST.frame.size.height / 2
        
        let borderBuscar = CALayer()
        borderBuscar.borderColor = UIColor.lightGray.cgColor
        borderBuscar.frame = CGRect(x: 0, y: self.txtSearch.frame.size.height - (Constantes.tamaños.anchoTextFieldLine), width:  self.txtSearch.frame.size.width, height: self.txtSearch.frame.size.height)
        borderBuscar.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtSearch.layer.addSublayer(borderBuscar)
        self.txtSearch.layer.masksToBounds = true
        self.txtSearch.text = self.stringSearch
        self.txtSearch.clearButtonMode = .whileEditing
        
        //textfield picker
        let anchopicker = Constantes.tamaños.anchoPantalla - (containerST.frame.size.width + 20)
        
        xframe.origin = CGPoint(x: containerST.frame.maxX + 5, y: 5)
        xframe.size = CGSize(width: anchopicker, height: containerST.frame.size.height)
        self.txtPicker = UITextField(frame: xframe)
        self.txtPicker.layer.cornerRadius = 10
        self.containerHeader.addSubview(self.txtPicker)
        self.txtPicker.center.y = self.containerHeader.frame.height / 2
        self.txtPicker.backgroundColor = Constantes.paletaColores.rojoTrendy
        self.txtPicker.setLeftPaddingPoints(10)
        self.txtPicker.textColor = UIColor.white
        self.pickerPersona = DownPicker(textField: self.txtPicker, withData: Constantes.strings.arrayMHN)
        self.pickerPersona.setValueAt(self.pickerIndex)
        self.view.addSubview(self.containerHeader)
        
        //flecha del picker
        let button = UIButton(type: .custom)
        button.setImage(Asset.icArrowDownW.image, for: .normal)
        let anchoImg = self.txtPicker.frame.size.height * 0.15
        let altoImg = self.txtPicker.frame.size.height * 0.3
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 5)
        button.frame = CGRect(x: CGFloat(self.txtPicker.frame.size.width - anchoImg), y: CGFloat(5), width: anchoImg, height: altoImg)
        self.txtPicker.rightView = button
        self.txtPicker.rightViewMode = .always
        
        //collection view
        let layout = CollectionViewWaterfallLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: ((self.tabBarController?.tabBar.frame.size.height)! * 2) + 30, right: 10)
        layout.columnCount = 2
        let ypos = self.containerHeader.frame.maxY + 5
        xframe = CGRect.zero
        
        xframe.origin = CGPoint(x: 0, y: ypos)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: self.view.frame.height)
        self.collectionResult = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionResult.dataSource = self
        self.collectionResult.delegate = self
        self.collectionResult.backgroundColor = UIColor.white
        
        self.collectionResult.register(CeldaDetalleCat.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.collectionResult)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosCollectionx.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaDetalleCat
        cell.backgroundColor = UIColor.white
        
        var xframe = CGRect.zero
        xframe = cell.frame
        cell.container.frame = xframe
        cell.container.layer.cornerRadius = 10
        cell.container.center.y = cell.frame.size.height / 2
        cell.container.center.x = cell.frame.size.width / 2
        
        let itemActual = self.datosCollectionx[indexPath.item]
        
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.9, height: cell.container.frame.size.height * 0.75)
        cell.imagen.frame = xframe
        cell.imagen.center.x = cell.frame.size.width / 2
        cell.imagen.contentMode = .scaleToFill
        let urlImg = URL(string:itemActual.foto)
        //cell.imagen.kf.setImage(with: urlImg, options: [.transition(.fade(0.2))])
        cell.imagen.kf.setImage(with: urlImg)
        cell.imagen.setNeedsDisplay()
        cell.imagen.center.x = cell.container.frame.size.width / 2
        
        //titulo
        xframe.origin = CGPoint(x: 5, y: cell.imagen.frame.maxY + 7)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblTitulo.frame = xframe
        cell.lblTitulo.text = itemActual.nombreMarca
        cell.lblTitulo.font = UIFont.boldSystemFont(ofSize: cell.lblTitulo.font.pointSize)
        cell.lblTitulo.adjustsFontSizeToFitWidth = true
        //cell.lblTitulo.sizeToFit()
        
        
        //precio
        xframe.origin = CGPoint(x: 5, y: cell.lblTitulo.frame.maxY + 4)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblPrecio.frame = xframe
        if #available(iOS 8.2, *) {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightThin)
        } else {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15)
        }
        
        if itemActual.conDesc {
            cell.lblPrecio.attributedText = Functions().getStringPrecio(precio: itemActual.precio, descuento: itemActual.descuento)
        } else {
            
            let strprecio = "\(itemActual.precio!) €"
            cell.lblPrecio.attributedText = NSAttributedString(string: strprecio)
        }
        
        cell.lblPrecio.adjustsFontSizeToFitWidth = true
        
        
        //like
        let anchoAltoLike = cell.container.frame.size.width * 0.22
        let yposlike = cell.imagen.frame.maxY + (((cell.container.frame.size.height - cell.imagen.frame.maxY) / 2) - (anchoAltoLike / 2) )
        let xposlike = cell.container.frame.size.width - (anchoAltoLike + 10)
        
        xframe.origin = CGPoint(x: xposlike, y: yposlike)
        xframe.size = CGSize(width: anchoAltoLike, height: anchoAltoLike)
        cell.imgLike.removeFromSuperview()
        cell.imgLike = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgLike)
        cell.imgLike.image = (Functions().existeIdEnArmario(id_prenda: itemActual.id) ? Asset.icHeartPressed.image : Asset.icHeart.image)
        cell.imgLike.setNeedsDisplay()
        let tapImgLike = UITapGestureRecognizer(target: self, action: #selector(self.addToCloset(sender:)))
        cell.imgLike.isUserInteractionEnabled = true
        cell.imgLike.addGestureRecognizer(tapImgLike)
        cell.imgLike.tag = indexPath.item
        
        //descuento
        let anchoAltoDesc = cell.container.frame.size.width * 0.3
        let yposDesc = cell.imagen.frame.minY
        let xposDesc = cell.imagen.frame.maxX - (anchoAltoDesc + 5)
        cell.imgDescuento.removeFromSuperview()
        xframe.origin = CGPoint(x: xposDesc, y: yposDesc)
        xframe.size = CGSize(width: anchoAltoDesc, height: anchoAltoDesc)
        cell.imgDescuento = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgDescuento)
        cell.imgDescuento.image = Asset.icDiscount.image
        cell.imgDescuento.setNeedsDisplay()
        
        //lbl descuento
        cell.lblDesc.removeFromSuperview()
        cell.lblDesc = UILabel(frame: cell.imgDescuento.frame)
        cell.imgDescuento.addSubview(cell.lblDesc)
        cell.lblDesc.numberOfLines = 2
        cell.lblDesc.textColor = UIColor.white
        cell.lblDesc.textAlignment = .center
        cell.lblDesc.text = ""
        cell.lblDesc.font = UIFont.systemFont(ofSize: cell.lblDesc.font.pointSize - 2)
        
        if itemActual.conDesc {
            cell.imgDescuento.isHidden = false
            cell.lblDesc.isHidden = false
            cell.lblDesc.text = Functions().getStringDescuento(precio: itemActual.precio, descuento: itemActual.descuento)
            cell.lblDesc.sizeToFit()
            cell.lblDesc.center.x = cell.imgDescuento.frame.size.width / 2
            cell.lblDesc.center.y = cell.imgDescuento.frame.size.height / 2
        } else {
            cell.imgDescuento.isHidden = true
            cell.lblDesc.isHidden = true
        }
        
        cell.dropShadow()
        
        cell.setNeedsDisplay()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        
        let itemActual = self.datosCollectionx[indexPath.item]
        
        var elTamaño = CGSize.zero
        let anchoCollView = (collectionView.frame.size.width) / 2 //2.08
        let anchoCelda = anchoCollView - 20
        
        let tamañoImagen = Functions().scaleImageSize(originalSize: CGSize(width: itemActual.foto_ancho, height: itemActual.foto_alto), scaledToWidth: anchoCelda * 0.9)
        
        var alto : CGFloat = 0
        
        if itemActual.foto_ancho < itemActual.foto_alto {
            alto = tamañoImagen.height + (tamañoImagen.height * 0.1) + 20
        } else {
            alto = tamañoImagen.height + (tamañoImagen.height * 0.35) + 50
        }
        
        elTamaño.height = max(alto, 225)
        
        elTamaño.width = anchoCelda
        
        //print("tamaño de imagen: \(itemActual.foto_ancho), \(itemActual.foto_alto)")
        //print("tamaño de celda: \(elTamaño)")
        
        return elTamaño
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detalle = ProductDetailVC()
        let itemActual = self.datosCollectionx[indexPath.item]
        detalle.itemActual = itemActual
        self.navigationController?.pushViewController(detalle, animated: true)
        detalle.buildInterface()
    }
    
    func addToCloset(sender: UITapGestureRecognizer){
        
        if Functions().isGuestUser() {
            self.mensajeSimple(mensaje: Constantes.guestData.mensajeAgregarArmario, dismiss: false)
        } else {
            let itemIndex = sender.view?.tag
            let msg = Functions().agregarQuitarDeArmario(id_prenda: self.datosCollectionx[itemIndex!].id, xjson: self.datosCollectionx[itemIndex!].json, idMarca: self.datosCollectionx[itemIndex!].idMarca)
            if msg != "" {
                print(msg)
            } else {
                let elImageView = sender.view as! UIImageView
                if (elImageView.image?.isEqual(Asset.icHeartPressed.image))!{
                    elImageView.image = Asset.icHeart.image
                } else {
                    elImageView.image = Asset.icHeartPressed.image
                }
                
            }
        }
        
    }
    
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        
        if ((self.collectionResult.contentOffset.y + self.collectionResult.frame.size.height) > ((self.collectionResult.contentSize.height / 3) * 2))
        {
            if !self.isDataLoading{
                self.loadData()
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
