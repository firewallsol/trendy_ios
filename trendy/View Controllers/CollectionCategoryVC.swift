//
//  CollectionCategoryVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CollectionViewWaterfallLayout

class CollectionCategoryVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CollectionViewWaterfallLayoutDelegate {
    
    var collectionCategory : UICollectionView!
    
    var datosCollectionx = [itemPrenda]()
    
    let cellIdentifier = "celdaDetalleCat"
    
    var tipoSexo = Sexo.Mujer
    
    var itemSubcategoria : SubCategoria!
    var itemCategoria : Categoria!

    var senderVC = senderAdvancedFilter.search
    
    var selectedIndexPath = IndexPath()
    
    var itemsCounter = 0 //siempre 20 en 20
    
    var isDataLoading = false
    
    var fromAdvancedFilter = false
    
    let notifFiltroCollection = Notification.Name("FiltroCollection")
    
    var strSexo : String!
    var strTienda : String? = nil
    var strMarca : String? = nil
    var strCategoria : String? = nil
    var strColor : String? = nil
    var strPrecios : String!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.buildInterface()
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.aplicaFiltro(notification:)), name: self.notifFiltroCollection, object: nil)
        
        if self.fromAdvancedFilter {
            self.collectionCategory.reloadData()
            self.stopLoaderAnimation()
        } else {
            self.loadData()
        }
    }
    
    func loadData(){
        self.setLoaderAnimation()
        
        self.isDataLoading = true
        
        if self.fromAdvancedFilter {
            
            Functions().getItemsFiltrovanzado(sexo: self.strSexo, marcas: self.strMarca, categorias: self.strCategoria, color: self.strColor, precios: self.strPrecios, tiendas: self.strTienda, index: self.itemsCounter){ items, error in
                if let xerror = error {
                    print("Ha ocurrido un problema: \(xerror)")
                } else {
                    if !items.isEmpty {
                        self.itemsCounter += 20
                        self.datosCollectionx.append(contentsOf: items)
                        let range = Range(uncheckedBounds: (0, self.collectionCategory.numberOfSections))
                        let indexSet = IndexSet(integersIn: range)
                        self.collectionCategory.performBatchUpdates({
                            self.collectionCategory.reloadSections(indexSet)
                        }, completion: {(finished) -> Void in
                            self.isDataLoading = false
                        })
                        
                    }
                    
                    if self.isLoaderAnimating(){
                        UIView.animate(withDuration: 0.3, animations: {
                            self.stopLoaderAnimation()
                        })
                    }
                    
                } // fin else error
            } // fin Functions().getItemsFiltrovanzado
        
        } else {
            
            var id_item = 0
            switch self.senderVC {
            case .categoria:
                id_item = itemCategoria.id
                break
            case .subcategoria:
                id_item = itemSubcategoria.id
                break
            default: break
                
            }
            
            Functions().getCollectionSubCategoriaItems(id_subcat: id_item, tipoSexo: self.tipoSexo, index: self.itemsCounter){
                items, error in
                if let xerror = error {
                    print("Ha ocurrido un problema: \(xerror)")
                } else {
                    if !items.isEmpty {
                        self.itemsCounter += 20
                        self.datosCollectionx.append(contentsOf: items)
                        let range = Range(uncheckedBounds: (0, self.collectionCategory.numberOfSections))
                        let indexSet = IndexSet(integersIn: range)
                        self.collectionCategory.performBatchUpdates({
                            self.collectionCategory.reloadSections(indexSet)
                        }, completion: {(finished) -> Void in
                            self.isDataLoading = false
                        })
                        
                    }
                    
                    if self.isLoaderAnimating(){
                        UIView.animate(withDuration: 0.3, animations: {
                            self.stopLoaderAnimation()
                        })
                    }
                    
                } //fin else error
            } // fin Functions().getCollectionSubCategoriaItems
        } //fin else if from advance filter
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        var titulo = ""
        if let _ = self.itemSubcategoria {
            titulo = self.itemSubcategoria.nombre
        } else if let _ = self.itemCategoria {
            titulo = self.itemCategoria.nombre
        }
        
        self.navigationItem.title = titulo
        
        let rightbbitem = UIBarButtonItem(image: Asset.icFilterWhite.image, style: .plain, target: self, action: #selector(self.gotoAdvancedFilter))
        rightbbitem.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightbbitem
        
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
    }
    
    func gotoAdvancedFilter(){
        let elVC = AdvancedFilterVC()
        elVC.searchSex = self.tipoSexo
        
        elVC.senderVC = self.senderVC
        var id_item = 0
        switch self.senderVC {
        case .categoria:
            id_item = itemCategoria.id
            break
        case .subcategoria:
            id_item = itemSubcategoria.id
            break
        case .advancedfilter:
            elVC.strMarca = self.strMarca
            elVC.strCategoria = self.strCategoria
            elVC.strColor = self.strColor
            elVC.strPrecios = self.strPrecios
            elVC.strTienda = self.strTienda
            
            break
        default: break
            
        }
        
        elVC.idCatDefault = id_item
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func aplicaFiltro(notification: NSNotification){
        let _ = self.navigationController?.popViewController(animated: true)
        let paramFiltro = notification.object as! NSDictionary
        
        let strSexo = paramFiltro["sexo"] as! String
        let strTienda = paramFiltro["tiendas"] as! String
        let strMarca = paramFiltro["marca"] as! String
        let strCategoria = paramFiltro["categoria"] as! String
        let strColor = paramFiltro["color"] as! String
        let strPrecios = paramFiltro["precios"] as! String
        let items = paramFiltro["items"] as! [itemPrenda]
        
        //ahora va a collection category
        self.datosCollectionx = items
        self.strSexo = strSexo
        self.strMarca = (strMarca == "" ? nil : strMarca)
        self.strCategoria = (strCategoria == "" ? nil : strCategoria)
        self.strColor = (strColor == "" ? nil : strColor)
        self.strPrecios = strPrecios
        self.strTienda = (strTienda == "" ? nil : strTienda)
        self.fromAdvancedFilter = true
        self.itemsCounter = 20
        self.tipoSexo = Sexo(rawValue: Int(strSexo)!)!
        self.senderVC = .advancedfilter
        self.collectionCategory.reloadData()
    }
    
    func buildInterface(){
        
        let layout = CollectionViewWaterfallLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: ((self.tabBarController?.tabBar.frame.size.height)! * 3), right: 10)
        layout.columnCount = 2
        let ypos = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        var xframe = CGRect.zero
        
        xframe.origin = CGPoint(x: 0, y: ypos)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: self.view.frame.height)
        self.collectionCategory = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionCategory.dataSource = self
        self.collectionCategory.delegate = self
        self.collectionCategory.backgroundColor = UIColor.white
        
        self.collectionCategory.register(CeldaDetalleCat.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.collectionCategory)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosCollectionx.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaDetalleCat
        cell.backgroundColor = UIColor.white
        
        var xframe = CGRect.zero
        xframe = cell.frame
        cell.container.frame = xframe
        cell.container.layer.cornerRadius = 10
        cell.container.center.y = cell.frame.size.height / 2
        cell.container.center.x = cell.frame.size.width / 2
        
        var itemActual = itemPrenda()
        if  !datosCollectionx.isEmpty {
            itemActual = self.datosCollectionx[indexPath.item]
        } else {
            //validar si esta parte es necesaria
            itemActual.foto = ""
            itemActual.nombreMarca = ""
            itemActual.precio = "0"
            itemActual.descuento = "0"
            itemActual.id = 0
        }

        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.9, height: cell.container.frame.size.height * 0.75)
        cell.imagen.frame = xframe
        cell.imagen.center.x = cell.frame.size.width / 2
        cell.imagen.contentMode = .scaleToFill
        let urlImg = URL(string:itemActual.foto)
        cell.imagen.kf.indicatorType = .activity
        //cell.imagen.kf.setImage(with: urlImg, options: [.transition(.fade(0.2))])
        cell.imagen.kf.setImage(with: urlImg)
        cell.imagen.setNeedsDisplay()
        cell.imagen.center.x = cell.container.frame.size.width / 2
        
        //titulo
        xframe.origin = CGPoint(x: 5, y: cell.imagen.frame.maxY + 7)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblTitulo.frame = xframe
        cell.lblTitulo.text = itemActual.nombreMarca
        cell.lblTitulo.font = UIFont.boldSystemFont(ofSize: cell.lblTitulo.font.pointSize)
        cell.lblTitulo.adjustsFontSizeToFitWidth = true
        //cell.lblTitulo.sizeToFit()
        
        
        //precio
        xframe.origin = CGPoint(x: 5, y: cell.lblTitulo.frame.maxY + 4)
        xframe.size = CGSize(width: cell.container.frame.size.width * 0.6, height: 20)
        cell.lblPrecio.frame = xframe
        if #available(iOS 8.2, *) {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightThin)
        } else {
            cell.lblPrecio.font = UIFont.systemFont(ofSize: 15)
        }
        if itemActual.conDesc {
            cell.lblPrecio.attributedText = Functions().getStringPrecio(precio: itemActual.precio, descuento: itemActual.descuento)
        } else {
            
            let strprecio = "\(itemActual.precio!) €"
            cell.lblPrecio.attributedText = NSAttributedString(string: strprecio)
        }
        
        cell.lblPrecio.adjustsFontSizeToFitWidth = true
        
        
        //like
        let anchoAltoLike = cell.container.frame.size.width * 0.22
        let yposlike = cell.imagen.frame.maxY + (((cell.container.frame.size.height - cell.imagen.frame.maxY) / 2) - (anchoAltoLike / 2) )
        let xposlike = cell.container.frame.size.width - (anchoAltoLike + 10)
        
        xframe.origin = CGPoint(x: xposlike, y: yposlike)
        xframe.size = CGSize(width: anchoAltoLike, height: anchoAltoLike)
        cell.imgLike.removeFromSuperview()
        cell.imgLike = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgLike)
        cell.imgLike.image = (Functions().existeIdEnArmario(id_prenda: itemActual.id) ? Asset.icHeartPressed.image : Asset.icHeart.image)
        cell.imgLike.setNeedsDisplay()
        let tapImgLike = UITapGestureRecognizer(target: self, action: #selector(self.addToCloset(sender:)))
        cell.imgLike.isUserInteractionEnabled = true
        cell.imgLike.addGestureRecognizer(tapImgLike)
        cell.imgLike.tag = indexPath.item
        
        //descuento
        let anchoAltoDesc = cell.container.frame.size.width * 0.3
        let yposDesc = cell.imagen.frame.minY
        let xposDesc = cell.imagen.frame.maxX - (anchoAltoDesc + 5)
        cell.imgDescuento.removeFromSuperview()
        xframe.origin = CGPoint(x: xposDesc, y: yposDesc)
        xframe.size = CGSize(width: anchoAltoDesc, height: anchoAltoDesc)
        cell.imgDescuento = UIImageView(frame: xframe)
        cell.container.addSubview(cell.imgDescuento)
        cell.imgDescuento.image = Asset.icDiscount.image
        cell.imgDescuento.setNeedsDisplay()
        
        //lbl descuento
        cell.lblDesc.removeFromSuperview()
        cell.lblDesc = UILabel(frame: cell.imgDescuento.frame)
        cell.imgDescuento.addSubview(cell.lblDesc)
        cell.lblDesc.numberOfLines = 2
        cell.lblDesc.textColor = UIColor.white
        cell.lblDesc.textAlignment = .center
        cell.lblDesc.text = ""
        cell.lblDesc.font = UIFont.systemFont(ofSize: cell.lblDesc.font.pointSize - 2)
        
        if itemActual.conDesc {
            cell.imgDescuento.isHidden = false
            cell.lblDesc.isHidden = false
            cell.lblDesc.text = Functions().getStringDescuento(precio: itemActual.precio, descuento: itemActual.descuento)
            cell.lblDesc.sizeToFit()
            cell.lblDesc.center.x = cell.imgDescuento.frame.size.width / 2
            cell.lblDesc.center.y = cell.imgDescuento.frame.size.height / 2
        } else {
            cell.imgDescuento.isHidden = true
            cell.lblDesc.isHidden = true
        }
        
        cell.dropShadow()
        
        cell.setNeedsDisplay()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        let anchoCollView = (collectionView.frame.size.width) / 2 //2.08
        let anchoCelda = anchoCollView - 20
        
        if self.datosCollectionx.isEmpty {
            elTamaño.width = anchoCelda
            elTamaño.height = 225
        } else {
            let itemActual = self.datosCollectionx[indexPath.item]
            
            let tamañoImagen = Functions().scaleImageSize(originalSize: CGSize(width: itemActual.foto_ancho, height: itemActual.foto_alto), scaledToWidth: anchoCelda * 0.9)
            
            var alto : CGFloat = 0
            
            if itemActual.foto_ancho < itemActual.foto_alto {
                alto = tamañoImagen.height + (tamañoImagen.height * 0.1) + 20
            } else {
                alto = tamañoImagen.height + (tamañoImagen.height * 0.35) + 50
            }
            
            elTamaño.height = max(alto, 225)
            
            elTamaño.width = anchoCelda
        }
        
        return elTamaño
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detalle = ProductDetailVC()
        let itemActual = self.datosCollectionx[indexPath.item]
        detalle.itemActual = itemActual
        self.navigationController?.pushViewController(detalle, animated: true)
        detalle.buildInterface()
    }
    
    func addToCloset(sender: UITapGestureRecognizer){
        
        if Functions().isGuestUser() {
            self.mensajeSimple(mensaje: Constantes.guestData.mensajeAgregarArmario, dismiss: false)
        } else {
            let itemIndex = sender.view?.tag
            let msg = Functions().agregarQuitarDeArmario(id_prenda: self.datosCollectionx[itemIndex!].id, xjson: self.datosCollectionx[itemIndex!].json, idMarca: self.datosCollectionx[itemIndex!].idMarca)
            if msg != "" {
                print(msg)
            } else {
                let elImageView = sender.view as! UIImageView
                if (elImageView.image?.isEqual(Asset.icHeartPressed.image))!{
                    elImageView.image = Asset.icHeart.image
                } else {
                    elImageView.image = Asset.icHeartPressed.image
                }
                
            }
        }
        
    }
    
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        
        if ((self.collectionCategory.contentOffset.y + self.collectionCategory.frame.size.height) > ((self.collectionCategory.contentSize.height / 3) * 2))
        {
            if !self.isDataLoading{
                self.loadData()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension UIView {
    
    func dropShadow(scale: Bool = true) {
        
        self.layer.cornerRadius = 10
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        self.layer.shadowRadius = 4.0
        
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
