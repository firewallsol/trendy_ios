//
//  GeneralWebViewVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 09/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import WebKit

class GeneralWebViewVC: UIViewController, WKNavigationDelegate {
    
    var URLParam : String!
    var webV : WKWebView!
    var titulo = ""
    var altoNavBar : CGFloat = 0
    var altoTabBar : CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = ""
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func setWebView(){
        let url = URL (string: self.URLParam)
        let requestObj = URLRequest(url: url!)
        
        self.webV = WKWebView(frame: self.view.frame)
        self.webV.navigationDelegate = self
        self.view.addSubview(self.webV)
        let posy = Constantes.tamaños.altoStatusBar + self.altoNavBar
        self.webV.frame.origin.y = posy
        //LSS - 21062017 se quita esta validacion, siempre va a cortar el tamaño del webview
        //if (self.titulo == "Privacidad"){
            self.webV.frame.size.height -= (self.altoTabBar + posy)
        //}
        self.webV.load(requestObj)
        
        self.webV.addObserver(self, forKeyPath: #keyPath(WKWebView.loading), options: .new, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = self.titulo
        self.setWebView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setLoaderAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.webV.removeObserver(self, forKeyPath: #keyPath(WKWebView.loading))
        
        self.stopLoaderAnimation()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if self.isLoaderAnimating() {
            UIView.animate(withDuration: 0.3, animations: {
                self.stopLoaderAnimation()
            })
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard (object as? WKWebView) != nil else {return}
        guard let keyPath = keyPath else {return}
        guard let change = change else {return}
        switch keyPath {
        case "loading": // new:1 or 0
            if let val = change[.newKey] as? Bool {
                if val {
                    if !self.isLoaderAnimating(){
                        self.setLoaderAnimation()
                    }
                } else {
                    if self.webV.estimatedProgress == 1 {
                        if self.isLoaderAnimating() {
                            UIView.animate(withDuration: 0.3, animations: {
                                self.stopLoaderAnimation()
                            })
                        }
                    }
                }
            }
            break
        default: break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
