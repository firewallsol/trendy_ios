//
//  SearchSubCategoryVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher

class SearchSubCategoryVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var collectionSubCat : UICollectionView!
    
    var datosCollection : [SubCategoria]! = nil
    
    let cellIdentifier = "celdaMain"
    
    var headerBar : UIView!
    
    var titleHeader = ""
    
    var insetBottom : CGFloat = 0
    
    var laCategoria : Categoria!
    
    var tipoSexo = Sexo.Mujer

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = self.laCategoria.nombre
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)! + 15
        
        self.view.backgroundColor = UIColor.black
        
        self.title = self.laCategoria.nombre
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        let posy = Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)!
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 0, y: posy + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: self.view.frame.height)
        self.collectionSubCat = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionSubCat.dataSource = self
        self.collectionSubCat.delegate = self
        self.collectionSubCat.backgroundColor = UIColor.white
        
        self.collectionSubCat.register(CeldaMainCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.collectionSubCat)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaMainCell
        cell.backgroundView?.layer.cornerRadius = 10
        
        let subCat = self.datosCollection[indexPath.item]
        let urlImg = URL(string: subCat.imagen)
        cell.imagenFondo.kf.indicatorType = .activity
        cell.imagenFondo.kf.setImage(with: urlImg)
        cell.imagenFondo.contentMode = .scaleAspectFill
        cell.imagenFondo.center.x = cell.frame.size.width / 2
        cell.imagenFondo.center.y = cell.frame.size.height / 2
        cell.imagenFondo.clipsToBounds = true
        cell.imagenFondo.setNeedsDisplay()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        
        let anchoCollView = collectionView.frame.size.width - 20
        
        elTamaño = CGSize(width: anchoCollView, height: Constantes.tamaños.anchoPantalla * 0.25)
        
        return elTamaño
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: self.insetBottom, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let subcat = self.datosCollection[indexPath.item]
        let collectionCat = CollectionCategoryVC()
        collectionCat.senderVC = senderAdvancedFilter.subcategoria
        collectionCat.itemSubcategoria = subcat
        collectionCat.tipoSexo = self.tipoSexo
        self.navigationController?.pushViewController(collectionCat, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
