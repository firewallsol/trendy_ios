//
//  CoverScreenVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import SwiftyJSON

class CoverScreenVC: UIViewController {
    
    var btnFacebook : UIView!
    var btnRegistro : UIView!
    var btnLogin : UIView!
    var logo : UIImageView!
    
    var btnInvitado : UIView!
    
    var navBarHeight : CGFloat = 0
    
    var isInterfaceBuild = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /*
        //fondo transparente pero con el borde del nav bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        
        //agregar estas para quitar el borde del nav bar
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = .clear
        
        self.navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
         */
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if Functions().isUserLogged() {
            self.gotoMain(sender: UITapGestureRecognizer())
        } else {
            if !self.isInterfaceBuild {
                self.buildInterface()
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        self.stopLoaderAnimation()
    }
    
    func buildInterface(){
        
        self.title = ""
        self.navigationController?.view.backgroundColor = UIColor.black
        
        let imagenFondo = UIImage(named: "\(self.randRange(lower: 1, upper: 6))")
        let imageViewFondo = UIImageView(frame: CGRect(x: 0, y: 0, width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla))
        imageViewFondo.image = imagenFondo
        imageViewFondo.contentMode = .scaleAspectFill
        self.view.addSubview(imageViewFondo)
        
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.7
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.15
        
        var xframe = CGRect.zero
        //label titulo
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: altoBoton * 1.2)
        self.logo = UIImageView(frame: xframe)
        self.logo.image = Asset.trendyAdvisorLogo.image
        self.logo.contentMode = .scaleAspectFill
        self.view.addSubview(self.logo)
        self.logo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.logo.center.y = (Constantes.tamaños.altoPantalla / 2) - (self.navBarHeight + (Constantes.tamaños.altoStatusBar * 2))
        
        //label condiciones de politica de privacidad
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 20)
        let lblcondiciones = UILabel(frame: xframe)
        //self.view.addSubview(lblcondiciones)
        lblcondiciones.text = "Condiciones de Servicio y Política de Privacidad"
        lblcondiciones.textColor = UIColor.white
        lblcondiciones.adjustsFontSizeToFitWidth = true
        
        lblcondiciones.center.x = Constantes.tamaños.anchoPantalla / 2
        lblcondiciones.frame.origin.y = Constantes.tamaños.altoPantalla - (lblcondiciones.frame.size.height + 10)
        
        //boton email
        xframe.origin = CGPoint(x: 10, y: self.logo.frame.maxY + 30)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnRegistro = UIView(frame: xframe)
        self.btnRegistro.backgroundColor = UIColor.white
        self.btnRegistro.layer.cornerRadius = 5
        self.btnRegistro.center.x = Constantes.tamaños.anchoPantalla / 2
        self.btnRegistro.center.y = (self.logo.frame.maxY + lblcondiciones.frame.minY) / 2
        self.view.addSubview(self.btnRegistro)
        //label
        let lblBtnRegistro = UILabel(frame: xframe)
        lblBtnRegistro.text = "Únete con email"
        lblBtnRegistro.sizeToFit()
        lblBtnRegistro.textColor = UIColor.black
        self.btnRegistro.addSubview(lblBtnRegistro)
        lblBtnRegistro.center.x = self.btnRegistro.frame.size.width / 2
        lblBtnRegistro.center.y = self.btnRegistro.frame.size.height / 2
        
        //tap registro
        let tapRegistro = UITapGestureRecognizer(target: self, action: #selector(self.gotoRegistro(sender:)))
        self.btnRegistro.isUserInteractionEnabled = true
        self.btnRegistro.addGestureRecognizer(tapRegistro)
        
        //boton facebook
        xframe.origin = CGPoint(x: 10, y: self.logo.frame.maxY + 30)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnFacebook = UIView(frame: xframe)
        self.btnFacebook.backgroundColor = Constantes.paletaColores.azulBtnFacebook
        self.btnFacebook.layer.cornerRadius = 5
        self.btnFacebook.center.x = Constantes.tamaños.anchoPantalla / 2
        self.btnFacebook.frame.origin.y = self.btnRegistro.frame.minY - 10 - self.btnFacebook.frame.size.height
        self.view.addSubview(self.btnFacebook)
        //label
        let lblBtnFacebook = UILabel(frame: xframe)
        lblBtnFacebook.text = "Entrar con Facebook"
        lblBtnFacebook.sizeToFit()
        lblBtnFacebook.textColor = UIColor.white
        self.btnFacebook.addSubview(lblBtnFacebook)
        lblBtnFacebook.center.x = self.btnFacebook.frame.size.width / 2
        lblBtnFacebook.center.y = self.btnFacebook.frame.size.height / 2
        
        let tapBtnFacebook = UITapGestureRecognizer(target: self, action: #selector(self.LoginFacebook(sender:)))
        self.btnFacebook.isUserInteractionEnabled = true
        self.btnFacebook.addGestureRecognizer(tapBtnFacebook)

        
        //label iniciar sesion
        xframe.origin = CGPoint(x: 10, y: btnRegistro.frame.maxY + 20)
        xframe.size = CGSize(width: anchoBoton, height: (altoBoton / 2))
        self.btnLogin = UIView(frame: xframe)
        self.btnLogin.backgroundColor = UIColor.clear
        self.btnLogin.center.x = Constantes.tamaños.anchoPantalla / 2
        self.view.addSubview(self.btnLogin)
        //label
        let lblBtnLogin = UILabel(frame: xframe)
        lblBtnLogin.text = "Iniciar sesión"
        lblBtnLogin.sizeToFit()
        lblBtnLogin.textColor = UIColor.white
        self.btnLogin.addSubview(lblBtnLogin)
        lblBtnLogin.center.x = self.btnLogin.frame.size.width / 2
        lblBtnLogin.center.y = self.btnLogin.frame.size.height / 2
        let tapBtnLogin = UITapGestureRecognizer(target: self, action: #selector(self.gotoLogin(sender:)))
        self.btnLogin.isUserInteractionEnabled = true
        self.btnLogin.addGestureRecognizer(tapBtnLogin)
        
        //boton invitado
        xframe.origin = CGPoint(x: 10, y: self.btnLogin.frame.maxY + 15)
        xframe.size = CGSize(width: anchoBoton, height: (altoBoton / 2))
        self.btnInvitado = UIView(frame: xframe)
        self.btnInvitado.backgroundColor = UIColor.clear
        self.btnInvitado.center.x = Constantes.tamaños.anchoPantalla / 2
        self.view.addSubview(self.btnInvitado)
        //label
        let lblBtnInvitado = UILabel(frame: xframe)
        lblBtnInvitado.text = "Ingresar como invitado"
        lblBtnInvitado.sizeToFit()
        lblBtnInvitado.textColor = UIColor.white
        self.btnInvitado.addSubview(lblBtnInvitado)
        lblBtnInvitado.center.x = self.btnInvitado.frame.size.width / 2
        lblBtnInvitado.center.y = self.btnInvitado.frame.size.height / 2
        let tapBtnInvitado = UITapGestureRecognizer(target: self, action: #selector(self.confLoginInvitado))
        self.btnInvitado.isUserInteractionEnabled = true
        self.btnInvitado.addGestureRecognizer(tapBtnInvitado)
        
        self.isInterfaceBuild = true
    }
    
    func gotoRegistro(sender: UITapGestureRecognizer){
        let VCSignUp = SignUpVC()
        self.navigationController?.pushViewController(VCSignUp, animated: true)
    }
    
    func gotoLogin(sender: UITapGestureRecognizer){
        let VCloginEmail = LoginEmailVC()
        self.navigationController?.pushViewController(VCloginEmail, animated: true)
    }
    
    func gotoMain(sender: UITapGestureRecognizer){
        let xtabbarmain = TabBarMainControllerViewController()
        self.navigationController?.present(xtabbarmain, animated: true, completion: nil)
    }
    
    func randRange (lower: UInt32 , upper: UInt32) -> UInt32 {
        return lower + arc4random_uniform(upper - lower + 1)
    }
    
    func LoginFacebook(sender: UITapGestureRecognizer){
        
        //cerrar cualquier sesion abierta anteriormente
        Functions().borraTodoCierraSesion()
        
        let lmanager = LoginManager()
        lmanager.logOut()
        lmanager.logIn([.publicProfile, .email, .custom("user_likes") ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, _):
                print("Logged in!")
                if declinedPermissions.contains("email") {}
                if grantedPermissions.contains("email"){
                    self.setLoaderAnimation()
                   let request = GraphRequestConnection()
                    
                    request.add(MyProfileRequest()){ httpResponse, result in
                        switch result {
                        case .success(let response):
                            
                            if response.isValid {
                                //print("Graph Request Succeeded: \(response)")
                                var elSexo = Sexo.Mujer
                                if response.genero == "male" {
                                    elSexo = Sexo.Hombre
                                }
                                
                                //1 facebook
                                Functions().registerUser(rnombre: response.nombre!, rapellidos: response.apellidos!, rcorreo: response.email!, rpasswd: response.id!, rsexo: elSexo, rtipo: tipoRegistroLogin.facebook, urlImg: response.urlImg, flikes: response.likes){ exito, mensaje, error in
                                    
                                    if let xerror = error {
                                        print("Ocurrio un error \(xerror)")
                                        self.stopLoaderAnimation()
                                        self.mensajeSimple(mensaje: mensaje)
                                    } else {
                                        if exito {
                                            let elID = String(Functions().getIdUsr())
                                            Functions().itelligentUsuarioRegistrar(rid: elID){ exito, error in
                                                self.stopLoaderAnimation()
                                                
                                                self.gotoMain(sender: UITapGestureRecognizer())
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } else {
                                self.stopLoaderAnimation()
                                self.mensajeSimple(mensaje: "No ha otorgado los permisos a la aplicación", dismiss: false)
                            }
                            
                        case .failed(let error):
                            self.stopLoaderAnimation()
                            self.mensajeSimple(mensaje: "Error al intentar consultar los datos de Facebook")
                            print("Graph Request Failed: \(error)")
                        }
                    
                    }
                    request.start()
                } else {
                    self.stopLoaderAnimation()
                    self.mensajeSimple(mensaje: "No ha otorgado los permisos a la aplicación", dismiss: false)
                }
                
            }
        }
    }
    
    func confLoginInvitado(){
        
        let alert = UIAlertController(title: "TrendyAdvisor", message: "Para poder usar todas las funcionalidades de la app te sugerimos crear una cuenta o iniciar sesión", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK    ", style: UIAlertActionStyle.default, handler:{ action in
            Functions().loginInvitado(){ terminado in
                self.gotoMain(sender: UITapGestureRecognizer())
                
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
