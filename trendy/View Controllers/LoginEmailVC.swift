//
//  LoginEmailVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class LoginEmailVC: UIViewController, UITextFieldDelegate {
    
    var btnFacebook : UIView!
    var txtCorreo : UITextField!
    var txtContra : UITextField!
    var btnOlvidasteContra : UIView!
    var btnListo : UIView!
    
    var scrollView : UIScrollView!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        //cerrar cualquier sesion abierta anteriormente
        Functions().borraTodoCierraSesion()
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.isTranslucent = false
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.title = "Iniciar sesión"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
        
        self.stopLoaderAnimation()
    }
    
    func buildInterface(){
        
        self.view.backgroundColor = UIColor.white
        
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.7
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.15
        
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.9
        let altoCampo = Constantes.tamaños.anchoPantalla * 0.1
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //boton facebook
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnFacebook = UIView(frame: xframe)
        self.btnFacebook.backgroundColor = Constantes.paletaColores.azulBtnFacebook
        self.btnFacebook.layer.cornerRadius = 5
        self.btnFacebook.center.x = Constantes.tamaños.anchoPantalla / 2
        self.scrollView.addSubview(self.btnFacebook)
        //label
        let lblBtnFacebook = UILabel(frame: xframe)
        lblBtnFacebook.text = "Entrar con Facebook"
        lblBtnFacebook.sizeToFit()
        lblBtnFacebook.textColor = UIColor.white
        self.btnFacebook.addSubview(lblBtnFacebook)
        lblBtnFacebook.center.x = self.btnFacebook.frame.size.width / 2
        lblBtnFacebook.center.y = self.btnFacebook.frame.size.height / 2
        
        let tapBtnFacebook = UITapGestureRecognizer(target: self, action: #selector(self.LoginFacebook(sender:)))
        self.btnFacebook.isUserInteractionEnabled = true
        self.btnFacebook.addGestureRecognizer(tapBtnFacebook)
        
        //correo
        xframe.origin = CGPoint(x: 10, y: self.btnFacebook.frame.maxY + 20)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtCorreo = UITextField(frame: xframe)
        self.txtCorreo.placeholder = "Correo"
        self.scrollView.addSubview(self.txtCorreo)
        self.txtCorreo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtCorreo.delegate = self
        self.txtCorreo.keyboardType = .emailAddress
        self.txtCorreo.returnKeyType = .next
        self.txtCorreo.tag = 1
        self.txtCorreo.autocorrectionType = .no
        self.txtCorreo.autocapitalizationType = .none
        
        let borderCorreo = CALayer()
        borderCorreo.borderColor = UIColor.darkGray.cgColor
        borderCorreo.frame = CGRect(x: 0, y: self.txtCorreo.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtCorreo.frame.size.width, height: self.txtCorreo.frame.size.height)
        
        borderCorreo.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtCorreo.layer.addSublayer(borderCorreo)
        self.txtCorreo.layer.masksToBounds = true
        
        //contraseña
        xframe.origin = CGPoint(x: 10, y: self.txtCorreo.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder =  "Contraseña"
        self.scrollView.addSubview(self.txtContra)
        self.txtContra.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtContra.delegate = self
        self.txtContra.returnKeyType = .done
        self.txtContra.tag = 2
        self.txtContra.isSecureTextEntry = true
        self.txtContra.autocorrectionType = .no
        self.txtContra.autocapitalizationType = .none
        
        let borderContra = CALayer()
        borderContra.borderColor = UIColor.darkGray.cgColor
        borderContra.frame = CGRect(x: 0, y: self.txtContra.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtContra.frame.size.width, height: self.txtContra.frame.size.height)
        borderContra.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtContra.layer.addSublayer(borderContra)
        self.txtContra.layer.masksToBounds = true
        
        //olvidaste tu contraseña
        xframe.origin = CGPoint(x: 10, y: self.txtContra.frame.maxY + 20)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnOlvidasteContra = UIView(frame: xframe)
        self.btnOlvidasteContra.backgroundColor = UIColor.clear
        self.scrollView.addSubview(self.btnOlvidasteContra)
        self.btnOlvidasteContra.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let tapOlvideContra = UITapGestureRecognizer(target: self, action: #selector(self.gotoRecuperarContra(sender:)))
        self.btnOlvidasteContra.isUserInteractionEnabled = true
        self.btnOlvidasteContra.addGestureRecognizer(tapOlvideContra)
        
        //label
        let lblBtnOlvidaste = UILabel(frame: xframe)
        lblBtnOlvidaste.text = "¿Olvidaste tu Contraseña?"
        lblBtnOlvidaste.sizeToFit()
        self.btnOlvidasteContra.addSubview(lblBtnOlvidaste)
        lblBtnOlvidaste.center.x = self.btnOlvidasteContra.frame.size.width / 2
        lblBtnOlvidaste.center.y = self.btnOlvidasteContra.frame.size.height / 2
        
        //boton listo
        xframe.origin = CGPoint(x: 10, y: btnOlvidasteContra.frame.maxY + 20)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnListo = UIView(frame: xframe)
        self.btnListo.layer.cornerRadius = 5
        self.btnListo.layer.borderWidth = 0.5
        self.btnListo.layer.borderColor = UIColor.black.cgColor
        self.scrollView.addSubview(self.btnListo)
        self.btnListo.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //label btn
        let lblBtnSignUp = UILabel(frame: self.btnListo.frame)
        lblBtnSignUp.text = "LISTO"
        lblBtnSignUp.sizeToFit()
        self.scrollView.addSubview(lblBtnSignUp)
        lblBtnSignUp.center.x = self.btnListo.frame.minX + (self.btnListo.frame.size.width / 2)
        lblBtnSignUp.center.y = self.btnListo.frame.minY + (self.btnListo.frame.size.height / 2)
        
        //sombra
        
        self.btnListo.layer.shadowColor = UIColor.darkGray.cgColor
        self.btnListo.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.btnListo.layer.shadowOpacity = 1
        self.btnListo.layer.shadowRadius = 0.5
        let tapBtnListo = UITapGestureRecognizer(target: self, action: #selector(self.loginBtn(sender:)))
        self.btnListo.isUserInteractionEnabled = true
        self.btnListo.addGestureRecognizer(tapBtnListo)
        
        self.view.addSubview(self.scrollView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoRecuperarContra(sender: UITapGestureRecognizer){
        let VCforgot = ForgotPasswordVC()
        self.navigationController?.pushViewController(VCforgot, animated: true)
    }
    
    func LoginFacebook(sender: UITapGestureRecognizer){
        //cerrar cualquier sesion abierta anteriormente
        Functions().borraTodoCierraSesion()
        
        let lmanager = LoginManager()
        lmanager.logOut()
        lmanager.logIn([.publicProfile, .email, .custom("user_likes") ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, _):
                print("Logged in!")
                if declinedPermissions.contains("email") {}
                if grantedPermissions.contains("email"){
                    self.setLoaderAnimation()
                    let request = GraphRequestConnection()
                    
                    request.add(MyProfileRequest()){ httpResponse, result in
                        switch result {
                        case .success(let response):
                            
                            if response.isValid {
                                //print("Graph Request Succeeded: \(response)")
                                var elSexo = Sexo.Mujer
                                if response.genero == "male" {
                                    elSexo = Sexo.Hombre
                                }
                                
                                //1 facebook
                                Functions().registerUser(rnombre: response.nombre!, rapellidos: response.apellidos!, rcorreo: response.email!, rpasswd: response.id!, rsexo: elSexo, rtipo: tipoRegistroLogin.facebook, urlImg: response.urlImg, flikes: response.likes){ exito, mensaje, error in
                                    self.stopLoaderAnimation()
                                    if let xerror = error {
                                        print("Ocurrio un error \(xerror)")
                                        
                                        self.mensajeSimple(mensaje: mensaje)
                                    } else {
                                        if exito {
                                            let alert = UIAlertController(title: "TrendyAdvisor", message: "Usuario creado correctamente", preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                                                alert: UIAlertAction!) in
                                                
                                                let elID = String(Functions().getIdUsr())
                                                Functions().itelligentUsuarioRegistrar(rid: elID){ exito, error in
                                                    
                                                    let _ = self.navigationController?.popViewController(animated: true)
                                                }
                                                
                                                
                                            })
                                            )
                                            
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    }
                                    
                                }
                                
                            } else {
                                self.stopLoaderAnimation()
                                self.mensajeSimple(mensaje: "No ha otorgado los permisos a la aplicación", dismiss: false)
                            }
                            
                        case .failed(let error):
                            self.stopLoaderAnimation()
                            self.mensajeSimple(mensaje: "Error al intentar consultar los datos de Facebook")
                            print("Graph Request Failed: \(error)")
                        }
                        
                    }
                    request.start()
                } else {
                    self.stopLoaderAnimation()
                    self.mensajeSimple(mensaje: "No ha otorgado los permisos a la aplicación", dismiss: false)
                }
                
            }
        }
    }
    
    func loginBtn(sender: UITapGestureRecognizer){
        self.setLoaderAnimation()
        //antes de enviar nada, validar todos los datos
        if (!self.validaForm()){
            self.stopLoaderAnimation()
            return
            
        }
        
        let correo = self.txtCorreo.text?.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let passwd = self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Functions().loginUser(rcorreo: correo!, rpasswd: passwd!, tipo: tipoRegistroLogin.email){ exito, error in
            
            if exito {
                let elID = String(Functions().getIdUsr())
                Functions().itelligentUsuarioRegistrar(rid: elID){ exito, error in
                    self.stopLoaderAnimation()
                    let _ = self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.stopLoaderAnimation()
                if let xerror = error {
                    print("Ocurrio un error \(xerror)")
                }
                self.mensajeSimple(mensaje: "Ocurrio un error realizar el login de usuario")
            }
        }
    }
    
    func validaForm()->Bool{
        var mensaje = ""
        
        //correo
        if (mensaje == "" && (self.txtCorreo.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Correo\", es obligatorio"
            
        }
        //contra
        if (mensaje == "" && (self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Contraseña\", es obligatorio"
            
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        switch textField.tag {
        case 1: self.txtContra.becomeFirstResponder(); break;
        default: break
        }
        
        return true
    }
    
}
