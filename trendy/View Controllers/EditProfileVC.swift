//
//  EditProfileVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 02/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import DownPicker
import NVActivityIndicatorView

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    var scrollView : UIScrollView!
    var imgUsuario : UIImageView!
    var btnCambiarImagen : UIButton!
    var txtNombre : UITextField!
    var txtApellidos : UITextField!
    var txtSexo : UITextField!
    var pickerSexo : DownPicker!
    var txtCorreo : UITextField!
    var txtContra : UITextField!
    var txtConfcontra : UITextField!
    var btnListo : UIView!
    
    var activityI : NVActivityIndicatorView!
    
    var insetBottom : CGFloat = 0
    var altoNavBar : CGFloat = 0
    
    let imagePicker = UIImagePickerController()
    
    var editoImg = false
    
    var idUsrEdicion = 0
    
    var datosUsr = [String: Any]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)! + 10
        
        self.imagePicker.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func buildInterface(){
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        //imagen
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: self.altoNavBar + 10)
        let sizeImg = Constantes.tamaños.anchoPantalla * 0.3
        xframe.size = CGSize(width: sizeImg, height: sizeImg)
        self.imgUsuario = UIImageView(frame: xframe)
        self.imgUsuario.image = Constantes.imagenes.imgPlaceHolderProfile
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.layer.borderColor = UIColor.lightGray.cgColor
        self.imgUsuario.layer.borderWidth = 0.5
        self.imgUsuario.center.x = Constantes.tamaños.anchoPantalla / 2
        
        self.scrollView.addSubview(self.imgUsuario)
        
        //boton cambiar imagen
        xframe.origin = CGPoint(x: 0, y: self.imgUsuario.frame.maxY + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.anchoPantalla * 0.1)
        self.btnCambiarImagen = UIButton(frame: xframe)
        self.btnCambiarImagen.center.x = Constantes.tamaños.anchoPantalla / 2
        self.btnCambiarImagen.setTitle("Cambiar imagen", for: .normal)
        self.btnCambiarImagen.backgroundColor = UIColor.clear
        self.btnCambiarImagen.setTitleColor(UIColor.lightGray, for: .normal)
        self.scrollView.addSubview(self.btnCambiarImagen)
        self.btnCambiarImagen.addTarget(self, action: #selector(self.abreGaleria(sender:)), for: .touchUpInside)
        
        let anchoControl = Constantes.tamaños.anchoPantalla * 0.9
        let altoContol = Constantes.tamaños.anchoPantalla * 0.1
        
        //nombre
        xframe.origin = CGPoint(x: 0, y: self.btnCambiarImagen.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtNombre = UITextField(frame: xframe)
        self.txtNombre.placeholder = "Nombre"
        self.scrollView.addSubview(self.txtNombre)
        self.txtNombre.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderNombre = CALayer()
        
        borderNombre.borderColor = UIColor.darkGray.cgColor
        borderNombre.frame = CGRect(x: 0, y: self.txtNombre.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtNombre.frame.size.width, height: self.txtNombre.frame.size.height)
        
        borderNombre.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtNombre.layer.addSublayer(borderNombre)
        self.txtNombre.layer.masksToBounds = true
        self.txtNombre.delegate = self
        
        //apellidos
        xframe.origin = CGPoint(x: 0, y: self.txtNombre.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtApellidos = UITextField(frame: xframe)
        self.txtApellidos.placeholder = "Apellidos"
        self.scrollView.addSubview(self.txtApellidos)
        self.txtApellidos.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderAppelidos = CALayer()
        
        borderAppelidos.borderColor = UIColor.darkGray.cgColor
        borderAppelidos.frame = CGRect(x: 0, y: self.txtApellidos.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtApellidos.frame.size.width, height: self.txtApellidos.frame.size.height)
        
        borderAppelidos.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtApellidos.layer.addSublayer(borderAppelidos)
        self.txtApellidos.layer.masksToBounds = true
        self.txtApellidos.delegate = self
        
        //picker
        xframe.origin = CGPoint(x: 0, y: self.txtApellidos.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtSexo = UITextField(frame: xframe)
        
        self.scrollView.addSubview(self.txtSexo)
        self.txtSexo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.pickerSexo = DownPicker(textField: self.txtSexo, withData: Constantes.strings.arrayMH)
        
        self.pickerSexo.showArrowImage(true)
        
        //correo
        xframe.origin = CGPoint(x: 0, y: self.txtSexo.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtCorreo = UITextField(frame: xframe)
        self.txtCorreo.placeholder = "Email"
        self.scrollView.addSubview(self.txtCorreo)
        self.txtCorreo.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderCorreo = CALayer()
        
        borderCorreo.borderColor = UIColor.darkGray.cgColor
        borderCorreo.frame = CGRect(x: 0, y: self.txtCorreo.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtCorreo.frame.size.width, height: self.txtCorreo.frame.size.height)
        
        borderCorreo.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtCorreo.layer.addSublayer(borderCorreo)
        self.txtCorreo.layer.masksToBounds = true
        self.txtCorreo.isUserInteractionEnabled = false
        self.txtCorreo.delegate = self
        
        //contra
        xframe.origin = CGPoint(x: 0, y: self.txtCorreo.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder = "Contraseña"
        self.txtContra.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtContra)
        self.txtContra.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderContra = CALayer()
        
        borderContra.borderColor = UIColor.darkGray.cgColor
        borderContra.frame = CGRect(x: 0, y: self.txtContra.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtContra.frame.size.width, height: self.txtContra.frame.size.height)
        
        borderContra.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtContra.layer.addSublayer(borderContra)
        self.txtContra.layer.masksToBounds = true
        self.txtContra.delegate = self
        
        //conf contra
        xframe.origin = CGPoint(x: 0, y: self.txtContra.frame.maxY + 20)
        xframe.size = CGSize(width: anchoControl, height: altoContol)
        self.txtConfcontra = UITextField(frame: xframe)
        self.txtConfcontra.placeholder = "Confirmar constraseña"
        self.txtConfcontra.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtConfcontra)
        self.txtConfcontra.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderConfContra = CALayer()
        
        borderConfContra.borderColor = UIColor.darkGray.cgColor
        borderConfContra.frame = CGRect(x: 0, y: self.txtConfcontra.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtConfcontra.frame.size.width, height: self.txtConfcontra.frame.size.height)
        
        borderConfContra.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtConfcontra.layer.addSublayer(borderConfContra)
        self.txtConfcontra.layer.masksToBounds = true
        self.txtConfcontra.delegate = self
        
        //boton
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.7
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.15
        xframe.origin = CGPoint(x: 10, y: txtConfcontra.frame.maxY + 40)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnListo = UIView(frame: xframe)
        self.btnListo.layer.cornerRadius = 5
        self.btnListo.layer.borderWidth = 0.5
        self.btnListo.layer.borderColor = UIColor.black.cgColor
        self.scrollView.addSubview(self.btnListo)
        self.btnListo.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //label btn
        let lblBtnSignUp = UILabel(frame: self.btnListo.frame)
        lblBtnSignUp.text = "LISTO"
        lblBtnSignUp.sizeToFit()
        self.scrollView.addSubview(lblBtnSignUp)
        lblBtnSignUp.center.x = self.btnListo.frame.minX + (self.btnListo.frame.size.width / 2)
        lblBtnSignUp.center.y = self.btnListo.frame.minY + (self.btnListo.frame.size.height / 2)
        
        //sombra
        
        self.btnListo.layer.shadowColor = UIColor.darkGray.cgColor
        self.btnListo.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.btnListo.layer.shadowOpacity = 1
        self.btnListo.layer.shadowRadius = 0.5
        self.btnListo.isUserInteractionEnabled = true
        let tapbtnusr = UITapGestureRecognizer(target: self, action: #selector(self.enviaDatos))
        self.btnListo.addGestureRecognizer(tapbtnusr)
        
        
        self.view.addSubview(self.scrollView)
        self.scrollView.contentSize.height = self.btnListo.frame.maxY + (((self.tabBarController?.tabBar.frame.size.height)!)) + 20
        
        //setear datos
        self.datosUsr = Functions().getDataUsuario()
        self.idUsrEdicion = (self.datosUsr["idUsr"] as? Int)!
        
        self.txtNombre.text = (self.datosUsr["nombreUsr"] as? String)!
        self.txtApellidos.text = (self.datosUsr["apellidosUsr"] as? String)!
        let elSexo = (self.datosUsr["sexoUsr"] as? Sexo)!
        self.txtCorreo.text = (self.datosUsr["emailUsr"] as? String)!
        
        switch elSexo {
        case .Mujer: self.pickerSexo.setValueAt(0)
        case .Hombre: self.pickerSexo.setValueAt(1)
        case .Niño: break
        }
        
        if (self.datosUsr["urlImg"] as? String)! != "" {
            self.imgUsuario.kf.indicatorType = .activity
            let url = URL(string: (self.datosUsr["urlImg"] as? String)!)
            self.imgUsuario.kf.setImage(with: url)
        }
        
        if (self.datosUsr["tipoLogin"] as? tipoRegistroLogin)! == .facebook {
            self.txtContra.isUserInteractionEnabled = false
            self.txtConfcontra.isUserInteractionEnabled = false
            
        }
        
    }
    
    func enviaDatos(){
        let xframe = CGRect(x: 100, y: 100, width: Constantes.tamaños.anchoPantalla * 0.1, height: Constantes.tamaños.anchoPantalla * 0.1)
        self.activityI = NVActivityIndicatorView(frame: xframe, type: NVActivityIndicatorType.ballPulseSync, color: Constantes.paletaColores.rojoTrendy)
        self.view.addSubview(self.activityI)
        self.activityI.center.x = self.view.frame.size.width / 2
        self.activityI.center.y = (self.view.frame.size.height / 2) - 20
        
        self.activityI.startAnimating()
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Validando...")
        //antes de enviar nada, validar todos los datos
        if (!self.validaForma()){
            self.activityI.stopAnimating()
            return
            
        }
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Enviando...")
        
        let strnombre = self.txtNombre.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let strApellidos = self.txtApellidos.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let strContra = self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var laContra : String? = nil
        if strContra != "" {
            laContra = strContra
        }
        var laImagen : UIImage? = nil
        if self.editoImg {
            laImagen = self.imgUsuario.image
        }
        
        let elSexo = Functions().getCurrentSexType(strSexo: self.txtSexo.text!)
        let tipoReg = self.datosUsr["tipoLogin"] as! tipoRegistroLogin
        
        Functions().updateUser(rid: self.idUsrEdicion, rnombre: strnombre!, rapellidos: strApellidos!, rpasswd: laContra, rsexo: elSexo, rtipo: tipoReg, objImg: laImagen){ exito, error in
            self.activityI.stopAnimating()
            if !exito {
                if let xerror = error {
                    print("Ocurrio un error \(xerror)")
                }
                self.mensajeSimple(mensaje: "Ocurrio un error realizar la actualizacion de datos", dismiss: false)
            } else {
                if exito {
                    let alert = UIAlertController(title: "TrendyAdvisor", message: "Usuario actualizado correctamente", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                        alert: UIAlertAction!) in
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                    )
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
        /*
         
         id: ID del usuario (requerido)
         nombre:  (requerido)
         apellidos: (requerido)
         sexo: (requerido)
         os: IOS, ANDROID (requerido, dependiendo de la naturaleza de la app)
         ------
         avatar: imagen en base64 para Android, o archivo de imagen para iOS (opcional)
         password: (opcional)
         
         
         }
         
         */
    }
    
    func validaForma()->Bool{
        
        var mensaje = ""
        
        let strnombre = self.txtNombre.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let strApellidos = self.txtApellidos.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let strContra = self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let strConfContra = self.txtConfcontra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        //nombre
        if (strnombre == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            
        }
        //nombre
        if (strApellidos == ""){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            
        }
        
        if (mensaje == "" && strContra != "") {
            
            //conf contra
            if (mensaje == "" && (strContra?.characters.count)! < 5 ){
                mensaje = "El campo \"Contraseña\", requiere al menos 5 caracteres"
            }
            
            if (mensaje == "" && (strContra !=  strConfContra) ){
                mensaje = "Las contraseñas no coinciden"
            }

        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    //MARK: Para image picker
    func abreGaleria(sender: UITapGestureRecognizer){
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.modalPresentationStyle = .overCurrentContext
        
        present(self.imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let nuevaImg = Functions().scaleAndCropImage(image: chosenImage, toSize: CGSize(width: 300, height: 300))
            self.imgUsuario.image = nuevaImg
            self.imgUsuario.setNeedsDisplay()
            self.editoImg = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    //para image picker e imagen

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
