//
//  SignUpVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SignUpVC: UIViewController, UIPickerViewDelegate, UITextFieldDelegate {
    
    var txtNombre : UITextField!
    var txtApellidos : UITextField!
    var txtSexo : UITextField!
    var txtCorreo : UITextField!
    var txtContra : UITextField!
    var btnSignUp : UIView!
    var scrollView : UIScrollView!
    
    var pickerSexo = UIPickerView()
    
    var activityI : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //cerrar cualquier sesion abierta anteriormente
        Functions().borraTodoCierraSesion()

        self.buildInterface()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let xframe = CGRect(x: 100, y: 100, width: Constantes.tamaños.anchoPantalla * 0.1, height: Constantes.tamaños.anchoPantalla * 0.1)
        self.activityI = NVActivityIndicatorView(frame: xframe, type: NVActivityIndicatorType.ballPulseSync, color: Constantes.paletaColores.rojoTrendy)
        self.view.addSubview(self.activityI)
        self.activityI.center.x = self.view.frame.size.width / 2
        self.activityI.center.y = (self.view.frame.size.height / 2) - 20

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = "Registro"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.navigationItem.title = ""
    }
    
    func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func buildInterface(){
        
        self.view.backgroundColor = UIColor.white
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.9
        let altoCampo = Constantes.tamaños.anchoPantalla * 0.1
        
        //nombre
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtNombre = UITextField(frame: xframe)
        self.txtNombre.placeholder = "Nombre *"
        self.scrollView.addSubview(self.txtNombre)
        self.txtNombre.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtNombre.tag = 1
        self.txtNombre.returnKeyType = .next
        self.txtNombre.delegate = self
        self.txtNombre.keyboardType = .asciiCapable
        
         let borderNombre = CALayer()
        
         borderNombre.borderColor = UIColor.darkGray.cgColor
         borderNombre.frame = CGRect(x: 0, y: self.txtNombre.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtNombre.frame.size.width, height: self.txtNombre.frame.size.height)
         
         borderNombre.borderWidth = Constantes.tamaños.anchoTextFieldLine
         self.txtNombre.layer.addSublayer(borderNombre)
         self.txtNombre.layer.masksToBounds = true
        
        //apellidos
        xframe.origin = CGPoint(x: 0, y: self.txtNombre.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtApellidos = UITextField(frame: xframe)
        self.txtApellidos.placeholder = "Apellidos *"
        self.scrollView.addSubview(self.txtApellidos)
        self.txtApellidos.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let borderAppelidos = CALayer()
        
        borderAppelidos.borderColor = UIColor.darkGray.cgColor
        borderAppelidos.frame = CGRect(x: 0, y: self.txtApellidos.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtApellidos.frame.size.width, height: self.txtApellidos.frame.size.height)
        
        borderAppelidos.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtApellidos.layer.addSublayer(borderAppelidos)
        self.txtApellidos.layer.masksToBounds = true
        self.txtApellidos.tag = 2
        self.txtApellidos.returnKeyType = .next
        self.txtApellidos.delegate = self
        self.txtApellidos.keyboardType = .asciiCapable
        
        //sexo
        xframe.origin = CGPoint(x: 10, y: self.txtApellidos.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtSexo = UITextField(frame: xframe)
        self.txtSexo.placeholder = "Sexo (Opcional)"
        self.scrollView.addSubview(self.txtSexo)
        self.txtSexo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtSexo.tag = 3
        self.txtSexo.returnKeyType = .next
        self.txtSexo.delegate = self
        
        //picker
        self.pickerSexo.delegate = self
        let toolbarSexo = UIToolbar()
        toolbarSexo.barStyle = UIBarStyle.default
        toolbarSexo.isTranslucent = true
        toolbarSexo.sizeToFit()
        
        let okBtnSex = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerSexo(sender:)))
        let spaceBtnSex = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelBtnSex = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerSexo(sender:)))
        
        toolbarSexo.setItems([cancelBtnSex, spaceBtnSex, okBtnSex], animated: false)
        toolbarSexo.isUserInteractionEnabled = true
        
        self.txtSexo.inputView = self.pickerSexo
        self.txtSexo.inputAccessoryView = toolbarSexo
        
        let borderSexo = CALayer()
        
        borderSexo.borderColor = UIColor.darkGray.cgColor
        borderSexo.frame = CGRect(x: 0, y: self.txtSexo.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtSexo.frame.size.width, height: self.txtSexo.frame.size.height)
        
        borderSexo.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtSexo.layer.addSublayer(borderSexo)
        self.txtSexo.layer.masksToBounds = true
        
        //correo
        xframe.origin = CGPoint(x: 10, y: self.txtSexo.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtCorreo = UITextField(frame: xframe)
        self.txtCorreo.placeholder = "Correo *"
        self.scrollView.addSubview(self.txtCorreo)
        self.txtCorreo.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtCorreo.tag = 4
        self.txtCorreo.returnKeyType = .next
        self.txtCorreo.delegate = self
        self.txtCorreo.keyboardType = .emailAddress
        self.txtCorreo.autocapitalizationType = .none
        self.txtCorreo.autocorrectionType = .no
        
        let borderCorreo = CALayer()
        borderCorreo.borderColor = UIColor.darkGray.cgColor
        borderCorreo.frame = CGRect(x: 0, y: self.txtCorreo.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtCorreo.frame.size.width, height: self.txtCorreo.frame.size.height)
        
        borderCorreo.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtCorreo.layer.addSublayer(borderCorreo)
        self.txtCorreo.layer.masksToBounds = true
        
        //contraseña
        xframe.origin = CGPoint(x: 10, y: self.txtCorreo.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder = "Contraseña *"
        self.txtContra.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtContra)
        self.txtContra.center.x = Constantes.tamaños.anchoPantalla / 2
        self.txtContra.tag = 5
        self.txtContra.returnKeyType = .done
        self.txtContra.delegate = self
        self.txtContra.keyboardType = .asciiCapable
        
        let borderContra = CALayer()
        borderContra.borderColor = UIColor.darkGray.cgColor
        borderContra.frame = CGRect(x: 0, y: self.txtContra.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtContra.frame.size.width, height: self.txtContra.frame.size.height)
        
        borderContra.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtContra.layer.addSublayer(borderContra)
        self.txtContra.layer.masksToBounds = true
        
        //campos requeridos
        xframe.origin = CGPoint(x: self.txtContra.frame.minX, y: self.txtContra.frame.maxY + 20)
        xframe.size = CGSize(width: anchoCampo, height: 20)
        let lblCampoRequerido = UILabel(frame: xframe)
        lblCampoRequerido.text = "* Campo obligatorio"
        lblCampoRequerido.font = UIFont.boldSystemFont(ofSize: Constantes.tamaños.fuentes.minus.LabelFontSize_4)
        lblCampoRequerido.textColor = Constantes.paletaColores.rojoTrendy
        lblCampoRequerido.sizeToFit()
        self.scrollView.addSubview(lblCampoRequerido)
        
        //label de condiciones
        xframe.origin = CGPoint(x: lblCampoRequerido.frame.minX, y: lblCampoRequerido.frame.maxY + 30)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo * 3)
        let lblCondiciones = UILabel(frame: xframe)
        lblCondiciones.text = Constantes.strings.textLabelRegistro
        lblCondiciones.numberOfLines = 0
        lblCondiciones.sizeToFit()
        self.scrollView.addSubview(lblCondiciones)
        
        //var privacidad
        xframe.origin = CGPoint(x: lblCondiciones.frame.minX, y: lblCondiciones.frame.maxY)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.25, height: altoCampo)
        let btnVerMas = UIButton(frame: xframe)
        btnVerMas.setTitle("Ver mas...", for: .normal)
        btnVerMas.titleLabel?.font = UIFont.systemFont(ofSize: Constantes.tamaños.fuentes.minus.LabelFontSize_2)
        btnVerMas.setTitleColor(Constantes.paletaColores.rojoTrendy, for: .normal)
        self.scrollView.addSubview(btnVerMas)
        btnVerMas.addTarget(self, action: #selector(self.gotoPrivacidad), for: .touchUpInside)
        
        //boton listo
        xframe.origin = CGPoint(x: 10, y: btnVerMas.frame.maxY + 30)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.7, height: Constantes.tamaños.anchoPantalla * 0.14)
        self.btnSignUp = UIView(frame: xframe)
        self.btnSignUp.layer.cornerRadius = 5
        self.btnSignUp.layer.borderWidth = 0.5
        self.btnSignUp.layer.borderColor = UIColor.black.cgColor
        self.scrollView.addSubview(self.btnSignUp)
        self.btnSignUp.center.x = Constantes.tamaños.anchoPantalla / 2
        
        let tapSignUpBtn = UITapGestureRecognizer(target: self, action: #selector(self.enviarDatos(sender:)))
        self.btnSignUp.isUserInteractionEnabled = true
        self.btnSignUp.addGestureRecognizer(tapSignUpBtn)
        
        //label btn
        let lblBtnSignUp = UILabel(frame: self.btnSignUp.frame)
        lblBtnSignUp.text = "LISTO"
        lblBtnSignUp.sizeToFit()
        self.scrollView.addSubview(lblBtnSignUp)
        lblBtnSignUp.center.x = self.btnSignUp.frame.minX + (self.btnSignUp.frame.size.width / 2)
        lblBtnSignUp.center.y = self.btnSignUp.frame.minY + (self.btnSignUp.frame.size.height / 2)
        
        //sombra
        
        self.btnSignUp.layer.shadowColor = UIColor.darkGray.cgColor
        self.btnSignUp.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.btnSignUp.layer.shadowOpacity = 1
        self.btnSignUp.layer.shadowRadius = 0.5
        
        
        self.view.addSubview(self.scrollView)
        
        let altoNavBar = (((self.navigationController?.navigationBar.frame.size.height) != nil) ? (self.navigationController?.navigationBar.frame.size.height)! : CGFloat(44))
        
        self.scrollView.contentSize.height = self.btnSignUp.frame.maxY + Constantes.tamaños.altoStatusBar + altoNavBar + 15
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func donePickerSexo(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtSexo.text = Constantes.strings.arrayMHNA[self.pickerSexo.selectedRow(inComponent: 0)]
        }
        
        self.view.endEditing(true)
        
    }
    
    //picker view delegate
    func numberOfComponentsInPickerView(pickerView : UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return Constantes.strings.arrayMHNA.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Constantes.strings.arrayMHNA[row]
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        switch textField.tag {
        case 1: self.txtApellidos.becomeFirstResponder(); break;
        case 2: self.txtSexo.becomeFirstResponder(); break;
        case 3: self.txtCorreo.becomeFirstResponder(); break;
        case 4: self.txtContra.becomeFirstResponder(); break;
        default: break
        }
        
        return true
    }
    
    //picker view delegate - fin
    
    func validaForma()->Bool{
        
        var mensaje = ""
        
        //nombre
        if (self.txtNombre.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            
        }
        //nombre
        if (mensaje == "" && (self.txtApellidos.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            
        }
        //correo
        if (mensaje == "" && (self.txtCorreo.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Correo\", es obligatorio"
            
        }
        //contra
        if (mensaje == "" && (self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Contraseña\", es obligatorio"
            
        }
        
        //contra
        if (mensaje == "" && ((self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count)! < 5 )){
            mensaje = "El campo \"Contraseña\", requiere al menos 5 caracteres"
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    func enviarDatos(sender: UITapGestureRecognizer){
        self.activityI.startAnimating()
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Validando...")
        //antes de enviar nada, validar todos los datos
        if (!self.validaForma()){
            self.activityI.stopAnimating()
            return
            
        }
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Enviando...")
        
        let correo = self.txtCorreo.text?.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let passwd = self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let nombre = self.txtNombre.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let apellidos = self.txtApellidos.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let elSexo = Functions().getCurrentSexType(strSexo: self.txtSexo.text!)
        
        Functions().registerUser(rnombre: nombre!, rapellidos: apellidos!, rcorreo: correo!, rpasswd: passwd!, rsexo: elSexo, rtipo: tipoRegistroLogin.email, urlImg: "", flikes: nil){ exito, mensaje, error in
            self.activityI.stopAnimating()
            if let xerror = error {
                print("Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: mensaje)
            } else {
                if exito {
                    let alert = UIAlertController(title: "TrendyAdvisor", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                        alert: UIAlertAction!) in
                        
                        let elID = String(Functions().getIdUsr())
                        Functions().itelligentUsuarioRegistrar(rid: elID){ exito, error in
                            
                            let _ = self.navigationController?.popViewController(animated: true)
                        }
                    })
                    )
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
        }
    }
    
    func gotoPrivacidad(){
        
        let elVC = GeneralWebViewVC()
        elVC.altoTabBar = 0
        elVC.altoNavBar = 0
        elVC.URLParam = Constantes.URLS.URL_privacidad
        elVC.titulo = "Privacidad"
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.pushViewController(elVC, animated: true)
    }

}
