//
//  SearchVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 29/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import DownPicker
import NVActivityIndicatorView
import Kingfisher

class SearchVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITextFieldDelegate, NVActivityIndicatorViewable {
    
    var containerHeader : UIView!
    var headerBar : UIView!
    var imageSearch : UIImageView!
    var txtSearch : UITextField!
    var txtPicker : UITextField!
    var pickerPersona : DownPicker!
    var collectionBusqueda : UICollectionView!
    
    var datosCollection = [Categoria]()
    
    let cellIdentifier = "celdaMain"
    
    var insetBottom : CGFloat = 0
    var altoNavBar : CGFloat = 0
    
    let notifAplicaFiltro = Notification.Name("aplicaFiltro")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoAdvancedFilterResult(notification:)), name: self.notifAplicaFiltro, object: nil)

        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        self.insetBottom = (self.tabBarController?.tabBar.frame.size.height)!
        
        self.loadData(sSexo: Functions().getSexUser())
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = "Búsqueda"
        
        let rightbbitem = UIBarButtonItem(image: Asset.icFilterWhite.image, style: .plain, target: self, action: #selector(self.gotoAdvancedFilter))
        rightbbitem.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightbbitem
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
        
    }
    
    func loadData(sSexo : Sexo){
        self.setLoaderAnimation()
        
        Functions().getCategoriasSubcategoriasServicio(elSexo: sSexo){ categorias, error  in
            self.stopLoaderAnimation()
            if let xerror = error {
                self.mensajeSimple(mensaje: "Ocurrio un error al cargar los datos, por favor revise su conexion")
                print("Ocurrio un error al cargar los datos \(xerror)")
            } else if let _ = categorias {
                self.datosCollection = categorias!
                self.collectionBusqueda.reloadData()
            } else {
                self.mensajeSimple(mensaje: "Ocurrio un error al cargar los datos, por favor revise su conexion")
            }
            
        }
    }
    
    func gotoAdvancedFilter(){
        let elVC = AdvancedFilterVC()
        elVC.searchSex = Functions().getCurrentSexType(strSexo: self.pickerPersona.text)
        elVC.senderVC = senderAdvancedFilter.search
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func gotoAdvancedFilterResult(notification: NSNotification){
        let _ = self.navigationController?.popViewController(animated: true)
        let paramFiltro = notification.object as! NSDictionary
        
        let strSexo = paramFiltro["sexo"] as! String
        let strTienda = paramFiltro["tiendas"] as! String
        let strMarca = paramFiltro["marca"] as! String
        let strCategoria = paramFiltro["categoria"] as! String
        let strColor = paramFiltro["color"] as! String
        let strPrecios = paramFiltro["precios"] as! String
        let items = paramFiltro["items"] as! [itemPrenda]
        
        //ahora va a collection category
        let elVC = CollectionCategoryVC()
        elVC.datosCollectionx = items
        elVC.strSexo = strSexo
        elVC.strTienda = (strTienda == "" ? nil : strTienda)
        elVC.strMarca = (strMarca == "" ? nil : strMarca)
        elVC.strCategoria = (strCategoria == "" ? nil : strCategoria)
        elVC.strColor = (strColor == "" ? nil : strColor)
        elVC.strPrecios = strPrecios
        elVC.fromAdvancedFilter = true
        elVC.itemsCounter = 20
        elVC.tipoSexo = Sexo(rawValue: Int(strSexo)!)!
        elVC.senderVC = .advancedfilter
        
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func buildInterface(){
        
        self.view.backgroundColor = UIColor.black
        
        let altoContainerPrincipal = Constantes.tamaños.anchoPantalla * 0.22
        
        var xframe = CGRect.zero
        
        //container
        xframe.origin = CGPoint(x: 0, y: self.altoNavBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoContainerPrincipal)
        self.containerHeader = UIView(frame: xframe)
        self.containerHeader.backgroundColor = UIColor.white
        
        //container2
        xframe.origin = CGPoint(x: 10, y: 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: altoContainerPrincipal * 0.7)
        let containerST = UIView(frame: xframe)
        self.containerHeader.addSubview(containerST)
        containerST.center.y = self.containerHeader.frame.height / 2
        containerST.layer.borderColor = UIColor.gray.cgColor
        containerST.layer.borderWidth = 0.5
        containerST.layer.cornerRadius = 5
        
        
        //imagen
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size  = CGSize(width: containerST.frame.size.height * 0.5, height: containerST.frame.size.height * 0.5)
        self.imageSearch = UIImageView(frame: xframe)
        self.imageSearch.contentMode = .scaleAspectFit
        self.imageSearch.image = Constantes.imagenes.imgFooterSearch
        containerST.addSubview(self.imageSearch)
        self.imageSearch.center.y = containerST.frame.height / 2
        
        //textfield busqueda
        let anchoTextField = containerST.frame.size.width - (self.imageSearch.frame.size.width + 15)
        xframe.origin = CGPoint(x: imageSearch.frame.maxX + 5, y: 5)
        xframe.size = CGSize(width: anchoTextField, height: containerST.frame.size.height * 0.7)
        self.txtSearch = UITextField(frame: xframe)
        self.txtSearch.placeholder = "Buscar"
        containerST.addSubview(self.txtSearch)
        self.txtSearch.center.y = containerST.frame.size.height / 2
        self.txtSearch.returnKeyType = .done
        self.txtSearch.delegate = self
        self.txtSearch.clearButtonMode = .whileEditing
        
        let borderBuscar = CALayer()
        borderBuscar.borderColor = UIColor.lightGray.cgColor
        borderBuscar.frame = CGRect(x: 0, y: self.txtSearch.frame.size.height - (Constantes.tamaños.anchoTextFieldLine), width:  self.txtSearch.frame.size.width, height: self.txtSearch.frame.size.height)
        borderBuscar.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtSearch.layer.addSublayer(borderBuscar)
        self.txtSearch.layer.masksToBounds = true
        
        //textfield picker
        let anchopicker = Constantes.tamaños.anchoPantalla - (containerST.frame.size.width + 20)
        
        xframe.origin = CGPoint(x: containerST.frame.maxX + 5, y: 5)
        xframe.size = CGSize(width: anchopicker, height: containerST.frame.size.height)
        self.txtPicker = UITextField(frame: xframe)
        self.txtPicker.layer.cornerRadius = 10
        self.containerHeader.addSubview(self.txtPicker)
        self.txtPicker.center.y = self.containerHeader.frame.height / 2
        self.txtPicker.backgroundColor = Constantes.paletaColores.rojoTrendy
        self.txtPicker.setLeftPaddingPoints(10)
        self.txtPicker.textColor = UIColor.white
        self.pickerPersona = DownPicker(textField: self.txtPicker, withData: Constantes.strings.arrayMHN)//0M1H
        
        let elSexo = Functions().getSexUser()
        switch elSexo {
        case .Hombre:
            self.pickerPersona.setValueAt(1)
        default:
            self.pickerPersona.setValueAt(0)
        }
        self.pickerPersona.addTarget(self, action: #selector(self.reloadCollectionData), for: UIControlEvents.valueChanged)
        self.view.addSubview(self.containerHeader)
        
        
        
        //flecha del picker
        
        let button = UIButton(type: .custom)
        button.setImage(Asset.icArrowDownW.image, for: .normal)
        
        let anchoImg = self.txtPicker.frame.size.height * 0.15
        let altoImg = self.txtPicker.frame.size.height * 0.3
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 5)
        button.frame = CGRect(x: CGFloat(self.txtPicker.frame.size.width - anchoImg), y: CGFloat(5), width: anchoImg, height: altoImg)
        self.txtPicker.rightView = button
        self.txtPicker.rightViewMode = .always
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        xframe.origin = CGPoint(x: 0, y: self.containerHeader.frame.maxY)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: self.view.frame.height - self.containerHeader.frame.maxY)
        self.collectionBusqueda = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionBusqueda.dataSource = self
        self.collectionBusqueda.delegate = self
        self.collectionBusqueda.backgroundColor = UIColor.white
        
        self.collectionBusqueda.register(CeldaMainCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.view.addSubview(self.collectionBusqueda)
        self.view.sendSubview(toBack: self.collectionBusqueda)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datosCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaMainCell
        
        let leCat = self.datosCollection[indexPath.item]
        let urlImg = URL(string: leCat.imagen)
        cell.imagenFondo.kf.indicatorType = .activity
        cell.imagenFondo.kf.setImage(with: urlImg)
        cell.imagenFondo.contentMode = .scaleAspectFit
        cell.imagenFondo.center.x = cell.frame.size.width / 2
        cell.imagenFondo.center.y = cell.frame.size.height / 2
        cell.imagenFondo.layer.cornerRadius = 10
        cell.imagenFondo.clipsToBounds = true
        cell.imagenFondo.setNeedsDisplay()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        
        let anchoCollView = collectionView.frame.size.width - 20
        
        elTamaño = CGSize(width: anchoCollView / 2.07, height: anchoCollView / 2.07)
        
        return elTamaño
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        return UIEdgeInsets(top: 5, left: 10, bottom: self.insetBottom + 5, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let leCat = self.datosCollection[indexPath.item]
        
        let elSexo = Functions().getCurrentSexType(strSexo: self.pickerPersona.text)
        
        if leCat.subcategorias.count > 0 {
            let subcatVC = SearchSubCategoryVC()
            subcatVC.titleHeader = leCat.nombre
            subcatVC.laCategoria = leCat
            subcatVC.datosCollection = leCat.subcategorias
            subcatVC.tipoSexo = elSexo
            self.navigationController?.pushViewController(subcatVC, animated: true)
            subcatVC.buildInterface()
        } else {
            let collectionCat = CollectionCategoryVC()
            collectionCat.senderVC = senderAdvancedFilter.categoria
            collectionCat.itemCategoria = leCat
            collectionCat.tipoSexo = elSexo
            self.navigationController?.pushViewController(collectionCat, animated: true)
        }
    }
    
    func reloadCollectionData(){
        
        let catText = Functions().getCurrentSexType(strSexo: self.pickerPersona.text)
        self.datosCollection.removeAll()
        
        self.loadData(sSexo: catText)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if !(textField.text?.isEmpty)! {
            self.getItemsBusqueda()
        }
        return true
    }
    
    func getItemsBusqueda(){
        
        let elSexo = Functions().getCurrentSexType(strSexo: self.pickerPersona.text)
        let elString = self.txtSearch.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        self.setLoaderAnimation()
        
        Functions().getResultBusqueda(tipoSexo: elSexo, strBusqueda: elString!){arrayItems, xerror in
            if let xxerror = xerror {
                self.stopLoaderAnimation()
                print("Ocurrio un error al cargar los datos \(xxerror)")
            } else {
                if arrayItems.count > 0 {
                    let vcSearchResult = SearchResultVC()
                    vcSearchResult.datosCollectionx = arrayItems
                    vcSearchResult.pickerIndex = self.pickerPersona.selectedIndex
                    vcSearchResult.stringSearch = self.txtSearch.text!
                    self.stopLoaderAnimation()
                    self.navigationController?.pushViewController(vcSearchResult, animated: true)
                    vcSearchResult.buildInterface()
                    
                } else {
                    self.stopLoaderAnimation()
                    let alert = UIAlertController(title: "TrendyAdvisor", message: Constantes.strings.mensajeSinresultados, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
