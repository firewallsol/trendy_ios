//
//  FullScreenGalleryVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 08/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import ImageSlideshow

class FullScreenGalleryVC: UIViewController {
    
    var FullimageSlider : ImageSlideshow!
    var itemActual : itemPrenda!
    var fotos = [KingfisherSource]()
    var titulo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        var xframe = CGRect.zero
        let posy = Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)!
        xframe.origin = CGPoint(x: 0, y: posy)
        let alto = self.view.frame.size.height - posy - 50
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: alto)
        self.FullimageSlider = ImageSlideshow(frame: xframe)
        self.FullimageSlider.contentScaleMode = .scaleAspectFill
        self.view.addSubview(self.FullimageSlider)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.FullimageSlider.setImageInputs(self.fotos)
        self.navigationItem.title = self.titulo
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
