//
//  AdvancedFilterVC.swift
//  trendy
//
//  Created by Leonel Sanchez on 06/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import TTRangeSlider
import Kingfisher

class AdvancedFilterVC: UIViewController, UIPickerViewDelegate, UIGestureRecognizerDelegate,  UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var btnCatNiño : UILabel!
    var btnCatHombre : UILabel!
    var btnCatMujer : UILabel!
    
    var txtCategoria : UITextField!
    var pickCategoria = UIPickerView()
    
    var txtTienda : UITextField! //cambio por marcas
    var pickTienda = UIPickerView() //cambio por marcas
    var arrayTiendasS = [Tienda_support]() //cambio por marcas
    var selectedIndexTiendas = [Int]() //cambio por marcas
    
    
    //var txtMarca : UITextField!
    //var pickMarca = UIPickerView()
    
    var sliderPrecio : TTRangeSlider!
    var lblPrecio : UILabel!
    
    var arraySubCategoriasS = [Subcategoria_support]()
    var arraySubcategoriaTodas = [Subcategoria_support]()
    
    
    var arrayMarcas = [Marca_support]()
    var selectedIndexMarcas = [Int]()
    var arrayMarcasTodas = [Marca_support]()
    
    var selectedIndexCategorias = [Int]()
    
    
    var currentColorIndex = -1
    
    var arrayViewColores = [UIView]()
    
    var checkColor = UIImageView()
    
    var scrollView : UIScrollView!
    
    var indexSexCat = 0
    
    let notifAplicaFiltro = Notification.Name("aplicaFiltro")
    let notifFiltroCollection = Notification.Name("FiltroCollection")
    let notifBackAllBrands = Notification.Name("BackAllBrands")
    
    var searchSex = Sexo.Hombre
    var sexFilterIndex = 0
    
    var senderVC = senderAdvancedFilter.search
    var idCatDefault = 0
    
    //
    var strMarca : String? = nil
    var strCategoria : String? = nil
    var strColor : String? = nil
    var strPrecios : String!
    var strTienda : String? = nil
    
    //collection marcas
    var collectionMarcas : UICollectionView!
    let cellIdentifier = "celdaMain"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.backFromAllBrands(notification:)), name: self.notifBackAllBrands, object: nil)
        
        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
        
        Functions().getCategoriaMarcaTienda(){ dicMarcas, dicCategoria, dicTienda, error in
            if let xerror = error {
                print("Ocurrio un error al cargar los datos \(xerror)")
            } else {
                self.arraySubCategoriasS.removeAll()
                self.arrayMarcas.removeAll()
                self.arraySubCategoriasS = dicCategoria
                self.arraySubcategoriaTodas = dicCategoria
                self.arrayMarcasTodas = dicMarcas
                self.arrayMarcas = dicMarcas
                self.arrayTiendasS = dicTienda
                self.stopLoaderAnimation()
                self.pickCategoria.reloadComponent(0)
                self.filterCategoriasMarcas(initialLoad: true)
                if (self.senderVC == .advancedfilter){
                    self.filterCategoriasAFilter()
                }
                
            }
            
        }
        
        self.selectedIndexCategorias.append(0)
        
        self.indexSexCat = self.searchSex.rawValue
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = "Filtro"
        
        let rightbbitem = UIBarButtonItem(title: "Aplicar", style: .plain, target: self, action: #selector(self.aplicarFiltro(sender:)))
        
        self.navigationItem.rightBarButtonItem = rightbbitem
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = ""
        
        self.stopLoaderAnimation()
        
    }
    
    func aplicarFiltro(sender: UIBarButtonItem){
        
        self.setLoaderAnimation()
        
        let strSexo = "\(self.indexSexCat)"
        
        var strMarca : String? = nil
        if self.selectedIndexMarcas.count > 0 && self.selectedIndexMarcas[0] != 0 {
            strMarca = self.getStringSeparado(sender: "marca", tipo: "id")
        }
        
        var strCatego : String? = nil
        if self.selectedIndexCategorias.count > 0 && self.selectedIndexCategorias[0] != 0 {
            strCatego = self.getStringSeparado(sender: "subcat", tipo: "id")
        }
        var strColor : String? = nil
        if self.currentColorIndex != -1 {
            strColor = Constantes.strings.arrayColores[self.currentColorIndex].nombre
        }
        let minVal = Int(round(self.sliderPrecio.selectedMinimum))
        let maxVal = Int(round(self.sliderPrecio.selectedMaximum))
        var strPrecios = ""
        strPrecios = "\(minVal),\(maxVal)"
        
        //tiendas
        var strTiendas : String? = nil
        if self.selectedIndexTiendas.count > 0 && self.selectedIndexTiendas[0] != 0 {
            strTiendas = self.getStringSeparado(sender: "tienda", tipo: "id")
        }
        
        print("llegue aqui mandare estos parametros: ")
        print("Sexo->\(strSexo)")
        print("marcas->\(strMarca)")
        print("Categoria->\(strCatego)")
        print("color->\(strColor)")
        print("precios->\(strPrecios)")
        print("tienda->\(strTiendas)")
        
        Functions().getItemsFiltrovanzado(sexo: strSexo, marcas: strMarca, categorias: strCatego, color: strColor, precios: strPrecios, tiendas: strTiendas, index: 0){ items, error in
            if let xerror = error {
                self.stopLoaderAnimation()
                print("Ha ocurrido un problema: \(xerror)")
            } else {
                self.stopLoaderAnimation()
                if !items.isEmpty {
                    let paramsFiltro = [
                        "sexo":strSexo,
                        "marca":strMarca ?? "",
                        "categoria": strCatego ?? "",
                        "color": strColor ?? "",
                        "precios": strPrecios,
                        "items":items,
                        "tiendas" : strTiendas ?? ""
                        ] as NSDictionary
                    print("este es el sender ---> \(self.senderVC)")
                    if (self.senderVC == .search) {
                        NotificationCenter.default.post(name: self.notifAplicaFiltro, object: paramsFiltro)
                    } else {
                        NotificationCenter.default.post(name: self.notifFiltroCollection, object: paramsFiltro)
                    }
                } else {
                    self.mensajeSimple(mensaje: "No se encontraron productos")
                }
            }
            
        }
        
    }
    
    func buildInterface(){
        
        var xframe = CGRect.zero
        let anchoLabelCat = Constantes.tamaños.anchoPantalla * 0.3
        let centroHorizontal = Constantes.tamaños.anchoPantalla / 2
        let yposinicial = Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)! + 15
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.9
        let altoCampo = Constantes.tamaños.anchoPantalla * 0.1
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //categorias labels
        //mujer
        xframe.origin = CGPoint(x: 0, y: yposinicial)
        xframe.size = CGSize(width: anchoLabelCat, height: 30)
        self.btnCatMujer = UILabel(frame: xframe)
        self.btnCatMujer.text = "Mujer"
        self.btnCatMujer.textAlignment = .center
        self.scrollView.addSubview(self.btnCatMujer)
        self.btnCatMujer.center.x = centroHorizontal
        self.btnCatMujer.tag = Sexo.Mujer.rawValue
        let tapBtnCatMujer = UITapGestureRecognizer(target: self, action: #selector(self.changeSexCat(recognizer:)))
        self.btnCatMujer.isUserInteractionEnabled = true
        self.btnCatMujer.addGestureRecognizer(tapBtnCatMujer)
        self.btnCatMujer.textColor = UIColor.gray
        
        //hombre
        xframe.origin = CGPoint(x: 0, y: yposinicial)
        xframe.size = CGSize(width: anchoLabelCat, height: 30)
        self.btnCatHombre = UILabel(frame: xframe)
        self.btnCatHombre.text = "Hombre"
        self.btnCatHombre.textAlignment = .center
        self.scrollView.addSubview(self.btnCatHombre)
        self.btnCatHombre.frame.origin.x = self.btnCatMujer.frame.minX - (5 + anchoLabelCat)
        self.btnCatHombre.tag = Sexo.Hombre.rawValue
        let tapBtnCatHombre = UITapGestureRecognizer(target: self, action: #selector(self.changeSexCat(recognizer:)))
        self.btnCatHombre.isUserInteractionEnabled = true
        self.btnCatHombre.addGestureRecognizer(tapBtnCatHombre)
        self.btnCatHombre.textColor = UIColor.gray
        
        //niños
        xframe.origin = CGPoint(x: 0, y: yposinicial)
        xframe.size = CGSize(width: anchoLabelCat, height: 30)
        self.btnCatNiño = UILabel(frame: xframe)
        self.btnCatNiño.text = "Niños"
        self.btnCatNiño.textAlignment = .center
        self.scrollView.addSubview(self.btnCatNiño)
        self.btnCatNiño.frame.origin.x = self.btnCatMujer.frame.maxX + 5
        self.btnCatNiño.tag = Sexo.Niño.rawValue
        let tapBtnCatNiño = UITapGestureRecognizer(target: self, action: #selector(self.changeSexCat(recognizer:)))
        self.btnCatNiño.isUserInteractionEnabled = true
        self.btnCatNiño.addGestureRecognizer(tapBtnCatNiño)
        self.btnCatNiño.textColor = UIColor.gray
        
        
        //categorias
        //label 
        xframe.origin = CGPoint(x: 10, y: self.btnCatHombre.frame.maxY + 15)
        let lblCategoria = UILabel(frame: xframe)
        lblCategoria.text = "Categoría"
        lblCategoria.font = UIFont.boldSystemFont(ofSize: lblCategoria.font.pointSize)
        lblCategoria.sizeToFit()
        self.scrollView.addSubview(lblCategoria)
        
        //textfield
        xframe.origin = CGPoint(x: 10, y: lblCategoria.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtCategoria = UITextField(frame: xframe)
        self.txtCategoria.text = "Todas"
        self.txtCategoria.textAlignment = .center
        self.txtCategoria.tintColor = UIColor.clear
        self.scrollView.addSubview(self.txtCategoria)
        self.txtCategoria.center.x = centroHorizontal
        
        let borderCat = CALayer()
        
        borderCat.borderColor = UIColor.darkGray.cgColor
        borderCat.frame = CGRect(x: 0, y: self.txtCategoria.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtCategoria.frame.size.width, height: self.txtCategoria.frame.size.height)
        
        borderCat.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtCategoria.layer.addSublayer(borderCat)
        self.txtCategoria.layer.masksToBounds = true
        
        //picker
        self.pickCategoria.delegate = self
        let toolbarCat = UIToolbar()
        toolbarCat.barStyle = UIBarStyle.default
        toolbarCat.isTranslucent = true
        toolbarCat.sizeToFit()
        
        let okBtnCat = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerCategoria(sender:)))
        let spaceBtnCat = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelBtnCat = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerCategoria(sender:)))
        
        toolbarCat.setItems([spaceBtnCat, okBtnCat], animated: false)
        toolbarCat.isUserInteractionEnabled = true
        
        self.txtCategoria.inputView = self.pickCategoria
        self.txtCategoria.inputAccessoryView = toolbarCat
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.rowTappedSubCategoria(sender:)))
        tapgesture.numberOfTapsRequired = 1
        tapgesture.delegate = self
        tapgesture.cancelsTouchesInView = false
        self.pickCategoria.addGestureRecognizer(tapgesture)
        
        //marcas
        //label
        xframe.origin = CGPoint(x: 10, y: self.txtCategoria.frame.maxY + 20)
        let lblMarca = UILabel(frame: xframe)
        lblMarca.text = "Marca"
        lblMarca.font = UIFont.boldSystemFont(ofSize: lblCategoria.font.pointSize)
        lblMarca.sizeToFit()
        self.scrollView.addSubview(lblMarca)
        
        //ver todas
        xframe.origin = CGPoint(x: 100, y: self.txtCategoria.frame.maxY + 20)
        let lblVerTodas = UILabel(frame: xframe)
        lblVerTodas.text = "Ver todas"
        lblVerTodas.font = UIFont.boldSystemFont(ofSize: lblCategoria.font.pointSize)
        lblVerTodas.textColor = UIColor.gray
        lblVerTodas.sizeToFit()
        self.scrollView.addSubview(lblVerTodas)
        let tapVertodas = UITapGestureRecognizer(target: self, action: #selector(self.verTodasTapped(sender:)))
        lblVerTodas.isUserInteractionEnabled = true
        lblVerTodas.addGestureRecognizer(tapVertodas)
        lblVerTodas.frame.origin.x = Constantes.tamaños.anchoPantalla - (lblVerTodas.frame.size.width + 10)
        
        xframe.origin = CGPoint(x: 10, y: lblMarca.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.95, height: altoCampo * 2)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        self.collectionMarcas = UICollectionView(frame: xframe, collectionViewLayout: flowLayout)
        self.collectionMarcas.register(CeldaMainCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.collectionMarcas.delegate = self
        self.collectionMarcas.dataSource = self
        self.scrollView.addSubview(self.collectionMarcas)
        self.collectionMarcas.backgroundColor = UIColor.white
        self.collectionMarcas.center.x = Constantes.tamaños.anchoPantalla / 2
        
        //---- hasta aqui marcas
        
        //tiendas
        //label
        xframe.origin = CGPoint(x: 10, y: self.collectionMarcas.frame.maxY + 20)
        let lblTienda = UILabel(frame: xframe)
        lblTienda.text = "Tienda"
        lblTienda.font = UIFont.boldSystemFont(ofSize: lblCategoria.font.pointSize)
        lblTienda.sizeToFit()
        self.scrollView.addSubview(lblTienda)
        
        xframe.origin = CGPoint(x: 10, y: lblTienda.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtTienda = UITextField(frame: xframe)
        self.txtTienda.text = "Todas"
        self.txtTienda.textAlignment = .center
        self.txtTienda.tintColor = UIColor.clear
        self.scrollView.addSubview(self.txtTienda)
        self.txtTienda.center.x = centroHorizontal
        
        let borderTienda = CALayer()
        
        borderTienda.borderColor = UIColor.darkGray.cgColor
        borderTienda.frame = CGRect(x: 0, y: self.txtTienda.frame.size.height - Constantes.tamaños.anchoTextFieldLine, width:  self.txtTienda.frame.size.width, height: self.txtTienda.frame.size.height)
        
        borderTienda.borderWidth = Constantes.tamaños.anchoTextFieldLine
        self.txtTienda.layer.addSublayer(borderTienda)
        self.txtTienda.layer.masksToBounds = true
        
        //picker
        self.pickTienda.delegate = self
        let toolbarTienda = UIToolbar()
        toolbarTienda.barStyle = UIBarStyle.default
        toolbarTienda.isTranslucent = true
        toolbarTienda.sizeToFit()
        
        let okBtnTienda = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerTienda(sender:)))
        let spaceBtnTienda = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelBtnTienda = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerTienda(sender:)))
        
        toolbarTienda.setItems([spaceBtnTienda, okBtnTienda], animated: false)
        toolbarTienda.isUserInteractionEnabled = true
        
        self.txtTienda.inputView = self.pickTienda
        self.txtTienda.inputAccessoryView = toolbarTienda
        
        let tapgestureTienda = UITapGestureRecognizer(target: self, action: #selector(self.rowTappedTienda(sender:)))
        tapgestureTienda.numberOfTapsRequired = 1
        tapgestureTienda.delegate = self
        tapgestureTienda.cancelsTouchesInView = false
        self.pickTienda.addGestureRecognizer(tapgestureTienda)
        //tiendas fin
        
        //color
        
        //label
        xframe.origin = CGPoint(x: 10, y: self.txtTienda.frame.maxY + 20)
        let lblColor = UILabel(frame: xframe)
        lblColor.text = "Color"
        lblColor.font = UIFont.boldSystemFont(ofSize: lblColor.font.pointSize)
        lblColor.sizeToFit()
        self.scrollView.addSubview(lblColor)
        
        //colores
        let itemsXLinea = 8
        let anchoAltoColor = Constantes.tamaños.anchoPantalla * 0.1
        var ypos = lblColor.frame.maxY + 15
        var xpos : CGFloat = 10
        var cont = 1
        var index = 0
        self.arrayViewColores.removeAll()
        
        for item in Constantes.strings.arrayColores {
            if cont == itemsXLinea {
                cont = 1
                ypos += (anchoAltoColor + 10)
                xpos = 10
            }
            xframe.origin = CGPoint(x: xpos, y: ypos)
            xframe.size = CGSize(width: anchoAltoColor, height: anchoAltoColor)
            let viewColor = UIView(frame: xframe)
            viewColor.backgroundColor = item.color
            viewColor.layer.cornerRadius = viewColor.frame.size.width / 2
            viewColor.clipsToBounds = true
            if item.color == UIColor.white {
                viewColor.layer.borderWidth = 0.5
                viewColor.layer.borderColor = UIColor.black.cgColor
            }
            viewColor.tag = index
            self.scrollView.addSubview(viewColor)
            self.scrollView.sendSubview(toBack: viewColor)
            viewColor.isUserInteractionEnabled = true
            let tapColor = UITapGestureRecognizer(target: self, action: #selector(self.tapOnColor(gesture:)))
            viewColor.addGestureRecognizer(tapColor)
            self.arrayViewColores.append(viewColor)
            
            xpos += (anchoAltoColor + 10)
            cont += 1
            index += 1
        }
        
        xframe.size = CGSize(width: anchoAltoColor * 0.7, height: anchoAltoColor * 0.7)
        self.checkColor = UIImageView(frame: xframe)
        self.checkColor.backgroundColor = UIColor.clear
        self.checkColor.layer.cornerRadius = self.checkColor.frame.size.width / 2
        self.checkColor.clipsToBounds = true
        self.checkColor.contentMode = .scaleToFill
        self.checkColor.image = Asset.checkBlack48.image
        self.checkColor.isHidden = true
        self.scrollView.addSubview(self.checkColor)
        
        //precio
        //label
        xframe.origin = CGPoint(x: 10, y: self.checkColor.frame.maxY + 25)
        let lblPrecio = UILabel(frame: xframe)
        lblPrecio.text = "Precio"
        lblPrecio.font = UIFont.boldSystemFont(ofSize: lblPrecio.font.pointSize)
        lblPrecio.sizeToFit()
        self.scrollView.addSubview(lblPrecio)
        
        xframe.origin = CGPoint(x: lblPrecio.frame.maxX + 20, y: lblPrecio.frame.minY)
        //self.lblPrecio = UILabel(frame: xframe)
        //self.lblPrecio.text = "0 - 500 €"
        //self.lblPrecio.sizeToFit()
        //self.scrollView.addSubview(self.lblPrecio)
        
        //slider
        
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.85, height: Constantes.tamaños.anchoPantalla * 0.07)
        xframe.origin = CGPoint(x: 10, y: lblPrecio.frame.maxY + 30)
        self.sliderPrecio = TTRangeSlider(frame: xframe)
        self.sliderPrecio.maxValue = 500
        self.sliderPrecio.minValue = 0
        self.sliderPrecio.tintColor = UIColor.black
        self.sliderPrecio.tintColorBetweenHandles = Constantes.paletaColores.rojoTrendy
        self.sliderPrecio.selectedMinimum = 0
        self.sliderPrecio.selectedMaximum = 500
        let formatoMoneda = NumberFormatter()
        formatoMoneda.numberStyle = .currency
        formatoMoneda.locale = Locale(identifier: "es_ES")
        formatoMoneda.maximumFractionDigits = 0
        self.sliderPrecio.numberFormatterOverride = formatoMoneda
        self.sliderPrecio.handleColor = Constantes.paletaColores.rojoTrendy
        self.sliderPrecio.handleDiameter = Constantes.tamaños.anchoPantalla * 0.08
        self.sliderPrecio.lineHeight = Constantes.tamaños.anchoPantalla * 0.01
        self.sliderPrecio.minDistance = 20.0
        self.sliderPrecio.maxLabelColour = Constantes.paletaColores.rojoTrendy
        self.sliderPrecio.minLabelColour = Constantes.paletaColores.rojoTrendy
        self.sliderPrecio.enableStep = true
        self.sliderPrecio.step = 10.0
        self.scrollView.addSubview(self.sliderPrecio)
        self.sliderPrecio.center.x = centroHorizontal - 10
        
        self.scrollView.contentSize.height = self.sliderPrecio.frame.maxY + (self.tabBarController?.tabBar.frame.size.height)! + 20
        
        self.view.addSubview(self.scrollView)
        
        //loader
        self.setLoaderAnimation()
        
        switch self.searchSex {
        case .Hombre:
            self.btnCatHombre.font = UIFont.boldSystemFont(ofSize: self.btnCatMujer.font.pointSize)
            self.btnCatHombre.textColor = Constantes.paletaColores.rojoTrendy
            self.indexSexCat = self.btnCatHombre.tag
            break
        case .Mujer:
            self.btnCatMujer.font = UIFont.boldSystemFont(ofSize: self.btnCatMujer.font.pointSize)
            self.btnCatMujer.textColor = Constantes.paletaColores.rojoTrendy
            self.indexSexCat = self.btnCatMujer.tag
            break
        case .Niño:
            self.btnCatNiño.font = UIFont.boldSystemFont(ofSize: self.btnCatMujer.font.pointSize)
            self.btnCatNiño.textColor = Constantes.paletaColores.rojoTrendy
            self.indexSexCat = self.btnCatNiño.tag
            break
            
        }
    }

    func filterCategoriasMarcas(initialLoad: Bool = false){
        var strSearchSex1 = ""
        var strSearchSex2 = ""
        var strSearchSex3 = ""
        var strSearchSex4 = ""
        
        switch self.indexSexCat {
        case Sexo.Mujer.rawValue: //mujer
            strSearchSex1 = "2"
            strSearchSex2 = "3"
            strSearchSex3 = "5"
            break
        case Sexo.Hombre.rawValue: //hombre
            strSearchSex1 = "1"
            strSearchSex2 = "3"
            strSearchSex3 = "6"
            break
        case Sexo.Niño.rawValue: //niño
            strSearchSex1 = "4"
            strSearchSex2 = "5"
            strSearchSex3 = "6"
            strSearchSex4 = "3"
            break
        default:
            break
        }
        
        //categorias 
        self.arraySubCategoriasS.removeAll()
        let unItem = Subcategoria_support()
        unItem.id = "0"
        unItem.nombre = "Todas"
        unItem.tipo = "0"
        self.arraySubCategoriasS.append(unItem)
        
        for item in self.arraySubcategoriaTodas {
            if ((item.tipo == strSearchSex1) || (item.tipo == strSearchSex2) || (item.tipo == strSearchSex3) || (item.tipo == strSearchSex4) ) {
                self.arraySubCategoriasS.append(item)
                if ((initialLoad) && (Int(item.id) == self.idCatDefault) && (self.idCatDefault != 0)){
                    self.selectedIndexCategorias.removeAll()
                    self.selectedIndexCategorias.append(self.arraySubCategoriasS.count - 1)
                }
            }
        }
        
        if ((initialLoad) && (self.idCatDefault != 0)) {
            self.txtCategoria.text = self.getStringSeparado(sender: "subcat", tipo: "nombre")
        } else {
            self.selectedIndexCategorias.removeAll()
            self.selectedIndexCategorias.append(0)
            self.txtCategoria.text = "Todas"
        }
        self.pickCategoria.reloadAllComponents()
        
        //marcas
        self.arrayMarcas.removeAll()
        
        let strTipoMarca = String(self.indexSexCat)
        
        for item in self.arrayMarcasTodas {
            if ((item.tipo == strTipoMarca) && (item.principal)) {
                self.arrayMarcas.append(item)
            }
        }
        
        //ordenar marcas
        self.arrayMarcas.sort(by: {$0.ordenPrincipal < $1.ordenPrincipal})
        
        self.selectedIndexMarcas.removeAll()
        //self.selectedIndexMarcas.append(0)
        self.collectionMarcas.reloadData()
        //print("count array marcas->\(self.arrayMarcas.count)")
        //self.pickMarca.reloadAllComponents()
        
        
        //self.txtMarca.text = "Todas"
        
        //tiendas
        self.selectedIndexTiendas.removeAll()
        self.selectedIndexTiendas.append(0)
        self.pickTienda.reloadAllComponents()
    }
    
    func filterCategoriasAFilter(){
        
        var cont = 0
        
        //marcas
        var marcas = ""
        if let xmarcas = self.strMarca {
            marcas = xmarcas
        }
        
        if marcas != ""{
            let arrMarcas = marcas.components(separatedBy: ",")
            self.selectedIndexMarcas.removeAll()
            
            for item in arrMarcas {
                self.selectedIndexMarcas.append(Int(item)!)
            }
            self.collectionMarcas.reloadData()
        }
        
        //categorias
        cont = 0
        var categorias = ""
        if let xcatego = self.strCategoria {
            categorias = xcatego
        }
        
        if categorias != "" {
            let arrCatego = categorias.components(separatedBy: ",")
            self.selectedIndexCategorias.removeAll()
            
            for item in self.arraySubCategoriasS {
                for icatego in arrCatego {
                    if icatego == item.id {
                        self.selectedIndexCategorias.append(cont)
                    }
                }
                cont += 1
            }
            self.txtCategoria.text = self.getStringSeparado(sender: "subcat", tipo: "nombre")
        }
        
        //precios
        let arrPrecios = self.strPrecios.components(separatedBy: ",")
        self.sliderPrecio.selectedMinimum = Float(arrPrecios[0])!
        self.sliderPrecio.selectedMaximum = Float(arrPrecios[1])!
        
        
        //tiendas
        cont = 0
        var tiendas = ""
        if let xtienda = self.strTienda {
            tiendas = xtienda
        }
        
        if tiendas != "" {
            let arrTiendas = tiendas.components(separatedBy: ",")
            self.selectedIndexTiendas.removeAll()
            
            for item in self.arrayTiendasS {
                for itienda in arrTiendas {
                    if itienda == item.id {
                        self.selectedIndexTiendas.append(cont)
                    }
                }
                cont += 1
            }
            self.txtTienda.text = self.getStringSeparado(sender: "tienda", tipo: "nombre")
        }
    }
    
    
    //picker view delegate
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.pickCategoria {
            return self.arraySubCategoriasS.count
        } else if pickerView == self.pickTienda {
            return self.arrayTiendasS.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var cell = UITableViewCell()
        
        if view == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.clear
            //cell.bounds = CGRect(x: 0, y: 0, width: cell.frame.size.width - 20, height: 44)
            
        } else {
            cell = view as! UITableViewCell
        }
        
        if pickerView == self.pickCategoria {
            if let _ = self.selectedIndexCategorias.index(of: row) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            
            cell.textLabel?.text = self.arraySubCategoriasS[row].nombre
            cell.textLabel?.textAlignment = .center
            cell.tag = row
        } else if (pickerView == self.pickTienda) {
            if let _ = self.selectedIndexTiendas.index(of: row) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            
            cell.textLabel?.text = self.arrayTiendasS[row].nombre
            cell.textLabel?.textAlignment = .center
            cell.tag = row
        }
        
        
        
        return cell
    }
 
    //picker view delegate - fin
    
    func rowTappedSubCategoria(sender: UITapGestureRecognizer){
        
        if sender.state == UIGestureRecognizerState.ended {
            let rowHeight = self.pickCategoria.rowSize(forComponent: 0).height
            let selectedRowFrame = self.pickCategoria.bounds.insetBy(dx: 0.0, dy: (self.pickCategoria.frame.height - rowHeight) / 2.0)
            
            let usertappedonselectedrow = selectedRowFrame.contains(sender.location(in: self.pickCategoria))
            if usertappedonselectedrow {
                let selectedRow = self.pickCategoria.selectedRow(inComponent: 0)
                if let indexSelected = self.selectedIndexCategorias.index(of: selectedRow) {
                    self.selectedIndexCategorias.remove(at: indexSelected)
                    (self.pickCategoria.view(forRow: selectedRow, forComponent: 0) as! UITableViewCell).accessoryType = .none
                    self.updatePickCategoria(index: selectedRow)
                    
                } else {
                    if (selectedRow == 0){
                        self.selectedIndexCategorias.removeAll()
                        self.selectedIndexCategorias.append(0)
                        self.updatePickCategoria(index: selectedRow)
                        self.pickCategoria.reloadAllComponents()
                        
                    } else {
                        self.selectedIndexCategorias.append(selectedRow)
                        (self.pickCategoria.view(forRow: selectedRow, forComponent: 0) as! UITableViewCell).accessoryType = .checkmark
                        self.updatePickCategoria(index: selectedRow)
                    }
                }
                
                if (self.selectedIndexCategorias.count >= 1) {
                    if let indexZero = self.selectedIndexCategorias.index(of: 0){
                        if self.selectedIndexCategorias.count != 1 {
                            self.selectedIndexCategorias.remove(at: indexZero)
                            self.updatePickCategoria(index: selectedRow)
                        }
                    }
                } else {
                    if self.selectedIndexCategorias.index(of: 0) == nil {
                        self.selectedIndexCategorias.append(0)
                        self.updatePickCategoria(index: 0)
                        self.pickCategoria.reloadAllComponents()
                    }
                }
            }
        }
    }
    
    
    func rowTappedTienda(sender: UITapGestureRecognizer){
        
        if sender.state == UIGestureRecognizerState.ended {
            let rowHeight = self.pickTienda.rowSize(forComponent: 0).height
            let selectedRowFrame = self.pickTienda.bounds.insetBy(dx: 0.0, dy: (self.pickTienda.frame.height - rowHeight) / 2.0)
            
            let usertappedonselectedrow = selectedRowFrame.contains(sender.location(in: self.pickTienda))
            if usertappedonselectedrow {
                let selectedRow = self.pickTienda.selectedRow(inComponent: 0)
                
                if let indexSelected = self.selectedIndexTiendas.index(of: selectedRow) {
                    self.selectedIndexTiendas.remove(at: indexSelected)
                    (self.pickTienda.view(forRow: selectedRow, forComponent: 0) as! UITableViewCell).accessoryType = .none
                    self.updatePickTiendas(index: selectedRow)
                    
                } else {
                    if (selectedRow == 0){
                        self.selectedIndexTiendas.removeAll()
                        self.selectedIndexTiendas.append(0)
                        self.updatePickTiendas(index: selectedRow)
                        self.pickTienda.reloadAllComponents()
                        
                    } else {
                        self.selectedIndexTiendas.append(selectedRow)
                        (self.pickTienda.view(forRow: selectedRow, forComponent: 0) as! UITableViewCell).accessoryType = .checkmark
                        self.updatePickTiendas(index: selectedRow)
                    }
                }
             
                if (self.selectedIndexTiendas.count >= 1) {
                    if let indexZero = self.selectedIndexTiendas.index(of: 0){
                        if self.selectedIndexTiendas.count != 1 {
                            self.selectedIndexTiendas.remove(at: indexZero)
                            self.updatePickTiendas(index: selectedRow)
                        }
                    }
                } else {
                    if self.selectedIndexTiendas.index(of: 0) == nil {
                        self.selectedIndexTiendas.append(0)
                        self.updatePickTiendas(index: 0)
                        self.pickTienda.reloadAllComponents()
                    }
                    
                }
             
            }
        }
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func donePickerCategoria(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtCategoria.text = self.getStringSeparado(sender: "subcat", tipo: "nombre")
        }
        
        self.view.endEditing(true)
        
    }
    
    
    func donePickerTienda(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtTienda.text = self.getStringSeparado(sender: "tienda", tipo: "nombre")
        }
        
        self.view.endEditing(true)
        
    }
    
    
    func tapOnColor(gesture: UITapGestureRecognizer){
        let index = (gesture.view?.tag)!
        print("LLega aqui con este index->\(index)")
        let anchoAltoColor = Constantes.tamaños.anchoPantalla * 0.1
        var xframe = CGRect.zero
        xframe.size = CGSize(width: anchoAltoColor * 0.7, height: anchoAltoColor * 0.7)
        self.checkColor.frame = xframe
        
        if self.currentColorIndex != -1 {
            self.checkColor.isHidden = true
            self.checkColor.backgroundColor = UIColor.clear
        }
        if index != self.currentColorIndex {
            if Constantes.strings.arrayColores[index].color == UIColor.black {
                self.checkColor.image = Asset.checkWhite48.image
                self.checkColor.backgroundColor = UIColor.white
                xframe.size = CGSize(width: anchoAltoColor * 0.72, height: anchoAltoColor * 0.72)
                self.checkColor.frame = xframe
            } else {
                self.checkColor.image = Asset.checkBlack48.image
            }
            self.checkColor.setNeedsDisplay()
            print(self.checkColor.image!)
            self.checkColor.frame.origin = self.arrayViewColores[index].frame.origin
            self.checkColor.center = self.arrayViewColores[index].center
            self.checkColor.isHidden = false
            self.view.bringSubview(toFront: self.checkColor)
            self.currentColorIndex = index
        } else {
            self.currentColorIndex = -1
        }
    }
    
    func getStringSeparado(sender: String, tipo: String)->String {
        var stringReturn = ""
        if sender == "subcat" {
            for index in self.selectedIndexCategorias {
                if tipo == "id" {
                    stringReturn += "\(self.arraySubCategoriasS[index].id!),"
                } else {
                    stringReturn += "\(self.arraySubCategoriasS[index].nombre!), "
                }
            }
        }
        else if sender == "marca" {
            var arrayMarcasSend = [String]()
            for index in self.selectedIndexMarcas {
                let strIndex = String(index)
                if tipo == "id" {
                    
                    if let _ = self.arrayMarcasTodas.first(where: {$0.id == strIndex}){
                        arrayMarcasSend.append(strIndex)
                    }
                }
                //else {
                //    stringReturn += "\(self.arrayMarcas[index].nombre!), "
                //}
            }
            for item in arrayMarcasSend {
                stringReturn  += "\(item),"
            }
        }
        else if sender == "tienda" {
            for index in self.selectedIndexTiendas {
                if tipo == "id" {
                    stringReturn += "\(self.arrayTiendasS[index].id!),"
                } else {
                    stringReturn += "\(self.arrayTiendasS[index].nombre!), "
                }
            }
        }
        var xoffset = -2
        if tipo == "id" {
            xoffset = -1
        }
        let endIndex = stringReturn.index(stringReturn.endIndex, offsetBy: xoffset)
        let truncated = stringReturn.substring(to: endIndex)
        stringReturn = truncated
        print("STRING RETURN--->\(stringReturn)")
        return stringReturn
    }
    
    func changeSexCat(recognizer: UITapGestureRecognizer){
        let index = (recognizer.view?.tag)!
        switch index {
        case Sexo.Mujer.rawValue: //mujer
            if index != self.indexSexCat {
                self.indexSexCat = index
                self.btnCatMujer.font = UIFont.boldSystemFont(ofSize: self.btnCatMujer.font.pointSize)
                self.btnCatMujer.textColor = Constantes.paletaColores.rojoTrendy
                
                //
                self.btnCatHombre.font = UIFont.systemFont(ofSize: self.btnCatHombre.font.pointSize)
                self.btnCatHombre.textColor = UIColor.gray
                
                self.btnCatNiño.font = UIFont.systemFont(ofSize: self.btnCatNiño.font.pointSize)
                self.btnCatNiño.textColor = UIColor.gray
                
                self.filterCategoriasMarcas()
            }
            break
        case Sexo.Hombre.rawValue: //hombre
            if index != self.indexSexCat {
                self.indexSexCat = index
                self.btnCatHombre.font = UIFont.boldSystemFont(ofSize: self.btnCatHombre.font.pointSize)
                self.btnCatHombre.textColor = Constantes.paletaColores.rojoTrendy
                
                //
                self.btnCatMujer.font = UIFont.systemFont(ofSize: self.btnCatMujer.font.pointSize)
                self.btnCatMujer.textColor = UIColor.gray
                
                self.btnCatNiño.font = UIFont.systemFont(ofSize: self.btnCatNiño.font.pointSize)
                self.btnCatNiño.textColor = UIColor.gray
                
                self.filterCategoriasMarcas()
            }
            
            break
        case Sexo.Niño.rawValue: //niño
            if index != self.indexSexCat {
                self.indexSexCat = index
                self.btnCatNiño.font = UIFont.boldSystemFont(ofSize: self.btnCatNiño.font.pointSize)
                self.btnCatNiño.textColor = Constantes.paletaColores.rojoTrendy
                
                //
                self.btnCatMujer.font = UIFont.systemFont(ofSize: self.btnCatMujer.font.pointSize)
                self.btnCatMujer.textColor = UIColor.gray
                
                self.btnCatHombre.font = UIFont.systemFont(ofSize: self.btnCatHombre.font.pointSize)
                self.btnCatHombre.textColor = UIColor.gray
                
                self.filterCategoriasMarcas()
            }
            break
        default:
            break
        }
        
    }
    
    func updatePickCategoria(index: Int){
        //self.pickCategoria.reloadAllComponents()
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.pickCategoria.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
    
    func updatePickTiendas(index: Int){
        //self.pickTienda.reloadAllComponents()
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.pickTienda.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
    
    //collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayMarcas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! CeldaMainCell
        let laMarca = self.arrayMarcas[indexPath.item]
        let urlimg = URL(string: laMarca.url)
        var elFrame = cell.frame
        elFrame.size.height = cell.frame.size.height * 0.9
        elFrame.size.width = cell.frame.size.width * 0.9
        cell.imagenFondo.frame = elFrame
        cell.imagenFondo.kf.indicatorType = .activity
        cell.imagenFondo.kf.setImage(with: urlimg)
        cell.imagenFondo.contentMode = .scaleAspectFit
        cell.imagenFondo.center.x = cell.frame.size.width / 2
        cell.imagenFondo.center.y = cell.frame.size.height / 2
        cell.imagenFondo.clipsToBounds = true
        cell.imagenFondo.setNeedsDisplay()
        
        cell.layer.cornerRadius = 10
        
        if let _ = self.selectedIndexMarcas.index(of: Int(laMarca.id)!) {
            cell.layer.borderWidth = 2
            cell.layer.borderColor = Constantes.paletaColores.rojoTrendy.cgColor
        } else {
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        
        let anchoCell = Constantes.tamaños.anchoPantalla / 3
        
        elTamaño = CGSize(width: anchoCell, height: collectionView.frame.size.height * 0.9)
        
        return elTamaño
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let laMarca = self.arrayMarcas[indexPath.item]
        
        let cell = collectionView.cellForItem(at: indexPath)
        
        if let elIndex = self.selectedIndexMarcas.index(of: Int(laMarca.id)!) {
            self.selectedIndexMarcas.remove(at: elIndex)
            cell?.layer.borderColor = UIColor.lightGray.cgColor
            cell?.layer.borderWidth = 1
        } else {
            cell?.layer.borderWidth = 2
            cell?.layer.borderColor = Constantes.paletaColores.rojoTrendy.cgColor
            self.selectedIndexMarcas.append(Int(laMarca.id)!)
        }
        
    }
    
    //collection view end
    
    func verTodasTapped(sender: UITapGestureRecognizer){
        let elVC = AllBrandsTableVC()
        elVC.arrayTodasMarcas = self.arrayMarcasTodas
        elVC.tipoFiltro = String(self.indexSexCat)
        elVC.selectedIndexMarcas = self.selectedIndexMarcas
        self.navigationController?.pushViewController(elVC, animated: true)
        elVC.buildInterface()
        elVC.filterMarcas()
    }
    
    func backFromAllBrands(notification : NSNotification){
        let paramIndex = notification.object as! NSDictionary
        if let value = paramIndex["newIndex"] as? [Int] {
            self.selectedIndexMarcas = value
        }
        self.collectionMarcas.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
