//
//  CardView.swift
//  ZLSwipeableViewSwiftDemo
//
//  Created by Zhixuan Lai on 5/24/15.
//  Copyright (c) 2015 Zhixuan Lai. All rights reserved.
//

import UIKit
import Device_swift

class CardView: UIView {
    
    var imgFondo : UIImageView!
    var lblMarca : UILabel!
    var lblModelo : UILabel!
    var lblPrecio : UILabel!
    let isItem = true
    var backDescuento : UIImageView!
    var lblDescuento : UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        
        let deviceType = UIDevice.current.deviceType
        
        var altoImg : CGFloat = 0
        //(esiPad ? self.frame.size.height * 0.8 : self.frame.size.height * 0.8)
        
        switch deviceType {
        case .iPhone6, .iPhone6S, .iPhone6Plus, .iPhone6SPlus, .iPhone7, .iPhone7Plus , .iPhoneSE :
            altoImg = self.frame.size.height * 0.85
        default:
            altoImg = self.frame.size.height * 0.8
        }
        
        // Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0
        
        self.backgroundColor = UIColor.white
        
        
        //interface
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 0 , y:0)
        xframe.size = CGSize(width: self.frame.size.width, height: altoImg)
        self.imgFondo = UIImageView(frame: xframe)
        self.addSubview(self.imgFondo)
        
        //lblPrecio
        xframe.origin = CGPoint(x: 0 , y: self.frame.size.height - 30)
        xframe.size = CGSize(width: 50, height: 50)
        self.lblPrecio = UILabel(frame: xframe)
        self.addSubview(self.lblPrecio)
        
        //modelo
        xframe.origin = CGPoint(x: 10, y: self.imgFondo.frame.size.height + 10)
        self.lblModelo = UILabel(frame: xframe)
        self.lblModelo.textColor = UIColor.gray
        self.lblModelo.lineBreakMode = .byTruncatingTail
        
        self.addSubview(self.lblModelo)
        
        //marca
        xframe.origin = CGPoint(x: 10, y: self.lblModelo.frame.size.height + 10)
        self.lblMarca = UILabel(frame: xframe)
        self.lblMarca.lineBreakMode = .byTruncatingTail
        self.addSubview(self.lblMarca)
        
        //descuento
        let anchoAltoDesc = self.frame.size.width * 0.2
        xframe.size = CGSize(width: anchoAltoDesc, height: anchoAltoDesc)
        let xpos = self.frame.size.width - (anchoAltoDesc + 15)
        xframe.origin = CGPoint(x: xpos, y: 20)
        self.backDescuento = UIImageView(frame: xframe)
        self.backDescuento.contentMode = .scaleAspectFill
        self.backDescuento.image = Asset.icDiscount.image
        self.addSubview(self.backDescuento)
        self.backDescuento.isHidden = true
        
        //label
        xframe.size = CGSize(width: anchoAltoDesc * 0.7, height: anchoAltoDesc * 0.7)
        self.lblDescuento = UILabel(frame: xframe)
        self.lblDescuento.textColor = UIColor.white
        self.lblDescuento.textAlignment = .center
        self.lblDescuento.numberOfLines = 2
        self.lblDescuento.adjustsFontSizeToFitWidth = true
        self.backDescuento.addSubview(self.lblDescuento)
        self.lblDescuento.isHidden = true
    }
}
