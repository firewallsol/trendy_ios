//
//  MainScreenNavControllerNC.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class MainScreenNavControllerNC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mainvc = MainScreenVC()
        self.pushViewController(mainvc, animated: false)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
