//
//  PresentationCardView.swift
//  trendy
//
//  Created by Leonel Sanchez on 07/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class PresentationCardView: UIView {
        
    var imgFondo : UIImageView!
    var lblDescrip : UILabel!
    var lblNumProds : UILabel!
    var btnDiscover : UIButton!
    var shadowView : UIView!
    let isItem = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        // Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0
        
        self.backgroundColor = UIColor.white
        
        
        //interface
        var xframe = CGRect.zero
        
        self.imgFondo = UIImageView(frame: self.frame)
        self.addSubview(self.imgFondo)
        
        self.shadowView = UIView(frame: self.frame)
        self.shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        self.addSubview(self.shadowView)
        
        //lbl descrip
        xframe.origin = CGPoint(x: 0 , y: self.frame.size.height - 30)
        xframe.size = CGSize(width: 50, height: 50)
        self.lblDescrip = UILabel(frame: xframe)
        self.lblDescrip.numberOfLines = 0
        self.lblDescrip.textAlignment = .center
        self.lblDescrip.textColor = UIColor.white
        self.addSubview(self.lblDescrip)
        
        //num prods
        xframe.origin = CGPoint(x: 10, y: 100)
        self.lblNumProds = UILabel(frame: xframe)
        self.lblNumProds.textColor = UIColor.white
        if #available(iOS 8.2, *) {
            self.lblNumProds.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize, weight: UIFontWeightThin)
        } else {
            self.lblNumProds.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        }
        self.addSubview(self.lblNumProds)
        
        //button
        xframe.origin = CGPoint(x: 10, y: 200)
        xframe.size = CGSize(width: self.frame.size.width * 0.8, height: self.frame.size.width * 0.15)
        self.btnDiscover = UIButton(frame: xframe)
        self.btnDiscover.backgroundColor = Constantes.paletaColores.backgroundPresentationCardButton
        self.btnDiscover.setTitle("Discover & Shop", for: .normal)
        self.btnDiscover.setTitleColor(UIColor.white, for: .normal)
        self.btnDiscover.layer.cornerRadius = 15
        self.addSubview(self.btnDiscover)
        
    }

}
