//
//  ClosetNavControllerNC.swift
//  trendy
//
//  Created by Leonel Sanchez on 02/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class ClosetNavControllerNC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let xclosetvc = ClosetVC()
        self.pushViewController(xclosetvc, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
