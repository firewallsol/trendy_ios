//
//  ProfileNavControllerNC.swift
//  trendy
//
//  Created by Leonel Sanchez on 02/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class ProfileNavControllerNC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let xprofilevc = ProfileVC()
        self.pushViewController(xprofilevc, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
