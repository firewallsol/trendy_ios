//
//  TabBarMainControllerViewController.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class TabBarMainControllerViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /*
        let imgFooterHome = Functions().imageWithImage(sourceImage: Constantes.imagenes.imgFooterHome!, scaledToWidth: 30)
        
        let imgFooterSearch = Functions().imageWithImage(sourceImage: Constantes.imagenes.imgFooterSearch!, scaledToWidth: 30)
        let imgFooterCloset = Functions().imageWithImage(sourceImage: Constantes.imagenes.imgFooterCloset!, scaledToWidth: 30)
        let imgFooterProfile = Functions().imageWithImage(sourceImage: Constantes.imagenes.imgFooterProfile!, scaledToWidth: 30)
        */
        
        // Create Tabs
        let tabUno = MainScreenNavControllerNC()
        let tabUnoBarItem = UITabBarItem(title: "", image: Asset.icHome.image, selectedImage: Asset.icHome.image)
        
        tabUno.tabBarItem = tabUnoBarItem
        
        
        // Create Tab two
        let tabDos = SearchNavControllerNC()
        let tabDosBarItem = UITabBarItem(title: "", image: Asset.icSearch.image, selectedImage: Asset.icSearch.image)
        
        tabDos.tabBarItem = tabDosBarItem
        
        // Create Tab tres
        let tabTres = ClosetNavControllerNC()
        let tabTresBarItem = UITabBarItem(title: "", image: Asset.icCloset.image, selectedImage: Asset.icCloset.image)
        
        tabTres.tabBarItem = tabTresBarItem
        
        // Create Tab cuatro
        let tabCuatro = ProfileNavControllerNC()
        let tabCuatroBarItem = UITabBarItem(title: "", image: Asset.icProfile.image, selectedImage: Asset.icProfile.image)
        
        tabCuatro.tabBarItem = tabCuatroBarItem
        
        self.viewControllers = [tabUno, tabDos, tabTres, tabCuatro]
        
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.backgroundColor = Constantes.paletaColores.tabbarbackground
        self.tabBar.tintColor = Constantes.paletaColores.rojoTrendy
        
        
        for tabBarItem in tabBar.items!
        {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
        
        
    }
    
    // UITabBarControllerDelegate method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let titulo = viewController.title {
            print("Selected \(titulo)")
        }
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if Functions().isGuestUser() {
            var elReturn = true
            
            if viewController is ClosetNavControllerNC {
                self.mensajeSimple(mensaje: Constantes.guestData.mensajeAccesoArmario, dismiss: false)
                elReturn = false
            }
            
            return elReturn
        } else {
            return true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
