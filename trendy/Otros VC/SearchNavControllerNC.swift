//
//  SearchNavControllerNC.swift
//  trendy
//
//  Created by Leonel Sanchez on 31/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class SearchNavControllerNC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let xsearchvc = SearchVC()
        self.pushViewController(xsearchvc, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
