//
//  AppDelegate.swift
//  trendy
//
//  Created by Leonel Sanchez on 25/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import FacebookCore
import UserNotifications
import SwiftyJSON


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        //############## FIREBASE ##################
        
        FirebaseApp.configure()
        
        self.registerForPushNotifications(application: application)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification),name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        
        //############ FIREBASE END ################
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        self.gotoCoverScreen()
        
        return true
    }
    
    func gotoCoverScreen(){
        // Initialize the window
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.black
        let navPrincipal = UINavigationController()
        navPrincipal.view.backgroundColor = UIColor.black
        navPrincipal.setNavigationBarHidden(true, animated: false)
        
        self.window?.rootViewController = navPrincipal
        
        let VCcoverScreen = CoverScreenVC()
        navPrincipal.pushViewController(VCcoverScreen, animated: true)
        
        self.window?.makeKeyAndVisible()
    }
    
    public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return SDKApplicationDelegate.shared.application(application, open:url, sourceApplication:sourceApplication, annotation:annotation)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //############## FIREBASE ##################
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else{
                    print("no se dieron permisos a la app")
                }
            })
        }
            
        else{
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        //Pruebas
        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)

        //prod
        //Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
    }
    
    // [START receive_message]
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("willPresentNotification - userNotificationCenter: \(notification)")
        let userInfo = notification.request.content.userInfo as NSDictionary
        print("\(userInfo)")
        
        var title = ""
        var body = ""
        
        let json = JSON(userInfo)
        print ("elJSON->\(json)")
        if json["aps"]["alert"]["title"].exists() {
            title = json["aps"]["alert"]["title"].string!
            if json["aps"]["alert"]["body"].exists() {
                body = json["aps"]["alert"]["body"].string!
            }
            
        } else if (json["aps"]["alert"].exists()){
            body = json["aps"]["alert"].string!
        }
        let alerta = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .default,handler: nil)
        alerta.addAction(alertActionOK)
        (self.window?.rootViewController as! UINavigationController).visibleViewController?.present(alerta, animated: true, completion: nil)
        //self.window?.rootViewController?.present(alerta, animated: true, completion: nil)
        
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("didReceiveNotificationResponse \(response)")
        
        let userInfo = response.notification.request.content
        let alerta = UIAlertController(title: userInfo.title, message: userInfo.body, preferredStyle: .alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .default,handler: nil)
        alerta.addAction(alertActionOK)
        //self.window?.rootViewController?.present(alerta, animated: true, completion: nil)
        (self.window?.rootViewController as! UINavigationController).visibleViewController?.present(alerta, animated: true, completion: nil)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("notification - didReceiveRemoteNotification:\(userInfo)")
        var title:String = ""
        var body:String = ""
        
        let json = JSON(userInfo)
        print ("elJSON->\(json)")
        if json["aps"]["alert"]["title"].exists() {
            title = json["aps"]["alert"]["title"].string!
            if json["aps"]["alert"]["body"].exists() {
                body = json["aps"]["alert"]["body"].string!
            }
            
        } else if (json["aps"]["alert"].exists()){
            body = json["aps"]["alert"].string!
        }
        
        let alerta = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .default,handler: nil)
        alerta.addAction(alertActionOK)
        (self.window?.rootViewController as! UINavigationController).visibleViewController?.present(alerta, animated: true, completion: nil)
        //self.window?.rootViewController?.present(alerta, animated: true, completion: nil)
        
    }
    
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                             fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        print("notification - private didReceiveRemoteNotification:\(userInfo)")
        var title:String = ""
        var body:String = ""
        
        let json = JSON(userInfo)
        print ("elJSON->\(json)")
        if json["aps"]["alert"]["title"].exists() {
            title = json["aps"]["alert"]["title"].string!
            if json["aps"]["alert"]["body"].exists() {
                body = json["aps"]["alert"]["body"].string!
            }
            
        } else if (json["aps"]["alert"].exists()){
            body = json["aps"]["alert"].string!
        }
        
        let alerta = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .default,handler: nil)
        alerta.addAction(alertActionOK)
        (self.window?.rootViewController as! UINavigationController).visibleViewController?.present(alerta, animated: true, completion: nil)
        //self.window?.rootViewController?.present(alerta, animated: true, completion: nil)
        
    }
    
    
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM
        connectToFcm()
    }
    // [END refresh_token]
    
    
    // [START connect_to_fcm]
    func connectToFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = true
        /*
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                //print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
        */
    }
    // [END connect_to_fcm]
    
    // [START connect_on_active]
    func applicationDidBecomeActive(application: UIApplication) {
        connectToFcm()
    }
    // [END connect_on_active]
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        //Messaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    // [END disconnect_from_fcm]
    
    //subscribe unsubscribe
    func subscribeTo(topic : String){
        Messaging.messaging().subscribe(toTopic: "/topics/\(topic)")
    }
    
    func unSubscribeTo(topic : String){
        Messaging.messaging().unsubscribe(fromTopic: "/topics/\(topic)")
    }
    
    //############## FIREBASE ##################


}

